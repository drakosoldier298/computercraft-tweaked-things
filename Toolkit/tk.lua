version = 20220530.1500
--[[
	**********Toolkit v2**********
	https://pastebin.com/UFvjc1bw
	Last edited: see version YYYYMMDD.HHMM
	if NOT online:
		Make sure you create a folder 'lib' and place menu.lua and clsTurtle.lua into it
	else
		lib folder will be created and files obtained automatically!
	end
	all functions are being converted to 'local' as they are revised
]]

args = {...} -- eg "farm", "tree"

local menu, T
--[[
Computercraft started with mc version 1.7.10 and went to 1.8.9
ccTweaked started around mc 1.12 and currently at 1.18
mc 1.18 has new blocks and bedrock at -64, so needs to be taken into account.
_HOST = The ComputerCraft and Minecraft version of the current computer environment.
For example, ComputerCraft 1.93.0 (Minecraft 1.15.2).
]]
local bedrock = 0
local ceiling = 255
local deletesWater = false
local mcMajorVersion = tonumber(_HOST:sub(_HOST:find("Minecraft") + 10, _HOST:find("\)") -3)) -- eg 1.18
if mcMajorVersion < 1.7  and mcMajorVersion >= 1.18 then -- 1.12 to 1.??
	bedrock = -64
	ceiling = 319
end
if mcMajorVersion < 1.7  and mcMajorVersion <= 1.12 then -- 1.12 to 1.??
	deletesWater = true
end

local function attack(onPerch)
	local totalHitsF = 0
	local totalHitsU = 0
	local totalHitsD = 0
	if onPerch then
		turtle.digUp()
	end
	while true do
		local hitF = false
		local hitU = false
		local hitD = false
		if turtle.attackUp() then
			hitU = true
			totalHitsU = totalHitsU + 1
		end
		if onPerch then
			turtle.turnRight()
		else
			if turtle.attackDown() then
				hitD = true
				totalHitsD = totalHitsD + 1
			end
			if turtle.attack() then
				hitF = true
				totalHitsF = totalHitsF + 1
			end
		end
		if hitF or hitU or hitD then
			print("hits forward: "..totalHitsF..", up: "..totalHitsU..", down: "..totalHitsD)
		end
	end
end

local function checkFuelNeeded(quantity)
	print "2000"
end

local function checkLabel()
	if os.getComputerLabel() == nil then
		os.setComputerLabel("toolkit")
		print("Computer label set to "..os.getComputerLabel())
	end
end

function checkLibs(libDir, filename)
	local fileExists = false
	if fs.exists(libDir) then
		if not fs.isDir(libDir) then
			fs.move(libDir, libDir.."Renamed")
			fs.makeDir(libDir)
		end
	else
		fs.makeDir(libDir)
	end
	if fs.exists(fs.combine(libDir, filename)) or fs.exists(fs.combine(libDir, filename..".lua")) then
		fileExists = true
	end
	return fileExists
end

local function clearAndReplantTrees()
	--[[ clear all trees in a rectangle area defined by walls, fences or non-dirt blocks
	replant with same type of sapling. If original tree 2 blocks wide, replant 4 if possible. ]]
	
	local lib = {}
	
	function lib.getSaplingFromLogType(log)
		--[[ get type of sapling to plant from log type ]]
		if log:find("oak") ~= nil then
			return "minecraft:oak_sapling"
		elseif log:find("spruce") ~= nil then
			return "minecraft:spruce_sapling"
		elseif log:find("birch") ~= nil then
			return "minecraft:birch_sapling"
		elseif log:find("jungle") ~= nil then
			return "minecraft:jungle_sapling"
		elseif log:find("acacia") ~= nil then
			return "minecraft:acacia_sapling"
		elseif log:find("dark_oak") ~= nil then
			return "minecraft:dark_oak_sapling"
		end
		return "sapling"
	end
	
	function lib.plantSapling(sapling, double)
		--[[ plant sapling(s) ]]
		if sapling == "" or sapling == nil then sapling = "sapling" end
		T:up(1)
		lib.suck()
		if double then	-- check if enough saplings
			--slotData.lastSlot, slotData.leastModifier, total, slotData = T:getItemSlot(sapling, -1)
			local a, b, total, _ = T:getItemSlot(sapling, -1)
			if total >= 4 then
				for i = 1, 4 do
					T:place(sapling, -1, "down")
					T:go("F1R1")
				end
				T:forward(1)		-- above pre-planted sapling
			else
				if not T:place(sapling, -1, "down") then
					T:place("sapling", -1, "down")
				end
			end
		else
			if not T:place(sapling, -1, "down") then
				T:place("sapling", -1, "down")
			end
		end
		turtle.select(1)
	end
		
	function lib.suck()
		--[[ Collect saplings, sticks and apples ]]
		turtle.select(1)
		turtle.suck()
		turtle.suckUp()
		turtle.suckDown()
	end
	
	function lib.turn(direction)
		--[[ change direction and return new value for direction ]]
		if direction == "r" then
			T:turnRight(1)
			direction = "l"
		else
			T:turnLeft(1)
			direction = "r"
		end
		return direction	-- will only change direction variable if return value is used
	end
	
	function lib.emptyInventory(blockTypeD)
		--[[ Empty all except 32 of each sapling and 1 chest ]]
		if blockTypeD == nil then
			blockTypeD = T:getBlockType("down")
		end
		if blockTypeD:find("chest") ~= nil or blockTypeD:find("barrel") ~= nil then
			-- empty logs, apples, sticks and all but 1 stack of each sapling type
			T:emptyInventorySelection("down", {"chest", "oak_sapling", "birch_sapling", "spruce_sapling", "acacia_sapling", "jungle_sapling","dark_oak_sapling"},{1, 32, 32, 32, 32, 32, 32})
			return true
		else
			return false
		end
	end
	
	function lib.moveDown(blockTypeD)
		--[[ move down until hit ground. Break leaves and continue ]]
		if blockTypeD == nil then
			blockTypeD = T:getBlockType("down")
		end
		while blockTypeD == "" or blockTypeD:find("leaves") ~= nil do	-- move down, breaking leavse
			T:down(1)
			lib.suck()
			blockTypeD = T:getBlockType("down")
		end
		return blockTypeD
	end
	
	function lib.moveForward()
		--[[ Move forward 1 block only, go down to ground while air or leaves below ]]
		local blockTypeF = T:getBlockType("forward")
		local blockTypeD = T:getBlockType("down")
		if blockTypeF == "" or blockTypeF:find("leaves") ~= nil then	-- air or leaves ahead
			T:forward(1)												-- move forward, breaking leaves
			T:dig("up")													-- remove leaves / low branches above to allow space for player
			lib.suck()
			blockTypeD = lib.moveDown()
			if not lib.emptyInventory(blockTypeD) then					-- check if above a corner chest / barrel
				if lib.isBorder(blockTypeD) then						-- not above chest so check if above border
					return false, blockTypeD							-- above a border block so stop
				end
			end
			blockTypeF = T:getBlockType("forward")
			return true, blockTypeF										-- moved ok, could be air or block in front
		end
		return false, blockTypeF 										-- did not move, obstacle in front NOT leaves or air
	end
	
	function lib.moveUp(blockTypeF)
		--[[ Move up until air in front (dig leaves / harvest tree) ]]
		if blockTypeF == nil then
			blockTypeF = T:getBlockType("forward")
		end
		while blockTypeF:find("dirt") ~= nil or
			  blockTypeF:find("grass_block") ~= nil or
			  T:isVegetation(blockTypeF) do	-- go up while dirt, grass-block or any vegetation in front
			T:up(1)
			blockTypeF = T:getBlockType("forward")
			if blockTypeF:find("log") ~= nil then
				lib.harvestTree(blockTypeF)
				return T:getBlockType("forward")
			elseif blockTypeF:find("leaves") ~= nil then
				T:dig("forward")
				return ""
			end
		end
		return blockTypeF	-- should be "" (air) or any border block
	end
	
	function lib.harvestTree(blockTypeF)
		--[[ Fell tree, returns true if double size ]]
		-- clsTurtle.harvestTree(extend, craftChest, direction)
		local saplingType = lib.getSaplingFromLogType(blockTypeF)
		local double = T:harvestTree(false, false, "forward")	-- assume single tree, will auto-discover
		lib.plantSapling(saplingType, double)
	end
	
	function lib.safeMove()
		--[[ move forward until border reached. loop breaks at that point ]]
		local blockTypeF = ""
		local success = true
		while success do
			success, blockTypeF = lib.moveForward()				-- move forward 1 block, return block type ahead
			if not success then 								-- did not move forwards, block in the way: either log, dirt/grass, border block or vegetation
				if blockTypeF:find("log") then 					-- tree found
					lib.harvestTree(blockTypeF)
					success = true								-- block (log) removed, try again
				else
					success = not lib.isBorder(blockTypeF)		-- Is at border?: if is at border success = false so loop stops
					if success then								-- Not at border. Dirt/grass vegetation in front
						blockTypeF = lib.moveUp(blockTypeF)		-- move up until leaves/log/air
						success = not lib.isBorder(blockTypeF)	-- Is at border?: if is at border success = false so loop stops
						if success then							-- keep moving forward
							if blockTypeF:find("log") then 		-- tree found
								lib.harvestTree(blockTypeF)	
							end
																-- else blockTypeF is air/leaves  border has been checked
						end
					end
				end
			end													-- else success = true, 1 block moved so continue
		end
	end
	
	function lib.isBorder(blockType)
		--[[ Is the block log, dirt, grass_block, vegetation: non-border, or other:border]]
		if blockType == nil then 					-- not passed as parameter
			blockType = T:getBlockType("forward")
		end
		if blockType == "" then 					-- air ahead: not border
			return false, ""
		else										-- could be border or other
			if blockType:find("dirt") ~= nil or blockType:find("grass_block") ~= nil or blockType:find("log") ~= nil then -- either dirt, grass block or log
				return false, blockType				-- dirt, grass, log: not border
			end
			if T:isVegetation(blockType) then 		-- vegetation found: not border
				return false, blockType
			end
		end
		return true, blockType						-- dirt, grass_block, log and vegetation eliminated:must be border
	end
	
	function lib.inPosition()
		--[[ check if in lower left corner ]]
		local inPosition = true 		-- assume correct
		if not turtle.detectDown() then	-- hanging in mid-air
			return false
		end
		T:turnLeft(1)
		if lib.isBorder() then
			-- so far so good
			T:turnLeft(1)
			if not lib.isBorder() then 	-- not in correct place
				inPosition = false
			end
			T:turnRight(2) 				-- return to original position
		else
			inPosition = false
			T:turnRight(1) 				-- return to original position
		end
		return inPosition
	end
	
	function lib.findBorder()
		--[[ assume started after reset. if log above harvest tree else return to ground. Find starting corner]]
		local blockType = T:getBlockType("up")					-- dig any logs above, return to ground
		local log = "sapling"
		if blockType:find("log") ~= nil then					-- originally felling a tree so complete it
			log = lib.getSaplingFromLogType(blockType)
			local double = T:harvestTree(false, false, "up")	-- assume single tree, will auto-discover
			lib.plantSapling(log, double)
		else													-- no log above so go downm
			blockType = lib.moveDown()							-- return to ground (or vegetation)
		end
		lib.safeMove()											-- move forward until border reached
		T:turnRight(1)
		lib.safeMove()											-- move forward until second border reached
		T:turnRight(1)											-- should now be in correct position
		lib.emptyInventory()									-- empty inventory if above a chest
	end
	
	local direction = "r"
	local blockTypeF = ""
	local success = false
	if not lib.inPosition() then 
		lib.findBorder()
	end
	local secondBorderFound = false
	while not secondBorderFound do
		lib.safeMove()														-- moves forward until reaches border forward or below
		lib.turn(direction)													-- turn r or l. direction is not changed
		success, blockTypeF = lib.isBorder()								-- no blockType passed as parameter so will return current block in new forward direction
		if success then
			secondBorderFound = true										-- game over
		elseif blockTypeF:find("log") ~= nil then							-- tree in front
			lib.harvestTree(blockTypeF)
		elseif blockTypeF == "" or blockTypeF:find("leaves") ~= nil then	-- air or leaves in front
			T:forward(1)													-- move forward 1 block
			lib.moveDown()													-- go down if required
		elseif	blockTypeF:find("dirt") ~= nil or
				blockTypeF:find("grass_block") ~= nil or
				T:isVegetation(blockTypeF) then								-- dirt, grass_block or vegetation in front
			blockTypeF = lib.moveUp(blockTypeF)								-- move up until air or border ahead.
			if lib.isBorder(blockTypeF) then								-- border ahead
				secondBorderFound = true
			else															-- air ahead									
				T:forward(1)												-- move forward 1 block
			end
		end
		direction = lib.turn(direction)										-- turn r or l. direction is changed to opposite
	end
	lib.moveDown()															-- return to ground level
	lib.emptyInventory()
end

function clearArea(width, length, useDirt)
	if useDirt == nil then
		useDirt = true
	end
	local evenWidth = false
	local evenHeight = false
	local loopWidth
	-- go(path, useTorch, torchInterval, leaveExisting)
	if width % 2 == 0 then
		evenWidth = true
		loopWidth = width / 2
	else
		loopWidth = math.ceil(width / 2)
	end
	if length % 2 == 0 then
		evenHeight = true
	end
	turtle.select(1)
	-- clear an area between 2 x 4 and 32 x 32
	-- if width is even no, then complete the up/down run
	-- if width odd no then finish at top of up run and reverse
	-- should be on flat ground, check voids below, harvest trees
	for x = 1, loopWidth do
		-- Clear first column (up)
		for y = 1, length do
			if useDirt then
				if not turtle.detectDown() then
					T:place("minecraft:dirt", -1, "down", true)
				else --if not water, dirt, grass , stone then replace with dirt
					blockType, blockModifier = T:getBlockType("down")
					if blockType ~= "" then
						if blockType ~= "minecraft:dirt" and blockType ~= "minecraft:grass_block" then
							turtle.digDown()
							T:place("minecraft:dirt", -1, "down", true)
						end
					end
				end
			end
			if y < length then
				T:go("F1+1", false,0,false)
			end
		end
		-- clear second column (down)
		if x < loopWidth or (x == loopWidth and evenWidth) then -- go down if on width 2,4,6,8 etc
			T:go("R1F1+1R1", false,0,false)
			for y = 1, length do
				if useDirt then
					if not turtle.detectDown() then
						T:place("minecraft:dirt", -1, "down", true)
					else
						blockType, blockModifier = T:getBlockType("down")
						if blockType ~= "" then
							if blockType ~= "minecraft:dirt" and blockType ~= "minecraft:grass_block" then
								turtle.digDown()
								T:place("minecraft:dirt", -1, "down", true)
							end
						end
					end
				end
				if y < length then
					T:go("F1+1", false, 0, false)
				end
			end
			if x < loopWidth then 
				T:go("L1F1+1L1", false,0,false)
			else
				T:turnRight(1)
				T:forward(width - 1)
				T:turnRight(1)
			end
		else -- equals width but is 1,3,5,7 etc
			T:turnLeft(2) --turn round 180
			T:forward(length - 1)
			T:turnRight(1)
			T:forward(width - 1)
			T:turnRight(1)
		end
	end
end

local function clearRectangle(width, length, up, down)
	local lib = {}
	function lib.UpDown(length)
		for l = 1, length do
			T:go("x0x2F1x0x2")
		end
	end
	
	function lib.Up(length)
		for l = 1, length do
			T:go("x0F1x0")
		end
	end
	
	function lib.Down(length)
		for l = 1, length do
			T:go("x2F1x2")
		end
	end
	
	function lib.Forward(length)
		T:forward(length)
	end
	
	if up == nil then --remove blocks above
		up = false
	end
	if down == nil then --remove blocks below
		down = false
	end
	-- could be 1 wide x xx length (trench) up and return
	-- could be 2+ x 2+
	-- even no of runs return after last run
	-- odd no of runs forward, back, forward, reverse and return
	turtle.select(1)
	if width == 1 then 					-- single block trench ahead only
		if up and down then				-- single block wide trench dig up and down = 3 blocks deep
			T:up(1)
			lib.UpDown(length - 1)
			T:down(1)
		elseif up then					-- single block wide trench dig up = 2 blocks deep
			lib.Up(length - 1)
		elseif down then				-- single block wide trench dig down = 2 blocks deep
			lib.Down(length - 1)
		else 							-- single block wide = 1 block deep
			lib.Forward(length - 1)
		end
		T:turnRight(2)					-- turn at the top of the run
		T:forward(length - 1)			-- return to start
		T:turnRight(2)					-- turn round to original position
	else 								-- width 2 or more blocks
		local iterations = 0 			-- width = 2, 4, 6, 8 etc
		if width % 2 == 1 then  		-- width = 3, 5, 7, 9 eg width 7
			iterations = (width - 1) / 2 -- iterations 1, 2, 3, 4 for widths 3, 5, 7, 9
		else
			iterations = width / 2		-- iterations 1, 2, 3, 4 for widths 2, 4, 6, 8
		end
		for i = 1, iterations do 		-- eg 3 blocks wide, iterations = 1
			if up and down then							-- dig up and down
				lib.UpDown(length - 1)
				T:go("x0x2R1F1x0x2R1x0x2")				-- turn round
				lib.UpDown(length - 1)
			elseif up then								-- dig up
				lib.Up(length - 1)
				T:go("x0R1F1x0R1x0")
				lib.Up(length - 1)
			elseif down then							-- dig down
				lib.Down(length - 1)
				T:go("x2R1F1x2R1x2")
				lib.Down(length - 1)
			else										-- no digging up or down
				lib.Forward(length - 1)
				T:go("R1F1R1")
				lib.Forward(length - 1)
			end
			-- if 1 less than end, reposition for next run
			if i < iterations then
				T:go("L1F1L1", false, 0, false)
			end
		end
		if width % 2 == 1 then  -- additional run and return to base needed
			T:go("L1F1L1", false, 0, false)
			if up and down then
				lib.UpDown(length - 1)
			elseif up then
				lib.Up(length - 1)
			elseif down then
				lib.Down(length - 1)
			else
				lib.Forward(length - 1)
			end
			T:turnRight(2)
			T:forward(length - 1)
		end
		T:go("R1F"..width - 1 .."R1", false, 0, false)
	end
end

local function clearPerimeter(width, length, up, down)
	local lib = {}
	function lib.UpDown(length)
		for l = 1, length do
			T:go("x0x2F1x0x2")
		end
	end
	
	function lib.Up(length)
		for l = 1, length do
			T:go("x0F1x0")
		end
	end
	
	function lib.Down(length)
		for l = 1, length do
			T:go("x2F1x2")
		end
	end
	
	function lib.Forward(length)
		T:forward(length)
	end
	
	if up and down then
		for i = 1, 2 do
			lib.UpDown(length - 1)
			T:turnRight(1)
			lib.UpDown(width - 1)
			T:turnRight(1)
		end
	elseif up then
		for i = 1, 2 do
			lib.Up(length - 1)
			T:turnRight(1)
			lib.Up(width - 1)
			T:turnRight(1)
		end
	elseif down then
		for i = 1, 2 do
			lib.Down(length - 1)
			T:turnRight(1)
			lib.Down(width - 1)
			T:turnRight(1)
		end
	else
		for i = 1, 2 do
			lib.Forward(length - 1)
			T:turnRight(1)
			lib.Forward(width - 1)
			T:turnRight(1)
		end
	end
end

local function clearBuilding(width, length, height, direction, withCeiling, withFloor)
	-- clearBuilding(width, length, height, size, withCeiling, withFloor) -- eg 5, 5, 6, 0, true, true
	-- direction = Bottom->Top (0), Top->bottom(1)
	local lib = {}
	
	function lib.Layer1(direction, withCeiling, withFloor, width, length)
		-- 1 layer only
		if withFloor or withCeiling then	-- only one layer, so clearRectangle
			clearRectangle(width, length, false, false)
		else
			clearPerimeter(width, length, false, false) -- pass
		end
		return 0
	end
	
	function lib.Layer2(direction, withCeiling, withFloor, width, length)
		-- 2 layers. Stay on 1 layer and dig up / down as needed
		if withFloor or withCeiling then
			clearRectangle(width, length, withCeiling, withFloor)	-- clear area and dig up / down as required
			--[[if direction == 0 then
				clearRectangle(width, length, true, false)	-- clear area and dig up
			else
				clearRectangle(width, length, false, true)	-- clear area and dig down
			end]]
		else
			if direction == 0 then
				clearPerimeter(width, length, true, false)	-- clear perimeter and dig up
			else
				clearPerimeter(width, length, false, true)	-- clear perimeter and dig down
			end
		end
		return 0
	end
	
	function lib.Layer3(direction, withCeiling, withFloor, width, length)
		-- move up / down 1 block and dig up and down
		if direction == 0 then
			T:up(1)
		else
			T:down(1)
		end
		if withFloor or withCeiling then
			clearRectangle(width, length, true, true)
		else
			clearPerimeter(width, length, true, true)
		end
		return 1
	end
	
	function lib.Layer4(direction, withCeiling, withFloor, width, length)
		-- clear current level
		lib.Layer1(direction, withCeiling, withFloor, width, length)
		if direction == 0 then
			T:up(1)
			lib.Layer3(direction, withCeiling, false, width, length)
		else
			T:down(1)
			lib.Layer3(direction, false, withFloor, width, length)
		end
		return 2
	end
	
	function lib.Layer5(direction, withCeiling, withFloor, width, length)
		-- clear first 2 levels, then next 3
		if direction == 0 then
			T:up(1)
			lib.Layer2(direction, false, withFloor, width, length)
			T:up(1)
			lib.Layer3(direction, withCeiling, false, width, length)
		else
			T:down(1)
			lib.Layer2(direction, withCeiling, false, width, length)
			T:down(1)
			lib.Layer3(direction, false, withFloor, width, length)
		end
		return 3
	end
	
	function lib.Layer6(direction, withCeiling, withFloor, width, length)
		-- clear first 3 levels, then next 3
		if direction == 0 then
			lib.Layer3(direction, false, withFloor, width, length)
			T:up(2)
			lib.Layer3(direction, withCeiling, false, width, length)
		else
			lib.Layer3(direction, withCeiling, false, width, length)
			T:down(2)
			lib.Layer3(direction, false, withFloor, width, length)
		end
		return 4
	end
	local travel = 0
	if height == 1 then
		lib.Layer1(direction, withCeiling, withFloor, width, length)
	elseif height == 2 then
		lib.Layer2(direction, withCeiling, withFloor, width, length)
	elseif height == 3 then
		travel = lib.Layer3(direction, withCeiling, withFloor, width, length)
	elseif height == 4 then
		travel = lib.Layer4(direction, withCeiling, withFloor, width, length)
	elseif height == 5 then
		travel = lib.Layer5(direction, withCeiling, withFloor, width, length)
	elseif height == 6 then
		travel = lib.Layer6(direction, withCeiling, withFloor, width, length)
	else -- height 7 or more
		-- each iteration clears 3 layers
		local iterations = math.floor(height / 3) 	-- eg height = 7, iterations = 2
		local remaining = height % (iterations * 3)	-- eg height = 7, 1 remaining
		for i = 1, iterations do
			travel = travel + lib.Layer3(direction, withCeiling, withFloor, width, length)
			if direction == 0 then -- reset floor / ceiling once completed
				withFloor = false
				T:up(2)
			else
				withCeiling = false
				T:down(2)
			end
			travel = travel + 2
		end
		if remaining == 1 then
			travel = travel + lib.Layer1(direction, withCeiling, withFloor, width, length)
		elseif remaining == 2 then
			travel = travel + lib.Layer2(direction, withCeiling, withFloor, width, length)
		elseif remaining == 3 then
			travel = travel + lib.Layer3(direction, withCeiling, withFloor, width, length)
		elseif remaining == 4 then
			travel = travel + lib.Layer4(direction, withCeiling, withFloor, width, length)
		elseif remaining == 5 then
			travel = travel + lib.Layer5(direction, withCeiling, withFloor, width, length)
		elseif remaining == 6 then
			travel = travel + lib.Layer6(direction, withCeiling, withFloor, width, length)
		end
	end
	if direction == 0 and travel > 0 then
		T:down(travel)
	else
		T:up(travel)
	end
end

function clearMineshaft(equippedRight, equippedLeft, inInventory)
	local lib = {}
	function lib.checkCobweb(direction, inInventory)
		if inInventory == "minecraft:diamond_sword" then -- using a sword
			local side = "left"
			local item = T:getBlockType(direction)
			if item == "minecraft:cobweb" then
				--clsTurtle.equip(self, side, useItem, useDamage)
				if equippedRight == "minecraft:diamond_pickaxe" then
					side = "right"
				end
				T:equip(side, "minecraft:diamond_sword")
				T:dig(direction)
				T:equip(side, "minecraft:diamond_pickaxe")
			else
				T:dig(direction)
			end
		else
			T:dig(direction)
		end
	end

	-- check position by rotating until facing away from wall
	length = 0
	torch = 0
	turns = 0
	doContinue = true
	while not turtle.detect() do
		T:turnRight(1)
		turns = turns + 1
		if turns > 4 then
			doContinue = false
			print("I am not facing a wall. Unable to continue")
		end
	end
	if doContinue then --facing wall
		T:turnRight(2)
		-- move forward until obstructed, digging up/down. place torches
		while not turtle.detect() do
			lib.checkCobweb("up", inInventory) -- dig cobweb or any other block up
			lib.checkCobweb("down", inInventory) -- dig cobweb or any other block down
			length = length + 1
			torch = torch + 1
			if torch == 8 then
				torch = 0
				T:place("minecraft:torch", -1, "down", false) ---(self, blockType, damageNo, direction, leaveExisting, signText)
			end
			lib.checkCobweb("forward", inInventory) -- dig cobweb or any other block in front
			T:forward(1)
		end
		-- turn right, forward, right, return to start with up/down dig
		
		T:go("R1F1R1")
		for i = 1, length, 1 do
			lib.checkCobweb("up", inInventory) -- dig cobweb or any other block up
			lib.checkCobweb("down", inInventory) -- dig cobweb or any other block down
			lib.checkCobweb("forward", inInventory) -- dig cobweb or any other block in front
			T:forward(1)
		end
		-- move to other wall and repeat.
		T:go("R1F2R1")
		for i = 1, length, 1 do
			lib.checkCobweb("up", inInventory) -- dig cobweb or any other block up
			lib.checkCobweb("down", inInventory) -- dig cobweb or any other block down
			lib.checkCobweb("forward", inInventory) -- dig cobweb or any other block in front
			T:forward(1)
		end
		lib.checkCobweb("up", inInventory) -- dig cobweb or any other block up
		lib.checkCobweb("down", inInventory) -- dig cobweb or any other block down
	end
end

function clearMonumentLayer(width, length, size)
	local up = true
	local down = true
	if size == 0 then
		up = false
		down = false
	end
	-- send turtle down until it hits bottom
	-- then clear rectangle of given size
	-- start above water, usually on cobble scaffold above monument
	if T:detect("down") then -- in case not over wall
		T:forward(1)
	end
	height = 1
	-- go down until solid block detected
	while clearVegetation("down") do
		T:down(1)
		height = height + 1
	end
	T:down(1)
	height = height + 1
	clearRectangle(width, length, up, down)
	T:up(height - 1)
end

function clearSeaweed(width, length)
	-- move over water
	turtle.select(1)
	T:clear()
	print("Checking water length")
	while not clearVegetation("down") do
		T:forward(1)
	end
	local odd = false
	local calcLength = 0
	while clearVegetation("down") do
		T:forward(1)
		calcLength = calcLength + 1
	end
	-- now check on correct side: wall to the right
	print("Checking position")
	T:go("R2F1R1F1") -- over ? wall
	if clearVegetation("down") then -- over water, so return to start on correct side
		T:go("R2F1R1F"..calcLength - 1 .."R2")
	else
		T:go("R2F1R1") -- over water
	end
	if calcLength % 2 == 1 then
		odd = true
	end
	local forward = true
	--print("calcLength:"..calcLength.. " odd:"..tostring(odd))
	for w = 1, width do
		for l = 1, calcLength, 2 do -- 1,3,5,7 etc length 3 runs 1, 5 runs 2 etc
			local depth = 0
			if l == 1 or (l % 2 == 1 and l == calcLength) then
				if forward then
					T:turnLeft(1)
				else
					T:turnRight(1)
				end
			else
				T:turnLeft(2)
			end
			-- go down destroying vegetation until on floor, checking in front at same time
			while clearVegetation("down") do
				clearVegetation("forward")
				T:down()
				depth = depth + 1
			end
			-- if slab at bottom, cover with solid block
			if string.find(T:getBlockType("down"), "slab") then
				T:up(1)
				T:place("minecraft:sand", "down")
				depth = depth + 1
			end
			if l == 1 or (l % 2 == 1 and l == calcLength) then
				if forward then
					T:turnRight(1)
				else
					T:turnLeft(1)
				end
			else
				T:turnLeft(2)
			end
			for d = depth, 1, -1 do
				clearVegetation("forward")
				T:up(1)
			end
			-- first corner complete, back at surface above water
			if l < calcLength then
				T:forward(2)
			end
		end
		if forward then
			T:go("L1F1L1")
		else
			T:go("R1F1R1")
		end
		forward = not forward
		if not clearVegetation("down") then -- reached  far wall
			break
		end
	end
end

function clearMountainSide(width, length, size)
	local lib = {}
	function lib.excavate(blocksFromOrigin, going, length, digDown)
		local firstUp = 0
		for i = 1, length do
			-- record first block dug above
			if turtle.digUp() then
				if firstUp == 0 then
					firstUp = i -- will record first successful dig up
				end
			end
			if digDown then
				turtle.digDown()
			end
			T:forward(1)
			if going then
				blocksFromOrigin = blocksFromOrigin + 1
			else
				blocksFromOrigin = blocksFromOrigin - 1
			end
		end
		
		return blocksFromOrigin, firstUp
	end
	
	function lib.cutSection(blocksFromOrigin, going, length, firstUp)
		local height = 0
		local digDown = false
		blocksFromOrigin, firstUp = lib.excavate(blocksFromOrigin, going, length, digDown)
		-- while at least 1 block dug above do
		while firstUp > 0 do
			if digDown then
				turtle.digDown()
			else
				digDown = true
			end
			T:go("R2U1x1U1x1U1x1x0") -- go up 3 turn round
			going = not going
			height = height + 3
			if firstUp > 1 then
				length = length - firstUp + 1
			end
			-- go forward length digging up/down
			blocksFromOrigin, firstUp = lib.excavate(blocksFromOrigin, going, length,  true)
		end
		T:down(height)
		
		return blocksFromOrigin, going
	end
	
	local originalLength = length
	local going = true
	local firstUp = 0
	local blocksFromOrigin = 0
	--T:forward(1) -- get into position
	blocksFromOrigin, going = lib.cutSection(blocksFromOrigin, going, length, firstUp)
	if width > 1 then --move left/right and repeat
		for i = 2, width do
			if going then
				T:turnRight(2)			
			end
			if blocksFromOrigin > 0 then
				T:forward(blocksFromOrigin)
			end
			T:turnRight(2)
			blocksFromOrigin = 0
			if size == 0 then --Left <- Right
				T:go("L1F1R1")
			else
				T:go("R1F1L1")
			end
			going = true
			blocksFromOrigin, going = lib.cutSection(blocksFromOrigin, going, length, firstUp)
		end
	end
end

function clearSandWall(length)
	--dig down while on top of sand/soul_sand
	local height = 0
	turtle.select(1)
	T:clear()
	print("Checking sand length")
	local search = 0
	local blockType = T:getBlockType("down")
	while blockType ~= "minecraft:sand" and blockType ~= "minecraft:soul_sand" do --move forward until sand detected or 3 moves
		T:forward(1)
		search = search + 1
		blockType = T:getBlockType("down")
		if search > 3 then
			break
		end
	end
	if search <= 3 then
		local calcLength = 0
		blockType = T:getBlockType("down")
		while blockType == "minecraft:sand" or blockType == "minecraft:soul_sand" do
			T:forward(1)
			calcLength = calcLength + 1
			blockType = T:getBlockType("down")
		end
		T:go("R2F1") -- over sand
		blockType = T:getBlockType("down")
		while blockType == "minecraft:sand" or blockType == "minecraft:soul_sand" do
			T:down(1)
			height = height + 1
			blockType = T:getBlockType("down")
		end
		-- repeat length times
		for i = 1, calcLength do
			--if sand in front, dig
			while turtle.detect() do
				blockType = T:getBlockType("forward")
				if blockType == "minecraft:sand" or blockType == "minecraft:soul_sand" then
					T:dig("forward")
				else --solid block, not sand so move up
					T:up(1)
					height = height - 1
				end
			end
			--move forward
			if i < calcLength then
				T:forward(1)
				blockType = T:getBlockType("down")
				while blockType == "minecraft:sand" or blockType == "minecraft:soul_sand" do
					T:down(1)
					height = height + 1
					blockType = T:getBlockType("down")
				end
			end
		end
		-- go up, but reverse if block above
		for i = 1, height do
			if not turtle.detectUp() then
				T:up(1)
			else
				T:back(1)
			end
		end
		T:go("R2")
	end
end

function clearSandCube(width, length)
	--go down to bottom of sand
	turtle.select(1)
	while T:getBlockType("down") == "minecraft:sand" do
		T:down(1)
	end
	clearBuilding(width, length, 0, false)
end

local function clearSolid(width, length, height, direction)
	-- direction = Bottom->Top (0), Top->bottom(1)
	local lib ={}
	
	function lib.level(width, length)
		clearRectangle(width, length, false, false)
	end
	
	function lib.levelUp(width, length)
		clearRectangle(width, length, true, false)
	end
	
	function lib.levelUpDown(width, length)
		clearRectangle(width, length, true, true)
	end
	
	function lib.levelDown(width, length)
		clearRectangle(width, length, false, true)
	end
	
	function lib.level1()
		lib.level(width, length)
		return 0
	end
	
	function lib.level2()
		if direction == 0 then
			lib.levelUp(width, length)
		else
			lib.levelDown(width, length)
		end
		return 0
	end
	
	function lib.level3()
		if direction == 0 then
			T:up(1)
		else
			T:down(1)
		end
		lib.levelUpDown(width, length)lib.levelUpDown(width, length)
		return 1
	end
	
	function lib.level4()
		if direction == 0 then
			T:up(1)
			lib.levelUpDown(width, length)
			T:up(2)
		else
			T:down(1)
			lib.levelUpDown(width, length)
			T:down(2)
		end
		lib.level(width, length)
		return 3
	end
	
	function lib.level5()
		if direction == 0 then
			T:up(1)
			lib.levelUpDown(width, length)
			T:up(2)
			lib.levelUp(width, length)
		else
			T:down(1)
			lib.levelUpDown(width, length)
			T:down(2)
			lib.levelDown(width, length)
		end
		return 4
	end
	
	function lib.level6()
		if direction == 0 then
			T:up(1)
			lib.levelUpDown(width, length)
			T:up(3)
			lib.levelUpDown(width, length)
		else
			T:down(1)
			lib.levelUpDown(width, length)
			T:down(3)
			lib.levelUpDown(width, length)
		end
		return 5
	end
	
	if direction == nil then
		direction = 0
	end
	if height <= 0 then --move up until no more solid
		-- starts on level 1
		if direction == 0 then
			T:up(1)
		else
			T:down(1)
		end
		local level = 2
		local itemsOnBoard = T:getTotalItemCount()
		repeat
			lib.levelUpDown()
			T:sortInventory()
			if direction == 0 then
				T:up(3)
			else
				T:down(3)
			end
			height = height + 3
		until T:getFirstEmptySlot() == 0  or T:getTotalItemCount() == itemsOnBoard-- nothing collected or full inventory
	elseif height == 1 then --one layer only
		height = lib.level1(width, length)
	elseif height == 2 then --2 layers only
		height = lib.level2()
	elseif height == 3 then --3 layer only
		height = lib.level3()
	elseif height == 4 then --4 layer only
		height = lib.level4()
	elseif height == 5 then --5 layer only
		height = lib.level5()
	elseif height == 6 then --6 layer only
		height = lib.level6()
	else -- 7 or more levels
		local remaining = height - 7
		if direction == 0 then
			T:up(1)
		else
			T:down(1)
		end
		for i = 1, height - 7 do
			if direction == 0 then
				T:up(3)
			else
				T:down(3)
			end
			lib.levelUpDown(width, length)
		end
		height = height - 7
		if remaining == 1 then
			height = height + lib.level1(width, length)
		elseif remaining == 2 then
			height = height + lib.level2(width, length)
		elseif remaining == 3 then
			height = height + lib.level3(width, length)
		elseif remaining == 4 then
			height = height + lib.level4(width, length)
		elseif remaining == 5 then
			height = height + lib.level5(width, length)
		elseif remaining == 6 then
			height = height + lib.level6(width, length)
		end
	end
	if direction == 0 then
		T:down(height)
	else
		T:up(height)
	end
end

function clearWall(width, length, height, direction)
	-- direction = Bottom->Top (0), Top->bottom(1)
	-- width preset to 1
	if direction == nil then direction = 0 end-- bottom to top
	local atStart = true
	local offset = height - 2
	-- go(path, useTorch, torchInterval, leaveExisting)
	local evenHeight = false
	if height % 2 == 0 then
		evenHeight = true
	end
	turtle.select(1)
	-- dig along and up/down for specified length
	if evenHeight then --2, 4 , 6 etc
		for h = 2, height, 2 do
			for i = 1, length - 1 do
				T:go("x0F1", false, 0, false)
			end
			if h < height then
				if direction == 0 then --bottom to top
					T:go("R2U2", false, 0, false)
				else
					T:go("R2D2", false, 0, false)
				end
			end
			atStart = not atStart
		end
	else
		offset = height - 1
		for h = 1, height, 2 do
			if h == height then
				T:go("F"..tostring(length - 1), false, 0, false)
			else
				for i = 1, length - 1 do
					if direction == 0 then --bottom to top
						T:go("x0F1", false, 0, false)
					else
						T:go("x2F1", false, 0, false)
					end
				end
			end
			atStart = not atStart
			if h < height then
				if direction == 0 then --bottom to top
					T:go("R2U2", false, 0, false)
				else
					T:go("R2D2", false, 0, false)
				end
			end
		end
	end
	if evenHeight then
		if direction == 0 then
			T:go("x0", false, 0, false)
		else
			T:go("x2", false, 0, false)
		end
	end
	if direction == 0 then
		T:go("R2D"..offset, false, 0, false)
	else
		T:go("R2U"..offset, false, 0, false)
	end
	if not atStart then
		T:go("F"..tostring(length - 1).."R2", false, 0, false)
	end
end

function clearVegetation(direction)
	local isAirWaterLava = true
	-- blockType, blockModifier, data
	local blockType, blockModifier = T:getBlockType(direction)
	if blockType ~= "" then --not air
		if T:isVegetation(blockType) then
			T:dig(direction)
		elseif blockType:find("water") == nil
			   and blockType:find("lava") == nil
			   and blockType:find("bubble") == nil
			   and blockType ~= "minecraft:ice" then
			-- NOT water, ice or lava 
			isAirWaterLava = false -- solid block
		end
	end
	
	return isAirWaterLava --clears any grass or sea plants, returns true if air or water, bubble column or ice
end

function createAutoTreeFarm()
	lib = {}
	
	function lib.fillWaterBuckets()
		T:place("minecraft:bucket", -1, "down", false)
		sleep(0.5)
		T:place("minecraft:bucket", -1, "down", false)
	end
	
	createWaterSource(1)
	-- clsTurtle.go(path, useTorch, torchInterval, leaveExisting)
	-- place chest and hopper	
	T:go("x0F1x0F1x0F1x0F1R1")
	for i = 1, 4 do
		T:go("D1R1C1R1C1R1C1R1")
	end
	T:up(1)
	T:place("minecraft:chest", -1, "down", false)
	T:go("F1x0D1F1x0R2", false, 0, true)
	T:place("minecraft:hopper", -1, "forward", false)
	-- dig trench and ensure base is solid
	T:go("U1R2X7U1", false, 0, true)
	T:place("minecraft:water_bucket", -1, "down", false) -- collection stream
	T:go("F1X7U1", false, 0, true)
	T:place("minecraft:water_bucket", -1, "down", false) -- upper collection stream
	T:go("U1F1R1C2F1R1C2") --now on corner
	for i = 1, 14 do --place cobble
		T:go("F1C2", false, 0, false)
	end
	T:go("F4R1F2C2R2C1R2")
	for i = 1, 8 do --place cobble
		T:go("F1C2", false, 0, false)
	end
	T:turnRight(1)
	for i = 1, 18 do --place cobble
		T:go("F1C2", false, 0, false)
	end
	T:turnRight(1)
	for i = 1, 8 do --place cobble
		T:go("F1C2", false, 0, false)
	end
	T:go("R1F1R1D1") -- ready to clear ground inside cobble wall
	for i = 1, 17 do
		T:go("C2F1C2F1C2F1C2F1C2F1C2F1C2F1C2", false, 0, true)
		if i < 17 then
			if i % 2 == 1 then -- odd no
				T:go("L1F1L1")
			else
				T:go("R1F1R1")
			end
		end
	end
	T:go("U1R2F10R2") -- over pond
	lib.fillWaterBuckets()
	for i = 0, 16, 2 do
		T:go("F10R1")
		if i > 0 then
			T:go("F"..i)
		end
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("F1R2")
		if i < 16 then
			T:place("minecraft:water_bucket", -1, "down", false)
		end
		T:go("F"..i + 1 .."L1")
		T:go("F10R2")
		lib.fillWaterBuckets()
	end
	-- place dirt/torch/sapling
	T:go("F1U1R1F1L1")
	for i = 1, 7 do
		T:go("F6R1F1L1")
		for j = 1, 3 do
			T:place("minecraft:dirt", -1, "forward", false)
			T:up(1)
			T:place("sapling", -1, "forward", false)
			T:down(1)
			turtle.back()
			T:place("minecraft:torch", -1, "forward", false)
			turtle.back()
		end
		if i < 7 then
			T:go("R1F1L1")
		end
	end
	T:go("L1F12")
	T:place("minecraft:chest", -1, "up", false)
	T:go("R1F1L1")
	T:place("minecraft:chest", -1, "up", false)
	T:go("F1U1R1F1R1F1L1")
	T:clear()
	print("Auto TreeFarm completed")
	print("\nDO NOT PLACE ANY TORCHES OR OTHER")
	print("BLOCKS ON THE COBBLE PERIMETER!")
	print("\nUse option 5 Manage Auto Tree farm")
	print("to setup monitoring\n\nEnter to continue")
	read()
end

local function createBoatLift(state, side, height) -- state:0=new, size:1=extend, side:0=left, 1=right
	--[[ Legacy version (turtle deletes water source)
	Place turtles on canal floor, with sources ahead
	facing back wall preferred, but not essential
	1.14 + (turtle can be waterlogged)
	Place same as legacy but inside source blocks
	]]
	local lib ={}
	
	function lib.checkSource()
		local isWater = false
		local isSource = false
		T:dig("forward")						-- break frozen source block
		isWater, isSource = T:isWater("forward")
		return isSource
	end
	
	function lib.findPartner()
		local block = T:getBlockType("forward")
		while block:find("turtle") == nil do -- not found partner
			 if side == 0 then
				turtle.turnRight()
			 else
				turtle.turnLeft()
			 end
			 sleep(0.5)
			 block = T:getBlockType("forward")
		end
	end
	
	function lib.returnToWork(level)
		if level > 0 then
			for i = 1, level do
				T:place("minecraft:water_bucket", -1, "down")
				sleep(0.5)
				local _, isSource = T:isWater("down")
				while not isSource do
					print("Waiting for source...")
					sleep(0.5)
					_, isSource = T:isWater("down")
				end
				T:place("minecraft:bucket", -1, "down")
				T:up(1)
			end
		end
	end
	
	function lib.getToWater()
		local level = 0
		local isWater, isSource, isIce = T:isWater("down")
		while not isSource and isWater do -- water but not source
			T:down(1)
			level = level + 1
			isWater, isSource, isIce = T:isWater("down")
		end
		if isIce then T:dig("down")	end									-- break frozen source block
		if not isWater then
			isWater, isSource, isIce = T:isWater("up")
			if isSource then
				T:place("minecraft:bucket", -1, "up")
			end
		else
			T:place("minecraft:bucket", -1, "down")
		end
		lib.returnToWork(level)
	end
	
	function lib.refill()
		local s1, s2, buckets = T:getItemSlot("minecraft:bucket", -1) 	-- slotData.lastSlot, slotData.leastModifier, total, slotData
		local level = 0
		if buckets > 0 then												-- at least 1 empty bucket
			local isWater, isSource, isIce = T:isWater("down")
			if isIce then T:dig("down")	end								-- break frozen source block
			if T:place("minecraft:bucket", -1, "down") then				-- get water from below.
				sleep(0.5)
			else
				lib.getToWater()
			end
		end
		s1, s2, buckets = T:getItemSlot("minecraft:bucket", -1)
		if buckets > 0 then	-- at least 1 empty bucket
			if isIce then T:dig("down")	end								-- break frozen source block
			if not T:place("minecraft:bucket", -1, "down") then			-- get water from below
				lib.getToWater()
			end
		end
	end
	
	function lib.reset()
		redstone.setAnalogueOutput("front", 0)
		redstone.setOutput("front", false)
	end
	
	function lib.sendSignal()
		-- wait until turtle detected in front
		-- if detected set signal to 15 and pause 0.5 secs
		-- if recieves 15 from other turtle, continue
		-- if recieves 7 from other turtle, job done
		local block = T:getBlockType("forward")
		if block:find("turtle") == nil then -- not found partner
			print("Waiting for partner turtle...")
			print("If not facing towards partner")
			print("please restart this program")
			sleep(1)
		else
			redstone.setAnalogueOutput("front", 0)
			redstone.setOutput("front", false)
			if redstone.getAnalogueInput("front") == 15 then
				redstone.setOutput("front", true)
				print("Binary sent to partner turtle...")
				sleep(0.5)
				return true
			else
				print("Signal 15 sent to partner turtle...")
				redstone.setAnalogueOutput("front", 15)
				sleep(0.5)
			end
			redstone.setAnalogueOutput("front", 0)
			if redstone.getInput("front")  or redstone.getAnalogueInput("front") == 15 then
				 return true
			end
			return false
		end
		return false
	end
	
	function lib.legacyLeft(layer)
		lib.refill()
		T:go("U1")	
		T:place("minecraft:water_bucket", -1, "down")	-- place below. facing partner
		T:go("L1F1")									-- move to back, facing back
		T:place("minecraft:water_bucket", -1, "down")	-- place water below
		T:go("C1")										-- facing back, build back wall
		T:go("L1C1")									-- facing side, build side
		T:go("L1F1R1C1")								-- move towards front, build side wall
		if layer == 1 then								-- do not replace slab on layer 1
			T:go("L2")
		else
			T:go("L1C1L1")								-- build front wall, turn to face partner
		end
	end
	
	function lib.left(layer)
		lib.refill()
		T:go("D1")	
		T:place("minecraft:water_bucket", -1, "up")	-- place above. facing partner
		T:go("L1F1")								-- move to back, facing back
		T:place("minecraft:water_bucket", -1, "up")	-- place water above
		lib.refill()
		T:go("U2C1")								-- up 2;facing back, build back wall
		T:go("L1C1")								-- facing side, build side
		T:go("L1F1R1C1")							-- move towards front, build side wall
		if layer == 1 then							-- do not replace slab on layer 1
			T:go("L2")
		else
			T:go("L1C1L1")							-- build front wall, turn to face partner
		end
		local _, isSource, isIce = T:isWater("down")
		if isIce then T:dig("down")	end				-- break frozen source block
		if not isSource then
			lib.getToWater()
		end
	end
	
	function lib.right(layer)
		lib.refill()
		T:go("D1")	
		T:place("minecraft:water_bucket", -1, "up")	-- place above. facing partner
		T:go("R1F1")								-- move to back, facing back
		T:place("minecraft:water_bucket", -1, "up")	-- place water above
		lib.refill()
		T:go("U2C1")								-- up 2;facing back, build back wall
		T:go("R1C1")								-- facing side, build side
		T:go("R1F1L1C1")							-- move towards front, build side wall
		if layer == 1 then							-- do not replace slab on layer 1
			T:go("R2")
		else
			T:go("R1C1R1")							-- build front wall, turn to face partner
		end	
		local _, isSource, isIce = T:isWater("down")
		if isIce then T:dig("down")	end				-- break frozen source block
		if not isSource then
			lib.getToWater()
		end
	end
	
	function lib.legacyRight(layer)
		lib.refill()
		T:go("U1")	
		T:place("minecraft:water_bucket", -1, "down")	-- place below. facing partner
		T:go("R1F1")									-- move to back, facing back
		T:place("minecraft:water_bucket", -1, "down")	-- place water below
		T:go("C1")										-- facing back, build back wall
		T:go("R1C1")									-- facing side, build side
		T:go("R1F1L1C1")								-- move towards front, build side wall
		if layer == 1 then								-- do not replace slab on layer 1
			T:go("R2")
		else
			T:go("R1C1R1")								-- build front wall, turn to face partner
		end												-- up 1;turn to face partner
	end
	
	function lib.top(height)
		if side == 0 then --left side
			T:go("x0L1F1x0L2 F2R1C1C0 D1C1") -- place side block; block above, move down; side block
			T:place("sign", -1, "up", true, "Lift Exit\n<--\n<--\nEnjoy the ride...") 
			T:go("L1F1L2C1 U2x1x0 L2F1x0L2")
		else
			T:go("x0R1F1x0R2 F2L1C1C0 D1C1") -- place side block, move up
			T:place("sign", -1, "up", true, "Lift Exit\n-->\n-->\nEnjoy the ride...") 
			T:go("R1F1R2C1 U2x1x0 R2F1x0R2")
		end
		for i = height, 1, -1 do
			T:go("x1D1")
		end
	end
	
	local A = "R"
	local B = "L"
	if side == 1 then -- right side turtle
		A = "L"
		B = "R"
	end
	lib.findPartner()
	T:go(B.."1")
	if not lib.checkSource() then
		print("No water source ahead.\nReposition or add source")
		error()
	end
	T:go(A.."1")
	lib.reset()
	while not lib.sendSignal() do end
	lib.reset()
	-- confirmed opposite and active
	if state == 0 then -- new lift
		if side == 0 then
			T:turnLeft(1)						-- face back
		else
			T:turnRight(1)						-- face back
		end
		T:go(B.."2F1U1"..A.."1C1") 				-- face front;forward 1;up 1; :face side ready for sign and slab placement
		T:place("slab", -1, "up")				-- slab above
		T:go("D1")								-- down 1
		T:place("sign", -1, "up", true, "Lift Entrance\nKeep pressing\nforward key!") 
		T:go(A.."1F1"..A.."2") 					-- face back; forward 1; :face front
		T:place("slab", -1, "forward") 			-- slab on ground at entrance
		T:go(A.."1")							-- face side
		T:place("minecraft:soul_sand", -1, "down", false) -- replace block below with soul sand
		T:go("C1"..A.."1F1C1"..B.."1C1") 		-- block to side; forward, block at back; block to side :face side
		T:place("minecraft:soul_sand", -1, "down", false) -- replace block below with soul sand
		T:go("U1C1"..A.."1C1"..A.."2F1"..A.."1C1"..A.."2") -- up 1; block to side; forward; block to side :face other turtle
		local isWater, isSource, isIce = T:isWater("down")
		if not isSource then -- no source below
			T:place("minecraft:water_bucket", -1, "down") 	-- refill source
			T:go(B.."1F1"..B.."2")
			T:place("minecraft:water_bucket", -1, "down") 	-- refill source
			T:go("F1"..B.."1")
			sleep(0.5)
			isWater, isSource = T:isWater("down")
			if isSource then
				if isIce then
					T:dig("down")								-- break frozen source block
				end
				T:place("minecraft:bucket", -1, "down") 		-- refill bucket
				sleep(0.5)
				T:place("minecraft:bucket", -1, "down") 		-- refill bucket
			end
		end
		lib.reset()
		while not lib.sendSignal() do end
		lib.reset()
		-- on layer 1  1 x source blocks below
		for layer = 1, height do
			if side == 0 then --left side
				if deletesWater then
					lib.legacyLeft(layer)
				else
					lib.left(layer)
				end
			else
				if deletesWater then
					lib.legacyRight(layer)
				else
					lib.right(layer)
				end
			end
			lib.reset()
			while not lib.sendSignal() do end
			lib.reset()
		end
		lib.top(height)	
	else -- extend lift
		-- turtles should be at front, facing back of lift
		-- signs behind them, water sources below
		-- will face each other on startup, exactly as if finished new lift
		for layer = 1, height do
			for layer = 1, height do
				if side == 0 then --left side
					if deletesWater then
						lib.legacyLeft(layer)
					else
						lib.left(layer)
					end
				else
					if deletesWater then
						lib.legacyRight(layer)
					else
						lib.right(layer)
					end
				end
				lib.reset()
				while not lib.sendSignal() do end
				lib.reset()
			end
		end
		lib.top(height + 1)	-- drop to below existing lift
		while turtle.down() do end -- drop to base canal
	end
end

function createBridge(numBlocks)
	for i = 1, numBlocks do
		T:go("m1", false, 0, true)
	end
	T:go("R1F1R1", false, 0, true)
	for i = 1, numBlocks do
		T:go("m1", false, 0, true)
	end
end

local function createBubbleLift(height)
	local lib = {}
	
	function lib.addLayer()
		T:go("F2 L1C1R1C1R1C1L1", false, 0, true)
		turtle.back()
		T:place("minecraft:water_bucket", -1, "forward")
		T:dig("up")	-- clear block above so completed lift can be found
		turtle.back()
		T:dig("up")	-- clear block above so completed lift can be found
		T:place("stone", -1, "forward")
	end
	
	function lib.addSign()
		turtle.back()
		T:place("minecraft:water_bucket", -1, "forward")
		T:go("L1B1")
		T:place("sign", -1, "forward")
	end
	
	function lib.buildLift(toHeight)
		local built = lib.goToWater() 		-- returns lift blocks already placed, total height of drop from starting point
		local toBuild = toHeight - built 	-- no of blocks remaining to increase lift size
		local water = 0
		while toBuild > 0 do 				-- at least 1 block height remaining
			water = lib.fillBuckets(toBuild, false) -- no of water buckets onboard (could be more than required)
			if water > toBuild then			-- more water than required
				water = toBuild				-- reduce to correct amount
			end
			while turtle.detect() do 		-- climb to top of existing lift
				turtle.up()
			end
			while water > 0 do
				lib.addLayer()
				water = water - 1
				T:up(1)
				toBuild = toBuild - 1
			end
			-- may still be some height to complete, but needs refill
			if toBuild > 0 then
				built = lib.goToWater() --return to source
				toBuild = toHeight - built
				--lib.fillBuckets(toBuild)
			end
		end
	end
	
	function lib.cleanUp(fromHeight)
		local plug = false
		T:turnRight(2)
		for i = 1, fromHeight do
			plug = false
			if turtle.detect() then
				plug = true
			end
			turtle.down()
			if plug then
				T:place("stone", -1, "up")
			end
		end
	end
	
	function lib.fillBuckets(toBuild, withSort)
		local emptySlots, water = lib.stackBuckets(withSort)-- gets no of empty slots + no of water buckets
		if water < toBuild then 					-- no of water buckets onboard less than required quantity
			for i = 1, toBuild do 					-- fill required no of buckets up to max space in inventory
				if emptySlots == 0 then 			-- inventory full
					break
				else
					if T:place("minecraft:bucket", -1, "down", false) then
						water = water + 1
						sleep(0.5)
					end
				end
				emptySlots = lib.getEmptySlots()
			end
		end
		
		return water
	end
	
	function lib.getEmptySlots()
		local empty = 0
		for i = 1, 16 do
			if turtle.getItemCount(i) == 0 then
				empty = empty + 1
			end
		end
		return empty
	end
	
	function lib.goToWater()
		local built = 0 -- measures completed lift height
		while turtle.down() do -- takes turtle to bottom of water source
			--height = height + 1
			if turtle.detect() then
				built = built + 1
			end
		end
		T:up(1) -- above watersource assuming it is 1-1.5 blocks deep
		-- height = height - 1
		-- built = built - 1 not required as next block is water source: not detected
		return built -- , height
	end
	
	function lib.stackBuckets(withSort)
		if withSort == nil then withSort = false end
		local data = {}
		local bucketSlot = 0
		local emptySlots = 0
		local water = 0
		if withSort then
			T:sortInventory()
		end
		for i = 1, 16 do
			-- find first empty bucket
			if turtle.getItemCount(i) > 0 then
				data = turtle.getItemDetail(i)
				if data.name == "minecraft:bucket" then
					if bucketSlot == 0 then
						bucketSlot = i
					else
						turtle.select(i)
						turtle.transferTo(bucketSlot)
					end
				elseif data.name == "minecraft:water_bucket" then
					water = water + 1
				end
			else
				emptySlots = emptySlots + 1
			end
		end
		return emptySlots, water
	end
	
	-- go(path, useTorch, torchInterval, leaveExisting, preferredBlock)
	-- create water source
	T:go("D1 R2F1 D1C2 R1C1 R1C1 R1C1 R1", false, 0, true)	-- down 1, turn round 1 block away from stairs, down 1. blocks forward, down, left and right, face backwards
	T:go("F1C2 L1C1 R2C1 L1", false, 0, true) 				-- prepare centre of water source: blocks down , left, right
	T:go("F1C2 L1C1 R1C1 R1C1 R1F1", false, 0, true)		-- prepare end of water source, move to centre, facing forward
	T:place("minecraft:water_bucket", -1, "forward")
	T:turnRight(2)											-- facing backward
	T:place("minecraft:water_bucket", -1, "forward")
	T:go("R2U1F1") -- facing forward
	T:place("minecraft:soul_sand", -1, "forward", false) 	-- placed at end of water source
	turtle.back()											-- above centre of water source
	T:place("stone", -1, "forward")
	-- fill buckets and build first 2 levels
	lib.fillBuckets(height, true)	-- fill as many buckets as required or until inventory full, sort inventory as well
	T:go("U1F2 L1C1R1C1R1C1L1", false, 0, true)		-- prepare layer 1
	lib.addSign()
	T:go("U1F1R1F1 L1C1R1C1R1C1L1", false, 0, true)	-- prepare layer 2
	lib.addSign()
	T:go("L1F1 R1F1R1", false, 0, true)	 -- above source, level 2
	lib.buildLift(height)
	lib.cleanUp(height)
end

local function createDragonAttack()
	--[[
	  X 0 0 0 X    	X = exit site				 (y = 63)
	X E O O O E X	0 = bedrock on layer above   (y = 63)
	0 O O O O O 0	O = bedrock on working layer (y = 62)
	0 O O O O O 0	E = starting position		 (y = 62)
	0 O O O O O 0
	X E O O O E X
	  X 0 0 0 X
	]]
	local lib = {}
	
	function lib.upToBedrock()
		local blockTypeU = T:getBlockType("up")
		while blockTypeU ~= "minecraft:bedrock" do
			T:up(1)
			blockTypeU = T:getBlockType("up")
		end
	end
	
	function lib.toEdgeOfBedrock()
		local distance = 0
		local blockTypeU = T:getBlockType("up")
		while blockTypeU == "minecraft:bedrock" do
			T:forward(1)
			distance = distance + 1
			blockTypeU = T:getBlockType("up")
		end
		return distance
	end
	
	function lib.findStart()
		lib.toEdgeOfBedrock()				-- go outside bedrock area
		T:go("R2F1") 						-- turn round, forward 1, under bedrock again
		local width = lib.toEdgeOfBedrock()	-- ended outside bedrock

		if width == 5 then 					-- origin in main section
			T:go("R2F3L1") 					-- now in centre of 1 axis, turned left
			width = lib.toEdgeOfBedrock()	-- ended outside bedrock, width should be 1 to 4 depending on start pos
			T:go("R2F1L1") 					-- move back under bedrock edge (3 blocks)
			lib.toEdgeOfBedrock()			-- move to corner of bedrock
		--  elseif width == 3 then 			-- on outer strip of 3
		end
	end
	
	function lib.buildWall(length)
		for i = 1, length do
			if i < length then
				T:go("C2F1", false, 0, true)
			else
				T:go("C2", false, 0, true)
			end
		end
	end
	
	function lib.addCeiling(length)
		for i = 1, length do
			if i < length then
				T:go("C0F1", false, 0, true)
			else
				T:go("C0", false, 0, true)
			end
		end
	end
	
	lib.upToBedrock()						-- go up until hit bedrock
	lib.findStart()							-- should be on position 'E'
	T:go("F1U2L1")							-- forward to any 'X' up 2, turn left
	local blockTypeF = T:getBlockType("forward")
	if blockTypeF == "minecraft:bedrock" then	-- exit not correct
		T:turnRight(2)	-- face correct direction
	else
		T:turnRight(1)	-- return to original direction
	end
	T:go("U2F1 L1F2L2") -- on corner outside bedrock, ready to build wall
	for i = 1, 4 do
		lib.buildWall(9)
		T:turnRight(1)
	end
	T:up(1)
	for i = 1, 4 do
		lib.buildWall(9)
		T:turnRight(1)
	end
	T:go("F1R1F1L1D1")
	-- add ceiling
	lib.addCeiling(7)
	T:go("R1F1R1")
	lib.addCeiling(7)
	T:go("L1F1L1")
	lib.addCeiling(7)
	T:go("R1F1R1")
	lib.addCeiling(3)
	T:go("L2F2")
	T:go("R1F1R1")
	lib.addCeiling(7)
	T:go("L1F1L1")
	lib.addCeiling(7)
	T:go("R1F1R1")
	lib.addCeiling(7)
	T:go("R1F3R1")
	lib.addCeiling(3)
	T:dig("up")
	attack(true)
end

local function createDragonTrap()
	-- build up 145 blocks with ladders
	for i = 1, 145 do
		T:go("U1C2")
		turtle.back()
		T:place("minecraft:ladder", -1, "down")
		turtle.forward()
	end
	T:go("R2F1C1 L1C1 L2C1 R1")
	for i = 1, 100 do
		T:go("F1C2")
	end
	T:forward(1)
	T:place("minecraft:obsidian", -1, "down")
	T:go("R2F1x2R2")
	T:place("minecraft:water_bucket", -1, "forward")
	T:go("R2F6R2")
	attack(false)
end

local function createPath(length, isCanal)
	if length == nil then length = 0 end
	if isCanal == nil then isCanal = false end
	local numBlocks = 0
	local continue = true
	for i = 1, 2 do
		T:fillVoid("down", {}, false)
		T:forward(1)
		numBlocks = numBlocks + 1
	end
	local place = clearVegetation("down")
	while place do -- while air, water, normal ice, bubble column or lava below
		if T:fillVoid("down", {}, false) then -- false if out of blocks
			T:forward(1)
			numBlocks = numBlocks + 1
			if numBlocks % 8 == 0 then
				if T:getItemSlot("minecraft:torch", -1) > 0 then
					T:turnRight(2)
					T:place("minecraft:torch", -1, "forward", false)
					T:turnRight(2)
				end
			end
		else
			break
		end
		if length > 0 and numBlocks >= length then -- not infinite path (length = 0)
			break
		end
		place = clearVegetation("down")
	end
	return numBlocks
end

local function createCanal(side, length, height)
	-- go(path, useTorch, torchInterval, leaveExisting)
	-- if height = 0 then already at correct height on canal floor
		-- check block below, block to left and block above, move forward tunnelling
		-- if entering water then move up, onto canal wall and continue pathway
		-- if 55 and tunnelling then flood canal
	-- else height = 1 then above water and on path across
		-- move forward, checking for water below
		-- if water finishes, move into canal, drop down and continue tunnelling
	local lib = {}
	
	function lib.side(side, length, maxLength, height)
		local turnA = "R"
		local turnB = "L"
		local isWater = false
		local isSource = false
		local onWater = false
		local numBlocks = 0
		local doContinue = true
		if side == 1 then -- right 
			turnA = "L"
			turnB = "R"
		end
		-- if starting on wall, probably already on water
		if height == 1 then -- if on wall, move to floor, turn to face opposite canal wall
			T:forward(1)
			isWater, isSource = T:isWater("down")
			if isSource then --on river/ocean so just construct walkway
				onWater = true
				numBlocks = createPath(length - 1, true)
				if numBlocks < maxLength then -- continue with canal
					T:go(turnA.."1F1D1"..turnA.."1")
				else
					doContinue = false
				end
			else
				T:go(turnA.."1F1D1")
			end
		else -- on canal floor, but could be open water
			isWater, isSource = T:isWater("forward")
			if isSource then -- water source ahead. Assume on open water	
				T:go(turnB.."1U1F1"..turnA.."1")
				onWater = true
				numBlocks = createPath(length - 1, true)
				if numBlocks < maxLength then -- continue with canal
					T:go(turnA.."1F1D1"..turnA.."1")
				else
					doContinue = false
				end
			else
				T:go(turnA.."1")
			end		
		end
		if not onWater then
			if turtle.detect() then --block to side, check is not turtle
				if T:getBlockType("forward"):find("turtle") == nil then --not a turtle
					T:dig("forward") --automatically avoids turtle
				end
			end
			T:go(turnA.."1")
			-- facing canal start wall
			if turtle.detect() then -- solid block behind: at start of canal	
				T:go(turnB.."2C2F1"..turnB.."1C1"..turnB.."1C2x0", false, 0, true) --move forward and check base/side, face start
				T:place("minecraft:water_bucket", -1, "forward")
			else -- air or water behind
				-- check if water already present behind. if not create source
				isWater, isSource = T:isWater("forward")
				if not isSource then
					T:place("minecraft:water_bucket", -1, "forward")
				end
			end
		end
		-- could be over water, just need walls
		--print("Loop starting. Enter")
		--read()
		local blockType, modifier
		local torch = 0
		local sourceCount = 0
		--loop from here. Facing backwards over existing canal/start
		while doContinue and numBlocks < maxLength do
			isWater, isSource = T:isWater("forward")
			while not isSource do
				sleep(10)
				print("waiting for water...")
				isWater, isSource = T:isWater("forward")
			end
			T:place("minecraft:bucket", -1, "forward") -- take water from source
			-- move forward check canal wall for block, below for block
			--T:go(turnA.."2F1C2"..turnB.."1C1", false, 0, true) -- face canal wall, repair if req
			T:go(turnA.."2F1C2", false, 0, true) --move forward, close floor
			isWater, isSource = T:isWater("forward")
			--print("Source in front: "..tostring(isSource).." Enter")
			--read()
			if isSource then --now on open water. move up and continue
				sourceCount = sourceCount + 1
				if sourceCount > 3 then
					sourceCount = 0
					T:go(turnB.."1U1F1"..turnA.."1")
					numBlocks = numBlocks +  createPath(length - numBlocks, true) -- eg done 20/64 blocks createPath(44)
					if numBlocks < maxLength then -- continue with canal
						T:go(turnA.."1F1D1"..turnA.."1x1"..turnA.."2") --return to canal bottom, break any block behind
					else
						doContinue = false
					end
				end
			end
			T:go(turnB.."1C1", false, 0, true) -- face canal wall, repair if req
			--print("facing wall. Enter")
			--read()
			T:go("U1x1") --go up and remove any block
			torch = torch + 1
			numBlocks = numBlocks + 1
			if turtle.detectUp() then -- block above
				T:go("U1x1")
				if torch == 8 then
					torch = 0
					T:place("minecraft:torch", -1, "forward")
				end
				T:down(1)
			end
			if torch == 8 then
				torch = 0
				T:place("minecraft:torch", -1, "forward")
			end
			T:down(1) -- on canal floor facing wall
			T:go(turnB.."1", false, 0, true)
			-- place water behind if some water is present
			isWater, isSource = T:isWater("forward")
			if not isWater then --no flowing water behind so deploy both sources
				T:forward(1)
				T:place("minecraft:water_bucket", -1, "forward")
				turtle.back()
			end
			T:place("minecraft:water_bucket", -1, "forward")
			-- check opposite canal wall
			T:go(turnB.."1")
			if turtle.detect() then --block to side, check is not turtle
				if T:getBlockType("forward"):find("turtle") == nil then --not a turtle
					T:dig("forward")
				end
			end
			T:go(turnA.."1")
		end
	end
	
	function lib.leftSideLegacy(side, length, maxLength, height)
		local doContinue = true
		local numBlocks  = 0
		local doTunnel = false
		local requestTunnel = false
		local blockType, modifier
		while doContinue and numBlocks < maxLength do
			if height == 0 then -- canal floor
				blockType, modifier = T:getBlockType("down")
				if blockType == "" or blockType == "minecraft:lava" then -- air or lava below, so place block
					T:place("minecraft:cobblestone", -1, "down", false)
				end
				-- place side block
				T:go("L1C1R1", false , 0, true)
				-- check above
				blockType, modifier = T:getBlockType("up")
				--if blockType == "minecraft:log" or blockType == "minecraft:log2" then
				if blockType ~= "" then
					if string.find(blockType, "log") ~= nil then
						T:harvestTree(false, false, "up")
					elseif blockType == "minecraft:lava" or blockType == "minecraft:water" then
						T:up(1)
						T:place("minecraft:cobblestone", -1, "up", false)
						T:down(1)
					else --solid block or air above
						if blockType ~= "" then
							T:dig("up")
						end
					end
				end
				-- check if block in front is water source
				blockType, modifier = T:getBlockType("forward")
				--if block:find("water") ~= nil then
				if blockType == "minecraft:water" and modifier == 0 then -- source block in front could be lake/ocean
					-- move up, to left and continue as height = 1
					T:go("U1L1F1R1", false, 0, true)
					height = 1
				else
					T:forward(1, true)
					numBlocks = numBlocks + 1
				end
			else -- height = 1, on canal wall
				numBlocks = numBlocks + createPath(0, true)
				-- if path finished, then move back to canal floor and continue tunnelling
				T:go("R1F1D1L1", false, 0, true)
				height = 0
			end
		end
	end
	
	function lib.rightSideLegacy(side, length, maxLength, height)
		-- assume left side already under construction
		local doContinue = true
		local numBlocks  = 0
		local doTunnel = false
		local requestTunnel = false
		local blockType, modifier
		local poolCreated = false
		while doContinue and numBlocks < maxLength do
			if height == 0 then-- canal floor
				-- create first infinity pool
				if not poolCreated then
					T:up(1)
					T:place("minecraft:water_bucket", -1, "down", false)
					T:go("L1F1R1", false, 0, true)
					T:place("minecraft:water_bucket", -1, "down", false)
					T:forward(1)
					T:place("minecraft:water_bucket", -1, "down", false)
					T:back(1)
					-- refill buckets
					for j = 1, 3 do
						T:place("minecraft:bucket", -1, "down", false)
						sleep(0,5)
					end
					T:go("R1F1L1F2", false , 0, true)
					T:down(1)
					poolCreated = true
				end
				blockType, modifier = T:getBlockType("down")
				if blockType == "" or blockType == "minecraft:lava" 
				   or blockType == "minecraft:water" or blockType == "minecraft:flowing_water" then -- air, water or lava below, so place block
					T:place("minecraft:cobblestone", -1, "down", false)
				end
				-- place side block
				T:go("R1C1L1", false , 0, true)
				T:up(1)
				blockType, modifier = T:getBlockType("up")
				if blockType == "minecraft:log" or blockType == "minecraft:log2" then
					T:harvestTree(false, false, "up")
				elseif blockType == "minecraft:lava" or blockType == "minecraft:water" then
					T:place("minecraft:cobblestone", -1, "up", false)
				end
				T:place("minecraft:water_bucket", -1, "down", false)
				for j = 1, 2 do
					T:forward(1)
					blockType, modifier = T:getBlockType("up")
					--if blockType == "minecraft:log" or blockType == "minecraft:log2" then
					if blockType ~= "" then
						if string.find(blockType, "log") ~= nil then
							T:harvestTree(false, false, "up")
						elseif blockType == "minecraft:lava" or blockType == "minecraft:water" then
							T:place("minecraft:cobblestone", -1, "up", false)
						end
					end
					-- at ceiling level
					T:go("D1R1C1L1", false, 0, true)
					blockType, modifier = T:getBlockType("down")
					if blockType == "" or blockType == "minecraft:lava" 
					   or blockType == "minecraft:water" or blockType == "minecraft:flowing_water" then -- air, water or lava below, so place block
						T:place("minecraft:cobblestone", -1, "down", false)
					end
					-- check if block in front is water source
					blockType, modifier = T:getBlockType("forward")
					T:up(1)
					T:place("minecraft:water_bucket", -1, "down", false)
					if blockType == "minecraft:water" and modifier == 0 then -- source block in front could be lake/ocean
						-- move to right and continue as height = 1
						T:go("R1F1L1", false, 0, true)
						height = 1
						break	
					end
				end
				if height == 0 then
					T:back(2)
					for j = 1, 3 do
						T:place("minecraft:bucket", -1, "down", false)
						sleep(0,5)
					end
					T:go("F3D1", false, 0, true)
				end
			else -- height = 1: on wall 
				numBlocks = numBlocks + createPath(0, true)
				-- if path finished, then move back to canal floor and continue tunnelling
				T:go("L1F1L1F1", false, 0, true) -- facing backwards, collect water
				for j = 1, 3 do
					T:place("minecraft:bucket", -1, "down", false)
					sleep(0,5)
				end
				T:go("R2F1D1", false, 0, true) --canal floor
				height = 0
			end
		end
	end
	
	-- side = 0/1: left/right side
	-- length = 0-1024 0 = continuous
	-- height = 0/1 0 = floor, 1 = wall
	-- T:place(blockType, damageNo, direction, leaveExisting)
	-- T:go(path, useTorch, torchInterval, leaveExisting)
	
	local maxLength = 1024
	if length ~= 0 then
		maxLength = length
	end
	if side == 0 then -- left side
		if deletesWater then -- legacy version
			lib.leftSideLegacy(side, length, maxLength, height)
		else -- new version
			lib.side(side, length, maxLength, height)
		end
	else -- right side (1)
		if deletesWater then -- legacy version
			lib.rightSideLegacy(side, length, maxLength, height)
		else -- new version
			lib.side(side, length, maxLength, height)
		end
	end
end

local function createCorridor(length)
	if length == nil then length = 0 end	-- continue until out of blocks
	local currentSteps = 0					-- counter for infinite length. pause every 64 blocks
	local totalSteps = 0					-- counter for all steps so far
	local torchInterval = 0					-- assume no torches
	local torchSpaces = 0					-- if torches present, counter to place every 8 blocks
	if T:getItemSlot("minecraft:torch") > 0 then
		torchInterval = 8
	end
	for steps = 1, length do
		if currentSteps >= 64 and length == 0 then
			-- request permission to continue if infinite
			T:clear()
			print("Completed "..totalSteps..". Ready for 64 more")
			print("Do you want to continue? (y/n)")
			response = read()
			if response:lower() ~= "y" then
				break
			end
			currentSteps = 0
		end
		--go(path, useTorch, torchInterval, leaveExisting)
		T:go("F1x0C2", false, 0, true) -- forward 1; excavate above, cobble below
		currentSteps = currentSteps + 1
		totalSteps = totalSteps + 1
		torchSpaces = torchSpaces + 1
		if torchInterval > 0 and torchSpaces >= 8 then
			if T:getItemSlot("minecraft:torch") > 0 then
				T:turnRight(2)
				T:place("minecraft:torch", -1, "forward")
				T:turnRight(2)
			end
			torchSpaces = 0
		end
	end
end

local function createEnderTower(stage)
	--[[ lower base = stage 1, upper base = 2, tower = 3 ]]
	local lib = {}
	--[[ go(path, useTorch, torchInterval, leaveExisting, preferredBlock) ]]
	function lib.getEmptySlots()
		local empty = 0
		for i = 1, 16 do
			if turtle.getItemCount(i) == 0 then
				empty = empty + 1
			end
		end
		return empty
	end

	function lib.getStone(direction, stacks)
		--[[ get block user wants to use ]]
		local suck = turtle.suck	
		if direction == "down" then
			suck = turtle.suckDown
		end
		if T:getBlockType(direction) == "minecraft:chest" then
			T:sortInventory()
			local slot = T:getFirstEmptySlot() --find spare slot
			if slot > 0 then --empty slot found
				turtle.select(1)
				if stacks == 0 then
					while suck() do end
				else
					for i = 1, stacks do -- get # stacks of stone from chest
						suck()
					end
				end
				if T:getSlotContains(slot) == "" then
					return T:getMostItem()				-- empty chest
				else
					return T:getSlotContains(slot) 		-- use this as default building block
				end
			else
				return T:getMostItem()				-- full inventory
			end
		else
			return T:getMostItem()				-- no chest
		end
	end
	
	function lib.stackBuckets()
		local data = {}
		local bucketSlot = 0
		local emptySlots = 0
		local water = 0
		T:sortInventory()
		for i = 1, 16 do
			-- find first empty bucket
			if turtle.getItemCount(i) > 0 then
				data = turtle.getItemDetail(i)
				if data.name == "minecraft:bucket" then
					if bucketSlot == 0 then
						bucketSlot = i
					else
						turtle.select(i)
						turtle.transferTo(bucketSlot)
					end
				elseif data.name == "minecraft:water_bucket" then
					water = water + 1
				end
			else
				emptySlots = emptySlots + 1
			end
		end
		return emptySlots, water
	end
	
	function lib.countWaterBuckets()
		local data = {}
		local buckets = 0
		for i = 1, 16 do
			data = turtle.getItemDetail(i)
			if data.name == "minecraft:water_bucket" then
				buckets = buckets + 1
			end
		end
		return buckets
	end
	
	function lib.baseRun(preferredBlock, count, turn)
		for i = 1, count do
			T:go("C2F1", false, 0, false, preferredBlock)
		end
		T:go("C2"..turn, false, 0, false, preferredBlock)
	end
	
	function lib.outsideRun(preferredBlock)
		T:place("fence", -1, "down", false)
		T:forward(1)
		T:place(preferredBlock, -1, "down", false)
		T:forward(1)
		T:place(preferredBlock, -1, "down", false)
		T:forward(2)
		T:place(preferredBlock, -1, "down", false)
	end
	
	function lib.signRun(preferredBlock ,message)
		T:place(preferredBlock, -1, "down", false)
		T:forward(4)
		T:place(preferredBlock, -1, "down", false)
		turtle.back()
		turtle.back()
		T:down(1)
		T:place("sign", -1, "forward", false, message)
		T:go("U1F2")
	end
	
	function lib.goToWater(height)
		local built = 0 -- measures completed lift height
		while turtle.down() do -- takes turtle to bottom of water source
			height = height + 1
			if turtle.detect() then
				built = built + 1
			end
		end
		T:up(1) -- above watersource assuming it is 1-1.5 blocks deep
		height = height - 1
		-- built = built - 1 not required as next block is water source: not detected
		return built, height
	end
	
	function lib.fillBuckets(toBuild)
		local emptySlots, water = lib.stackBuckets() -- gets no of empty slots + no of water buckets
		if water < toBuild then -- no of water buckets onboard less than required quantity
			for i = 1, toBuild do -- fill required no of buckets up to max space in inventory
				emptySlots = lib.getEmptySlots()
				if emptySlots == 0 then -- inventory full
					break
				else
					if T:place("minecraft:bucket", -1, "down", false) then
						water = water + 1
						sleep(0.5)
					end
				end
			end
		end
		
		return water
	end
	
	function lib.buildLift(preferredBlock)
		local built = 0 -- measures completed lift height
		local height = 0 -- measures total height from starting position
		built, height = lib.goToWater(height) -- returns lift blocks already placed, total height of drop from starting point
		local toBuild = height - built -- no of blocks to increase lift size
		while toBuild > 0 do -- at least 1 block height remaining
			local water = lib.fillBuckets(toBuild) -- no of water buckets onboard (could be more than required)
			if water > toBuild then
				water = toBuild
			end
			while turtle.detect() do -- climb to top of existing lift
				turtle.up()
				height = height - 1
			end
			T:forward(1)
			for i = 1, water do -- build lift by no of water buckets
				if T:place("minecraft:water_bucket", -1, "forward", false) then
					T:up(1)
					height = height - 1
					toBuild = toBuild - 1
					T:place(preferredBlock, -1, "down", false)
				end
			end
			turtle.back()
			-- may still be some height to complete, but needs refill
			if toBuild > 0 then
				lib.goToWater(0) --return to source
				lib.fillBuckets(toBuild)
			end
		end
		if height > 0 then -- if any remaining distance
			T:up(height)
		end
		
	end
	
	function lib.buildSection(preferredBlock, solid)
		-- builds a section without any blocks in the centre
		-- second layer of each section end walls have fence posts
		T:go("F1C2 F2C2 F1R1", false, 0, false, preferredBlock) -- first side solid row
		if solid then -- first layer of each section
			T:go("F1C2 F1R1", false, 0, false, preferredBlock) -- top side solid row
		else
			T:go("F1") -- top side solid row
			if not T:place("fence", -1, "down", false) then-- first side
				T:place(preferredBlock, -1, "down", false)
			end
			T:go("F1R1") -- top side solid row
		end
		T:go("F1C2 F2C2 F1R1", false, 0, false, preferredBlock) -- far side solid row
		T:go("F1C2 F1R1U1", false, 0, false, preferredBlock) -- bottom side solid row
	end	
	--[[
		clsTurtle methods:
		clsTurtle.place(self, blockType, damageNo, direction, leaveExisting)
		clsTurtle.go(self, path, useTorch, torchInterval, leaveExisting, preferredBlock)
	]]
	-- remove 1 stack stone from chest
	local preferredBlock = lib.getStone("down", 1) -- use this as default building block
	if stage == 1 then
		-- build base floor
		--T:go("R2F2R1F3R1", false, 0, false, preferredBlock)
		T:go("R2F1C2R1F1C2F1C2F1C2R1", false, 0, false, preferredBlock)
		for i = 1, 2 do
			lib.baseRun(preferredBlock, 8, "R1F1R1")
			lib.baseRun(preferredBlock, 8, "L1F1L1")
			lib.baseRun(preferredBlock, 8, "R1F4R1")
		end
		-- move back to centre, build water source, with soul sand at base of first source
		--T:go("R1F3L1C2F1C2F2D1", false, 0, false, preferredBlock) --just behind chest, 1 below ground level
		T:go("R1F3L1F2C2F1D1", false, 0, false, preferredBlock) --1 block behind chest, 1 below ground level
		T:place("minecraft:soul_sand", -1, "down", false) -- over block 1 of water source
		T:go("F1C2F1C2", false, 0, false, preferredBlock) -- over block 2 of water source
		T:go("F1C2U1C2", false, 0, false, preferredBlock) -- over block 4 of water source
		T:go("F1C2F1C2R2F5R2", false, 0, false, preferredBlock) -- over block 1 of water source
		T:place("minecraft:water_bucket", -1, "down", false)
		T:forward(2) -- over block 3 of water source
		T:place("minecraft:water_bucket", -1, "down", false)
		turtle.back() -- over block 2 of water source
		T:place("minecraft:bucket", -1, "down", false)
		T:go("F2D1R2C2") -- over block 4 of water source
		T:go("U1", false, 0, false, preferredBlock)
		T:place("minecraft:water_bucket", -1, "down", false)
		T:forward(4)
		lib.stackBuckets() -- put all buckets in same slot
		T:dropItem("minecraft:dirt", 0, "up") -- drop dirt up:  clsTurtle.dropItem(self, item, keepAmount, direction)
		preferredBlock = lib.getStone("down", 6)
		T:go("R1F2R1U1") -- move to start position
		for i = 1, 2 do
			-- build first level of tower: 2 x outside run, 2 x sign run
			lib.outsideRun(preferredBlock)
			if i == 1 then -- place door
				T:go("L1F1L1F1L1D1")
				T:place("door", -1, "forward", false)
				T:go("U1L1F1R1F1L1")
			end
			T:go("R1F1R1")
			lib.signRun(preferredBlock, "Pint size\nzombies\nProhibited")
			T:go("L1F1L1C2", false, 0, false, preferredBlock)
			T:forward(4) -- miss out centre block
			T:place(preferredBlock, -1, "down", false)
			T:go("R1F1R1")
			lib.signRun(preferredBlock, "Pint size\nzombies\nProhibited")
			T:go("L1F1L1")
			lib.outsideRun(preferredBlock)
			if i == 1 then -- layer 1
				T:go("R1F1R1F1R1D1") -- place door
				T:place("door", -1, "forward", false)
				T:go("U1 R1F1 L1F5 L1U1 F2D1  F2R2 U1") -- go over door
			else -- layer 2
				T:go("L1F5L1F6R2U1") -- over corner of lower platform
			end
		end
		for i = 1, 2 do -- build both sides of platform, leave centre missing
			lib.baseRun(preferredBlock, 8, "R1F1R1")
			lib.baseRun(preferredBlock, 8, "L1F1L1")
			lib.baseRun(preferredBlock, 8, "R1F4R1")
		end
		T:go("R1F3L1C2F1C2F1C2F4C2F1C2F1C2", false, 0, false, preferredBlock) --fill in centre row
		--T:go("R2F6R1F1R1U1") -- go to start of tower base
		T:go("R2F7R2D3") -- go to start on top of chest
		T:sortInventory()
	elseif stage == 2 then
		-- start on top of chest, should have sufficient stone in inventory
		T:go("U3L1F1R1F1U1") -- go to start of tower base
		for i = 1, 7 do -- build 14 block high tower
			lib.buildSection(preferredBlock, false)
			lib.buildSection(preferredBlock, true)
		end
		T:go("R2F4R1F4R1", false, 0, false, preferredBlock) -- build upper platform (154 blocks remaining)
		for i = 1, 2 do -- build both sides of upper platform, leave centre missing
			lib.baseRun(preferredBlock, 12, "R1F1R1")
			lib.baseRun(preferredBlock, 12, "L1F1L1")
			lib.baseRun(preferredBlock, 12, "R1F1R1")
			lib.baseRun(preferredBlock, 12, "L1F1L1")
			lib.baseRun(preferredBlock, 12, "R1F6R1")
		end
		T:go("R1F5 L1C2 F1C2 F1C2 F1C2 F1C2 F4C2 F1C2 F1C2 F1C2 F1C2 ", false, 0, false, preferredBlock) --fill in centre row
		T:go("R2F5") -- return to drop area
		lib.buildLift(preferredBlock) -- build bubble lift
		T:go("F3R1F1R1U1") -- go to start of tower base
		T:go("C2F4 C2R1F1R1", false, 0, false, preferredBlock) 		-- left side layer 21
		T:go("F2C2 F2C2 L1F1L1", false, 0, false, preferredBlock) 	-- centre layer 21
		T:go("C2F4 C2R2U1", false, 0, false, preferredBlock) 		-- right side layer 21
		T:go("C2F4 C2R1F1R1", false, 0, false, preferredBlock) 		-- right side layer 22
		T:place("fence", -1, "down", false)							-- fence centre of bottom side layer 22
		T:go("F2C2 F2L1F1L1", false, 0, false, preferredBlock)		-- centre layer 22
		T:go("C2F4 C2R2F2L1F1R2D2", false, 0, false, preferredBlock) --ready to place ladder
		T:place("ladder", -1, "forward", false)
		T:up(1)
		T:place("ladder", -1, "forward", false)
		--T:go("U2R1F4R1F1R1") -- ready to make upper part of tower base
		T:go("U2R1F4R1F1R1") -- ready to make upper part of tower base
		for i = 1, 2 do -- build both sides of platform, leave centre missing
			lib.baseRun(preferredBlock, 8, "R1F1R1")
			lib.baseRun(preferredBlock, 8, "L1F1L1")
			lib.baseRun(preferredBlock, 8, "R1F4R1")
		end
		T:go("R1F3 L1C2 F1C2 F1C2 F1", false, 0, false, preferredBlock) --fill in centre row
		T:place("minecraft:soul_sand", -1, "down", false) 
		T:go("F1C2 F2C2 F1C2 F1C2", false, 0, false, preferredBlock)
		T:go("R2F6R1F1R1U1") -- go to start of tower base
		-- build 2 levels, finish signs and ladders
		T:go("C2F2 R1D2 U1", false, 0, false, preferredBlock)
		T:place("ladder", -1, "down", false)
		T:turnRight(1)
		T:place("sign", -1, "forward", false, "UP\n^\n|\n|")
		T:go("U1R2F2C2 R1F2C2 R1", false, 0, false, preferredBlock) --top right corner
		T:go("F4C2B2D1", false, 0, false, preferredBlock)
		T:place("sign", -1, "forward", false, "UP\n^\n|\n|")
		T:go("U1F2R1F1C2F1R1U1", false, 0, false, preferredBlock) --ready for second level
		T:go("C2F2 R2D1", false, 0, false, preferredBlock)
		T:place("sign", -1, "forward", false, "UP\n^\n|\n|")
		T:go("U1R2F2C2R1", false, 0, false, preferredBlock) --top left corner
		T:go("F1R1C2F4C2", false, 0, false, preferredBlock) --mid bottom row
		T:go("L1F1L1C2", false, 0, false, preferredBlock) -- bottom right corner
		T:go("F2R2D1", false, 0, false, preferredBlock)
		T:place("sign", -1, "forward", false, "UP\n^\n|\n|")
		T:go("U1R2F2C2", false, 0, false, preferredBlock) -- top right corner
		-- return to chest
		T:go("L1F1L1 F5D23R2", false, 0, false, preferredBlock) -- return to chest
		T:sortInventory()
	elseif stage == 3 then
		--[[ move to top of structure
		| 4 |
		|3 5|
		| X |
		|2 6|
		| 1 |
		]]
		local towerHeight = 128 -- even no only suggest 128
		while turtle.detect() do
			turtle.up()
		end
		T:go("F1U1", false, 0, false, preferredBlock) -- return to finish tower
		for i = 1, towerHeight do -- 1
			T:go("C2U1", false, 0, false, preferredBlock)
		end
		T:go("F1L1F1R1D2")
		while turtle.down() do -- 2
			T:fillVoid("up", {preferredBlock})
		end
		T:go("F1R2C1R2F1D1", false, 0, false, preferredBlock)
		for i = 1, towerHeight / 2 do -- 3
			T:go("U2C2", false, 0, false, preferredBlock)
		end
		T:go("U1F1R1F1R1D1", false, 0, false, preferredBlock) -- back of tower facing front
		local deviate = false
		while turtle.down() do -- 4
			T:place("fence", -1, "up", false)
			if turtle.down() then
				T:fillVoid("up", {preferredBlock})
			else
				T:go("F1R2C1R1F1R1D1", false, 0, false, preferredBlock)
				deviate = true
				break
			end
		end
		if not deviate then
			T:go("F1L1F1R1D1", false, 0, false, preferredBlock)
		end
		for i = 1, towerHeight / 2 do -- 5
			T:go("U2C2", false, 0, false, preferredBlock)
		end
		T:go("F2R2", false, 0, false, preferredBlock) -- facing back of tower
		while turtle.down() do -- 6
			T:fillVoid("up", {preferredBlock}) --layer 129
		end
		T:go("F1L2C1U"..towerHeight)
		T:go("F4R1F3R1U1", false, 0, false, preferredBlock)
		-- add small platform at the top
		lib.baseRun(preferredBlock, 8, "R1F1R1")
		lib.baseRun(preferredBlock, 8, "L1F3L1")
		lib.baseRun(preferredBlock, 8, "L1F1L1")
		lib.baseRun(preferredBlock, 8, "R1F1R1")
		T:go("C2 F1C2 F1C2 F4C2 F1C2 F1C2 R2F3", false, 0, false, preferredBlock) --fill in centre row
		lib.buildLift(preferredBlock) -- build bubble lift
	end
end

function createFarm(extend)
	lib = {}
	function lib.addWaterSource(pattern)
		-- pattern = {"d","c","c","d"}
		T:go("D1x2C2")
		for i = 1, 4 do
			T:dig("forward")
			if pattern[i] == "d" then
				T:place("minecraft:dirt", -1, "forward", false)
			elseif pattern[i] == "c" then
				T:place("minecraft:cobblestone", -1, "forward", false)
			end
			T:turnRight(1)
		end
		T:up(1)
		T:place("minecraft:water_bucket", -1, "down")
	end
	
	function lib.placeDirt(count, atCurrent)
		if atCurrent then
			local blockType = T:getBlockType("down")
			if blockType ~= "minecraft:dirt" and blockType ~= "minecraft:grass_block" then
				T:place("minecraft:dirt", -1, "down", false)
			end
		end
		for  i = 1, count do
			T:forward(1)
			T:dig("up")
			local blockType = T:getBlockType("down")
			if blockType ~= "minecraft:dirt" and blockType ~= "minecraft:grass_block" then
				T:place("minecraft:dirt", -1, "down", false)
			end
		end
	end
	
	-- extend "", "right" or "forward". only adds a single new farm.
	-- right adds farm and checks for existing front extensions, dealt with separately
	-- clsTurtle.place(blockType, damageNo, direction, leaveExisting)
	if extend == nil then
		extend = ""
	end
	local blockType = ""
	-- extend = "right": placed on cobble corner of existing farm facing right side
	-- extend = "front": placed on cobble corner of existing farm facing front
	-- else placed on ground at corner of potential new farm facing front
	
	-- step 1 dig ditch round perimeter wall
	if extend == "right" then
		-- move to front corner
		T:forward(12) -- over wall of existing farm, will be empty below if not extended
		blockType = T:getBlockType("down")
		if blockType == "minecraft:cobblestone" then -- already a farm extension on left side
			T:go("R1F1D1")
		else
			T:go("D1R1F1x0")
		end
		-- cut ditch round new farm extension
		for i = 1, 11 do
			T:go("x0F1")
		end
		T:go("R1x0")
		for i = 1, 13 do
			T:go("x0F1")
		end
		T:go("R1x0")
		-- now at lower right corner. if extension below, do not cut ditch
		blockType = T:getBlockType("forward")
		if blockType == "minecraft:cobblestone" then -- already a farm extension below
			-- return to start for adding chests and walls
			T:go("U1R1F1L1F12R1")
		else -- finish ditch
			for i = 1, 12 do
				T:go("x0F1")
			end
			T:go("R1U1F1") -- on corner of new extension
		end
	elseif extend == "forward" then
		-- cut ditch round new farm extension
		T:go("L1F1x0R1D1")
		for i = 1, 12 do
			T:go("x0F1")
		end
		T:go("R1x0")
		for i = 1, 13 do
			T:go("x0F1")
		end
		T:go("R1x0")
		for i = 1, 11 do
			T:go("x0F1")
		end
		T:go("U1x0F1R1F12R1") -- on corner of new extension
	else -- new farm. cut a groove round the entire farm base
		-- move to left side of intended wall
		T:go("L1F1x0R1")
		for j = 1, 4 do
			for i = 1, 12 do
				T:go("x0F1")
			end
			T:go("R1x0F1")
		end
		T:go("R1F1L1U1")
	end
	-- stage 2 place sapling and double chest
	T:dig("down") --remove cobble if present
	T:place("minecraft:dirt", -1, "down", false)
	T:go("F1R2")
	T:place("sapling", -1, "forward", false) -- plant sapling
	T:go("L1")
	T:dig("down")
	T:place("minecraft:chest", -1, "down", false)-- place chest below
	T:go("L1F1R1")
	T:dig("down")
	T:place("minecraft:chest", -1, "down", false) -- place chest 2 below
	T:turnLeft(1)
	if extend == "right" then -- cobble wall exists so go forward to its end
		T:forward(9)
	else -- new farm or extend forward
		for i = 1, 9 do -- complete left wall to end of farm
			T:go("F1x0x2C2")
		end
	end
	T:go("R1F1R1x0x2C2F1D1")-- turn round ready for first dirt col
	lib.addWaterSource({"d","c","c","d"}) -- water at top of farm
	lib.placeDirt(9, false) -- place dirt back to start
	lib.addWaterSource({"c","c","d","d"}) -- water source next to chests
	T:go("U1F1R2")
	if not T:getBlockType("down") ~= "minecraft:chest" then
		T:dig("down")
		T:place("minecraft:chest", -1, "down", false)
	end
	T:go("R1F1L1")
	if T:getBlockType("down") ~= "minecraft:chest" then
		T:dig("down")
		T:place("minecraft:chest", -1, "down", false)
	end
	T:go("F1D1")
	lib.placeDirt(9, true)
	local turn = "R"
	for i = 1, 7 do
		T:go("F1U1x0C2"..turn.."1F1"..turn.."1x0x2C2F1D1")
		lib.placeDirt(9, true)
		if turn == "R" then
			turn = "L"
		else
			turn = "R"
		end
	end
	T:go("F1U1x0C2"..turn.."1F1"..turn.."1x0x2C2F1D1")
	lib.addWaterSource({"d","c","c","d"})
	lib.placeDirt(9, false)
	lib.addWaterSource({"c","c","d","d"})
	T:go("F1U1R1C2x0F1x0x2C2R1")
	for i = 1, 11 do
		T:go("F1x0x2C2")
	end
	-- add chest to any existing farm extension to the right
	T:go("L1F1L1")
	if T:getBlockType("down") ~= "minecraft:cobblestone" then -- farm extension already exists to right
		T:place("minecraft:chest", -1, "down", false) --single chest marks this as an extension
	end
	T:go("L1F11")
end

function createFarmExtension()
	-- assume inventory contains 4 chests, 64 cobble, 128 dirt, 4 water, 1 sapling
	-- check position by rotating to face tree/sapling
	local doContinue = true
	local treePresent = false
	local blockType = T:getBlockType("down")
	local extend = "right" -- default
	if blockType ~= "minecraft:chest" then
		T:clear()
		print("Chest not present below\n")
		print("Unable to calculate position")
		print("Move me next to/front of the tree / sapling")
		print("lower left corner of the existing farm.")
		doContinue = false
	else
		for i = 1, 4 do
			blockType = T:getBlockType("forward")
			if blockType:find("log") ~= nil or blockType:find("sapling") ~= nil then
				treePresent = true
				break
			end
			T:turnRight()
		end
		if not treePresent then
			T:clear()
			print("Unable to locate tree or sapling")
			print("Plant a sapling on the lower left")
			print("corner of the farm, or move me there")
			doContinue = false
		end
	end
	if doContinue then -- facing tree. check if on front or l side of farm
		T:go("R1F1D1") -- will now be over water if at front
		if T:getBlockType("down"):find("water") == nil then
			extend = "forward"
		end
		T:go("R2U1F1L1") -- facing away from tree, either to front or right
		T:forward(9)
		-- if tree or sapling in front warn and stop
		blockType = T:getBlockType("forward")
		if blockType:find("log") ~= nil or blockType:find("sapling") ~= nil then
			doContinue = false
		else
			T:forward(1) -- on corner ? cobble
			blockType = T:getBlockType("down")
			if blockType ~= "minecraft:cobblestone" then
				doContinue = false
			end
		end
		if doContinue then -- extend farm.
			if extend == "right" then
				T:turnLeft(1)
			end
			createFarm(extend)
		else
			T:clear()
			print("This farm has already been extended")
			print("Move me next to/front of the tree / sapling")
			print("of the last extension in this direction.")
		end
	end
end

function createFloorCeiling(width, length, size ) -- size integer 1 to 4
	local useBlock = T:getSlotContains(1)
	print("Using ".. useBlock)
	-- check if block above/below
	local blockBelow = turtle.detectDown()
	local blockAbove = turtle.detectUp()
	local direction = "down"
	if size == 1 then -- Replacing current floor
		-- if no block below, assume is missing and continue
	elseif size == 2 then -- New floor over existing
		-- if no block below, assume in correct position and continue
		-- else move up 1 and continue
		if blockBelow then T:up(1) end
	elseif size == 3 then -- Replacing current ceiling
		-- if no block above, assume is missing and continue
		direction = "up"
	elseif size == 4 then -- New ceiling under existing
		-- if no block above, assume in correct position and continue
		-- else move down 1 and continue
		if blockAbove then T:down(1) end
		direction = "up"
	end
	
	local evenWidth = false
	local evenHeight = false
	local loopWidth
	-- go(path, useTorch, torchInterval, leaveExisting)
	if width % 2 == 0 then
		evenWidth = true
		loopWidth = width / 2
	else
		loopWidth = math.ceil(width / 2)
	end
	if length % 2 == 0 then
		evenHeight = true
	end
	turtle.select(1)
	-- if width is even no, then complete the up/down run
	-- if width odd no then finish at top of up run and reverse
	for x = 1, loopWidth do
		-- Clear first column (up)
		for y = 1, length do
			T:place(useBlock, -1, direction, false) -- leaveExisting = false
			if y < length then
				T:go("F1", false, 0, false)
			end
		end
		-- clear second column (down)
		if x < loopWidth or (x == loopWidth and evenWidth) then -- go down if on width 2,4,6,8 etc
			T:go("R1F1R1", false,0,false)
			for y = 1, length do
				T:place(useBlock, -1, direction, false) -- leaveExisting = false
				if y < length then
					T:go("F1", false, 0, false)
				end
			end
			if x < loopWidth then 
				T:go("L1F1L1", false,0,false)
			else
				T:turnRight(1)
				T:forward(width - 1)
				T:turnRight(1)
			end
		else -- equals width but is 1,3,5,7 etc
			T:turnLeft(2) --turn round 180
			T:forward(length - 1)
			T:turnRight(1)
			T:forward(width - 1)
			T:turnRight(1)
		end
	end
end

function createHollow(width, length, height)
	-- this function currently not used
	--should be in bottom left corner at top of structure
	-- dig out blocks in front and place to the left side
	--go(path, useTorch, torchInterval, leaveExisting)
	-- go @# = place any block up/forward/down # = 0/1/2
	for h = 1, height do
		for i = 1, length - 1 do
			T:go("L1@1R1F1", false, 0, true)
		end
		T:go("L1@1R2", false, 0, true, false)
		for i = 1, width - 1 do
			T:go("L1@1R1F1", false, 0, true)
		end
		T:go("L1@1R2", false, 0, true, false)
		for i = 1, length - 1 do
			T:go("L1@1R1F1", false, 0, true)
		end
		T:go("L1@1R2", false, 0, true, false)
		for i = 1, width - 1 do
			T:go("L1@1R1F1", false, 0, true)
		end
		T:go("L1@1R2", false, 0, true, false)
		-- hollowed out, now clear water/ blocks still present
		clearRectangle(width, length)
		if h < height then
			T:down(1)
		end
	end
end

local function createIceCanal(length)
	local placeIce = true
	local iceOnBoard = true
	local isWater, isSource, isIce = T:isWater("forward")
	if isWater then -- user put turtle inside canal water
		T:up(1)
	end
	-- place ice on alternate blocks until length reached, run out of ice or hit a solid block.
	if length == 0 then length = 1024 end
	for i = 1, length do
		if T:getBlockType("down"):find("ice") == nil then -- no ice below
			if placeIce then
				if not T:place("ice", -1, "down", false) then -- out of ice
					break
				end
				if i == length - 1 then
					break
				end
			else
				T:dig("down") -- remove any other block
			end
		else -- ice already below
			placeIce = true
		end
		if not turtle.forward() then -- movement blocked or out of fuel
			break
		end
		placeIce = not placeIce -- reverse action
	end
end

local function createIceCanalBorder(side, length)
	--[[ Used to convert water canal to ice with trapdoor margin on one side ]]
	-- position gained from setup left = 0, right = 1
	local A = "R"
	local B = "L"
	if side == 1 then
		A = "L"
		B = "R"
	end
	-- check position. Should be facing down canal with wall on same side
	-- so wall will be detected on i = 4 (if present)
	local turns = 0
	local wallFound = false
	for i = 1, 4 do
		if turtle.detect() then
			turns = i
			wallFound = true
		end
		T:go(A.."1")
	end
	if wallFound and turns ~= 4 then -- facing towards wall or canal
		T:go(A..turns)
	end
	redstone.setOutput("bottom", true)
	-- add trapdoors to canal towpath and activate them
	for i = 1, length do
		T:go(A.."1")
		if turtle.detectDown() then
			if T:getBlockType("down") == "minecraft:torch" then
				turtle.digDown()
			end
		end
		if not turtle.detectDown() then -- ignore previous trapdoors of slabs
			T:place("trapdoor", -1, "down", false)
		end
		T:go(B.."1F1")
	end
end

local function createLadder(destination, level, destLevel)
	-- createLadder("bedrock", 70, -48)
	-- go(path, useTorch, torchInterval, leaveExisting)
	-- place(blockType, damageNo, direction, leaveExisting)
	local function placeLadder(direction, ledge)
		-- 1 check both sides and behind
		local fluid = false
		local block = T:isWaterOrLava("forward", ledge)
		if block:find("water") ~= nil or block:find("lava") ~= nil then
			--[[ surround 2 block shaft with blocks ]]
			T:go("R1C1R1C1R1C1R1F1L1C1R1C1R1C1R1C1F1R2C1x1")
		else
			--[[ no water/lava so prepare ladder site]]
			T:go("F1L1C1R1C1R1C1L1B1", false, 0, true)
		end
		if not T:place("minecraft:ladder", -1, "forward", false) then
			print("Out of ladders")
			turtle.forward()
			error()
		end
		-- 3 check if ledge, torch
		if ledge == 0 then
			T:place("common", -1, direction, false) -- any common block
		elseif ledge == 1 then
			T:place("minecraft:torch", -1, direction, false)
		elseif ledge == 2 then
			ledge = -1
		end
		return ledge
	end
	
	local ledge = 0
	local height = math.abs(destLevel - level) --height of ladder
	if destination == "surface" then -- create ladder from current level to height specified
		-- check if extending an existing ladder
		for i = 1, height do -- go up, place ladder as you go
			ledge = placeLadder("down", ledge)
			T:up(1)
			ledge = ledge + 1
		end		
	else -- ladder towards bedrock		
		local success = true
		local numBlocks, errorMsg
		for i = 1, height do -- go down, place ladder as you go
			ledge = placeLadder("up", ledge)
			--success, blocksMoved, errorMsg, blockType = clsTurtle.down(self, steps, getBlockType)
			success, numBlocks, errorMsg, blockType = T:down(1, true)
			ledge = ledge + 1
			-- if looking for stronghold then check for stone_bricks 
			if blockType:find("stone_bricks") ~= nil then
				print("Stronghold discovered")
				break
			end
		end
		if not success then --success = false when hits bedrock
			-- test to check if on safe level immediately above tallest bedrock
			print("Bedrock reached")
			T:findBedrockTop(0)
			-- In shaft, facing start direction, on lowest safe level
			-- create a square space round shaft base, end facing original shaft, 1 space back
			T:go("L1n1R1n3R1n2R1n3R1n1", false, 0, true)
			T:go("U1Q1R1Q3R1Q2R1Q3R1Q1", false, 0, true)
		end
	end
end

function createLadderToWater()
	-- go down to water/lava with alternaate solid/open layers
	-- create a working area at the base
	-- Return to surface facing towards player placing ladders
	local inAir = true
	local numBlocks, errorMsg
	local block, blockType
	local height = 2
	T:go("R2D2", false, 0, true) -- face player, go down 2
	while inAir do --success = false when hits water/lava
		T:go("C1R1C1R2C1R1", false, 0, true)
		T:go("D1C1", false, 0, true)
		height = height + 1
		block, blockType = T:isWaterOrLava("down")
		if string.find(block, "water") ~= nil or string.find(block, "lava") ~= nil then
			inAir = false
		else
			T:down(1)
			height = height + 1
		end
	end
	-- In shaft, facing opposite start direction, on water/lava
	-- create a square space round shaft base, end facing original shaft, 1 space back
	T:go("R2C2F1C2F1C2R1", false, 0, true)
	T:go("F1C2F1C2R1", false, 0, true)
	T:go("F1C2F1C2F1C2F1C2R1", false, 0, true)
	T:go("F1C2F1C2F1C2F1C2R1", false, 0, true)
	T:go("F1C2F1C2F1C2F1C2R1", false, 0, true)
	T:go("F2R1F1", false, 0, true) -- under the upward pillar

	for i = height, 0, -1 do
		T:go("C2e1U1")
	end
	T:down(1)
end

function createMine()
	-- go(path, useTorch, torchInterval, leaveExisting, preferredBlock)
	T:clear()	
	T:go("m32U1R2M16", true, 8, true) -- mine ground level, go up, reverse and mine ceiling to mid-point
	T:go("U2D2") -- create space for chest
	T:place("minecraft:chest", -1, "up", false)
	T:emptyTrash("up")
	T:go("D1R1m16U1R2M16", true, 8, true) -- mine floor/ceiling of right side branch
	T:emptyTrash("up")
	T:go("D1m16U1R2M16", true, 8, true) -- mine floor/ceiling of left side branch
	T:emptyTrash("up")
	T:go("L1M15F1R1D1", true, 8, true) -- mine ceiling of entry coridoor, turn right
	T:go("F1x0F1x0n14R1n32R1n32R1n32R1n14F1x0F1U1", true, 8, true)-- mine floor of 36 x 36 square coridoor
	T:go("R1F16R2") --return to centre
	T:emptyTrash("up")
	T:go("F16R1") --return to entry shaft
	T:go("F2Q14R1Q32R1Q32R1Q32R1Q14F2R1", true, 8, true) --mine ceiling of 36x36 square coridoor. return to entry shaft + 1
	T:go("F16R2") --return to centre
	T:emptyTrash("up")
	-- get rid of any remaining torches
	while T:getItemSlot("minecraft:torch", -1) > 0 do
		turtle.select(T:getItemSlot("minecraft:torch", -1))
		turtle.dropUp()
	end
	T:go("F16R1F1R1") --return to shaft + 1
	for i = 1, 8 do
		T:go("N32L1F1L1", true, 8, true)
		T:go("N16L1F"..(i * 2).."R2", true, 8, true)
		T:emptyTrash("up")
		if i < 8 then
			T:go("F"..(i * 2).."L1N16R1F1R1", true, 8, true)
		else
			T:go("F"..(i * 2).."L1N16L1", true, 8, true)
		end
	end
	T:go("F17L1") -- close gap in wall, return to ladder + 1
	for i = 1, 8 do
		T:go("N32R1F1R1", true, 8, true)
		T:go("N16R1F"..(i * 2).."R2", true, 8, true)
		T:emptyTrash("up")
		if i < 8 then
			T:go("F"..(i * 2).."R1N16L1F1L1", true, 8, true)
		else
			T:go("F"..(i * 2).."R1N16R1", true, 8, true)
		end
	end
	T:go("F16R1")
	T:clear()
	print("Mining operation complete")
end

function createMineBase()
	T:clear()
	-- check ladder:
	T:turnRight(2)
	local blockType, modifier = T:getBlockType("forward")
	while blockType == "" do
		T:forward(1)
		blockType, modifier = T:getBlockType("forward")
	end
	if blockType ~= "minecraft:ladder" then -- in correct position
		-- no ladder, move back 1
		T:back(1)
	end
	-- build pond:
	T:go("R1F1x0L1F1x0F1x0R1") -- at side wall
	T:go("F1n4R2n4U1R2Q4R2Q4R2") -- cut pond 3x1
	T:go("C2F4C2R2F1")
	T:place("minecraft:water_bucket", 0, "down", false)
	T:forward(2)
	T:place("minecraft:water_bucket", 0, "down", false)
	T:go("F2L1F2R1F1L1") -- at start position
	--[[
	T:go("m32U1R2M16D1", true, 8) -- mine ground level, go up, reverse and mine ceiling to mid-point, drop to ground
	T:go("U1R1A15D1R2m15", false, 0) -- Create roof of coridoor, turn and create lower wall + floor
	T:go("U1A15D1R2m15U1x0", false, 0) -- Create roof of coridoor, turn and create lower wall + floor
	T:place("minecraft:chest", -1, "up", false) --place chest in ceiling
	T:emptyTrash("up")
	T:go("L1M15F1R1D1", true, 8) -- mine ceiling of entry coridoor, turn right, drop down
	T:go("F2n14R1n15", true, 8)-- mine floor of first quarter of square
	T:go("L1F1x0C1R1 F1x0L1C1R1 F1x0L1C1R1C1R1 F1x0C1L1") -- make alcove
	T:go("F1x0n14R1n32R1n15", true, 8)
	T:go("L1F1x0C1R1 F1x0L1C1R1 F1x0L1C1R1C1R1 F1x0C1L1") -- make alcove
	T:go("F1x0n14R1n14F2", true, 8)-- mine floor of last quarter of square
	
	T:go("U1R1F16R2D1") --return to centre
	T:emptyTrash("up")
	T:go("U1F16R1") --return to entry shaft
	T:go("F2Q14R1Q15", true, 8) -- mine ceiling of first quarter
	T:go("L1F1C1R1 F1L1C1R1 F1L1C1R1C1R1 F1C1L1") -- make alcove
	T:go("C0F1Q14R1Q32R1Q15", true, 8) --mine ceiling of second half
	T:go("L1F1C1R1 F1L1C1R1 F1L1C1R1C1R1 F1C1L1") -- make alcove
	T:go("C0F1Q14R1Q14F2R1", true, 8) -- mine ceiling of last quarter
	T:go("F16D1") --return to centre
	T:emptyTrash("up")
	-- get rid of any remaining torches
	while T:getItemSlot("minecraft:torch", -1) > 0 do
		turtle.select(T:getItemSlot("minecraft:torch", -1))
		turtle.dropUp()
	end
	
	for i = 1, 8 do
		T:go("N32L1F1L1", true)
		T:go("N16L1F"..(i * 2).."R2", true)
		T:emptyTrash("up")
		T:go("F"..(i * 2).."L1N16R1F1R1", true)
	end
	T:go("L1F17L1") -- close gap in wall, return to ladder + 1
	for i = 1, 8 do
		T:go("N32R1F1R1", true)
		T:go("N16R1F"..(i * 2).."R2", true)
		T:emptyTrash("up")
		T:go("F"..(i * 2).."R1N16L1F1L1", true)
	end
	-- fill water buckets
	-- return to centre
	T:go("R1F16R1")]] 
	
	T:clear()
	print("Mining operation complete")
end

function createMineEnhanced()
	T:clear()	
	T:go("m32U1R2M16D1x2", true, 8) -- mine ground level, go up, reverse and mine ceiling to mid-point, drop to ground, excavate
	T:emptyTrash("down")
	T:go("U1R1A15D1R2E13m2x2", false, 0) -- Create roof of coridoor, turn and create lower wall + remove floor
	T:emptyTrash("down")
	T:go("U1A15D1R2E13m2x2", false, 0) -- Create roof of coridoor, turn and create lower wall + remove floor
	T:emptyTrash("down")
	T:go("U1L1M15F1R1D1", true, 8) -- mine ceiling of entry coridoor, turn right, drop down
	T:go("F2n14R1n15", true, 8)-- mine floor of first quarter of square
	
	T:go("L1F1x0C1R1 F1x0L1C1R1 F1x0L1C1R1C1R1 F1x0C1L1") -- make alcove
	T:go("F1x0n14R1n32R1n15", true, 8)
	T:go("L1F1x0C1R1 F1x0L1C1R1 F1x0L1C1R1C1R1 F1x0C1L1") -- make alcove
	T:go("F1x0n14R1n14F2", true, 8)-- mine floor of last quarter of square
	T:go("U1R1F16R2D1") --return to centre
	T:emptyTrash("down")
	T:go("U1F16R1") --return to entry shaft
	T:go("F2Q14R1Q15", true, 8) -- mine ceiling of first quarter
	T:go("L1F1C1R1 F1L1C1R1 F1L1C1R1C1R1 F1C1L1") -- make alcove
	T:go("C0F1Q14R1Q32R1Q15", true, 8) --mine ceiling of second half
	T:go("L1F1C1R1 F1L1C1R1 F1L1C1R1C1R1 F1C1L1") -- make alcove
	T:go("C0F1Q14R1Q14F2R1", true, 8) -- mine ceiling of last quarter
	T:go("F16D1") --return to centre
	T:emptyTrash("down")
	-- get rid of any remaining torches
	while T:getItemSlot("minecraft:torch", -1) > 0 do
		turtle.select(T:getItemSlot("minecraft:torch", -1))
		turtle.dropDown()
	end
	--cut access coridoors
	T:go("U1F2R1F1Q14F1 R1F1L1F1R1F2R1F1L1F1R1 F1Q14F2Q14F1 R1F1L1F1R1F2R1F1L1F1R1F1 Q14F1D1") --ceiling
	T:go("F1n14F1 R1F1L1F1R1F2R1F1L1F1R1 F1n14F2n14F1 R1F1L1F1R1F2R1F1L1F1R1F1 n14F1U1") --floor, then up
	T:go("R1F2D1")
	T:go("R1F1C1B1C1L1C1L1F1C1B1C1L1C1L2")
	T:emptyTrash("down")
	T:go("U1F16R1F1R1") --return to entry shaft + 1

	for i = 1, 8 do
		T:go("N32L1F1L1", true)
		if i == 1 then
			T:go("N16L1F2R2", true)
			T:emptyTrash("down")
			T:go("F2L1N16R1F1R1", true)
		elseif i == 8 then
			T:go("L1F1R1N16", true)
			T:emptyTrash("down")
			T:go("N16R1F1R1", true)
		else
			T:go("N16", true)
			T:emptyTrash("down")
			T:go("N16R1F1R1", true)
		end
	end
	T:go("L1F16L1") -- return to shaft + 1
	for i = 1, 8 do
		T:go("N32R1F1R1", true)
		if i == 1 then
			T:go("N16R1F2R2", true)
			T:emptyTrash("down")
			T:go("F2R1N16L1F1L1", true)
		elseif i == 8 then
			T:go("R1F1L1N16", true)
			T:emptyTrash("down")
			T:go("N16R1F1R1", true)
		else
			T:go("N16", true)
			T:emptyTrash("down")
			T:go("N16L1F1L1", true)
		end	
	end
	T:go("L1F15R1") -- return
	T:clear()
	print("Mining operation complete")
end

function createMobBubbleLift(size)
	-- size = 0 or 1 (left/right)
	local lib = {}
		
	function lib.down()
		local moves = 0
		while turtle.down() do
			moves = moves + 1
		end
		return moves
	end
	
	function lib.up()
		local moves = 0
		while turtle.up() do
			moves = moves + 1
		end
		return moves
	end
	-- check if dirt or soulsand below
	local turn = "R"
	if size == 1 then
		turn = "L"
	end
	local onSand = false
	local blockType = T:getBlockType("down")
	if blockType == "minecraft:soul_sand" then
		onSand = true
	elseif blockType == "minecraft:dirt" then
		T:dig("down")
		if T:place("minecraft:soul_sand", -1, "down", false) then
			onSand = true
		end
	end
	if onSand then
		-- check facing sign, rotate if not
		blockType = T:getBlockType("forward")
		while blockType:find("sign") == nil do
			T:turnRight(1)
			blockType = T:getBlockType("forward")
		end
		for i = 1, 3 do
			-- fill in back and one side, go up
			if turn == "R" then
				T:go("R1C1R1C1R1x1R1U1", false, 0, true)
			else
				T:go("L1C1L1C1L1x1L1U1", false, 0, true)
			end
		end
		for i = 1, 17 do
			-- tunnel up, filling 3 sides
			if turn == "R" then
				T:go("R1C1R1C1R1x1R1C1U1", false, 0, true)
			else
				T:go("L1C1L1C1L1x1L1C1U1", false, 0, true)
			end
		end
		-- move either left/right 8 blocks, repairing ceiling and sides
		if turn == "R" then
			T:go("C0R2C1R1F1C0C1R1C1R2C1L1F1A8", false, 0, true) -- fill top of column
		else
			T:go("C0L2C1L1F1C0C1L1C1L2C1R1F1A8", false, 0, true) -- fill top of column
		end
		-- turn round, go down 1, forward 7 blocks repairing bottom and sides
		T:go("D1C1R2X7", false, 0, true)
		-- turn round, go up, place cobble, forward 4, place cobble
		T:go("R2U1C2F4C2", false, 0, true)
		-- turn round forward 1 place water, forward 2, place water
		T:go("R2F1", false, 0, true)
		T:place("minecraft:water_bucket", -1, "down", false)
		T:forward(2)
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("R2F1")
		repeat
			-- refill both buckets
			T:place("minecraft:bucket", -1, "down", false)
			sleep(0.5)
			T:place("minecraft:bucket", -1, "down", false)
			-- back 4, down to solid, place water,
			for i = 1, 4 do
				turtle.back()
			end
			local moves = lib.down() -- returns no of blocks descent 0 to 19
			if moves > 0 then
				T:place("minecraft:water_bucket", -1, "forward", false)
				T:go("U1C2")
				if moves > 1 then
					T:place("minecraft:water_bucket", -1, "forward", false)
					T:go("U1C2")
				end
			end
			lib.up() -- 0 - 19
			T:forward(4)
		until moves <= 1
		-- delete water sources and remove cobble
		T:go("R2F3C1R2F1")
		for i = 1, 7 do -- go to end of run placing cobble
			T:go("C2F1")
		end
		T:turnRight(2)
		for i = 1, 7 do -- go to end of run, down 2
			T:go("x2F1x2")
		end
		T:go("R2F7D2")
		for i = 1, 18 do
			-- tunnel down, filling all 4 sides
			T:go("R1C1R1C1R1C1R1C1D1", false, 0, true)
		end
		-- turn round, tunnel forward 6 blocks
		T:go("R2U1F1M5D1R2F1X5")-- end up under drop column
	else
		print("Unable to find or place soulsand.\nEnter to continue")
		read()
	end
end

local function createMobFarmCube(blaze, continue)
	if blaze == nil then blaze = false end
	if continue == nil then continue = false end
	-- continue allows for 2-part operation 1 = main cube, 2 = rails etc
	local lib = {}
	function lib.rail(move, isPowered, count)
		if move ~= "" then
			T:go(move)
		end
		for i = 1, count do
			if isPowered then
				if not T:place("minecraft:powered_rail", -1, "down", false) then
					T:place("minecraft:golden_rail", -1, "down", false)
				end
			else
				T:place("minecraft:rail", -1, "down", false)
			end
			if i < count then
				T:forward(1)
			end
		end
	end
	
	local brick = "minecraft:nether_brick" -- pre 1.16+ name
	if mcMajorVersion < 1.7  and mcMajorVersion >= 1.16 then -- 1.12 to 1.??
		brick = "minecraft:nether_bricks"
	end
	if not continue then -- new mob cube
		-- clsTurtle.go(self, path, useTorch, torchInterval, leaveExisting, preferredBlock)
		-- determine spawner position level 4, move to top of spawner (level 6)
		local onTop = false
		local blockType = T:getBlockType("down")
		if blockType:find("spawner") ~= nil then
			onTop = true
		end
		if not onTop then
			blockType = T:getBlockType("up")
			if blockType:find("spawner") ~= nil then
				T:go("B1U2F1")
				onTop = true
			end
		end
		if not onTop then
			blockType = T:getBlockType("forward")
			if blockType:find("spawner") ~= nil then
				T:go("U1F1")
				onTop = true
			end
		end
		if onTop then
			-- place slab on top T:place(blockType, damageNo, direction, leaveExisting)
			T:up(1)
			T:place("slab", -1, "down", true)
			-- go up 2 blocks, forward 4, right, forward 4, right
			T:go("U2F4R1F4R1")
			-- clear 9x9 and plug ceiling (level 9)
			--"Q": mine block above and/or fill void + mine block below if valuable + left side
			T:go("R2C1R2Q8R1Q8R1Q8R1Q7R1F1", false, 0, false)
			-- 7x7 rectangle filling above
			for i = 1, 3 do
				T:go("M7R1F1R1M7L1F1L1", false, 0, false)
			end
			T:go("M7R2F8R1F7R1", false, 0, false) --back to corner
			-- mine wall:     q# mine # blocks forward, check left side and patch
			for i = 1, 2 do
				-- down 1, clear 9x9 border, checking walls (level 8, 7)
				T:go("D1R2C1R2q8R1q8R1q8R1q7R1F1",  false, 0, false)
				-- clear remaining 7x7 area
				clearRectangle(7, 7, false, false)
				T:go("R2F1R1F1R1",  false, 0, false) --back to corner
			end
			for i = 1, 2 do
				-- down 1, clear 9x9 border (level 6, 5)
				T:go("D1R2C1R2q8R1q8R1q8R1q7R1",  false, 0, false)
				T:go("F7R1F6R1F6R1F5R1",  false, 0, false)
				T:go("F5R1F4R1F4R1F6L1F2R2",  false, 0, false)
			end
			local count = 3
			if blaze then
				count = 2
			end
			for i = 1, count do
				-- down 1, clear 9x9 border, checking walls (level 4, 3, (2))
				T:go("D1R2C1R2q8R1q8R1q8R1q7R1F1")
				-- clear remaining 7x7 area
				clearRectangle(7, 7, false, false)
				T:go("R2F1R1F1R1", false, 0, false) --back to corner
			end
			if blaze then
				-- strart in top right corner. border is made of slabs placed up
				T:go("D1R2F1R1F1R1")
				for i = 1, 3 do
					clearRectangle(11, 11, false, false) --layer 2, 1, 0
					T:down(1)
				end
				
				T:go("R2F1R1F1R1")
				-- ready to lay floor and border
				-- q# mine # blocks forward, check left side and patch
				T:go("R2C1R2 n12R1 n12R1 n12R1 n11R1F1", false, 0, false)
				--  fill in floor 11x11 rectangle below			
				
				local a, b, numBricks = T:getItemSlot(brick)
				for i = 2, 12 do -- 11 iterations (rows)
					T:go("m10", false, 0, false, brick)
					if i % 2 == 0 then -- 2, 4, 6, 8, 10
						if i < 12 then
							T:go("R1F1R1", false, 0, false, brick)
						end
					else -- 3,5,7,9,11
						T:go("L1F1L1", false, 0, false, brick)
					end
				end			
				-- move to starting point in front of spawner,
				-- outside retaining wall' facing in, and ask for supplies
				T:go("L1F5U6R1F2R2")
				continue = true -- script continues below for blaze farm
			else -- not blaze
				-- clear floor and plug holes
				-- n# mine block below and/or fill void + check left side
				T:down(2)
				for j = 1, 2 do
					T:go("R2C1R2n8R1n8R1n8R1n7R1F1",  false, 0, true)
					-- 7x7 rectangle filling below
					for i = 1, 4 do
						if i < 4 then
							T:go("m6R1F1R1m6L1F1L1", false, 0, true)
						else
							T:go("m6R1F1R1m6", false, 0, true)
						end
					end
					if j == 1 then
						T:go("U1F1R1F8R1")
					end
				end
			end
		else -- not on top
			T:clear()
			print("Spawner not found. Place me on top,")
			print("immediately below, or facing it.")
			print("\nEnter to quit")
			read()
		end
	end
	if continue then
		T:sortInventory()
		T:turnRight(2)
		T:emptyTrashItem("forward", "minecraft:netherrack", 0)
		T:emptyTrashItem("forward", brick, 128)
		T:emptyTrashItem("forward", "fence", 0)
		T:turnRight(2)
		--clsTurtle.getItemSlot(self, item, useDamage): return slotData.lastSlot, slotData.leastModifier, total, slotData
		local a, b, numBricks = T:getItemSlot(brick)
		if numBricks < 81 then -- enough for floor
			T:checkInventoryForItem({brick, "stone"}, {81 - numBricks, 81 - numBricks})
		end
		T:checkInventoryForItem({"stone"}, {339})
		T:checkInventoryForItem({"slab"}, {36})
		T:checkInventoryForItem({"minecraft:powered_rail", "minecraft:golden_rail"}, {8, 8})
		T:checkInventoryForItem({"minecraft:rail"}, {64})
		T:checkInventoryForItem({"minecraft:redstone_torch"}, {2})
		T:checkInventoryForItem({"minecraft:hopper_minecart"}, {1})
		T:checkInventoryForItem({"minecraft:stone_button"}, {1})
		print("Stand clear. Starting in 2 secs")
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		-- return to starting point. rail laid first, bricks placed over rails
		T:go("F2L1D5F4R1")
		lib.rail("", true, 2) -- lay 2 plain rail at start
		lib.rail("F1", false, 1) -- lay 1 plain rail
		lib.rail("F1", true, 3) -- lay 3 powered rail
		T:go("L1F1")
		T:place("minecraft:redstone_torch", -1, "down", false) --place redstone torch
		lib.rail("R2F1L1F1", false, 3)
		lib.rail("R1F1R1", false, 8)
		lib.rail("L1F1L1", false, 7)
		lib.rail("R1F1R1", false, 8)
		lib.rail("L1F1L1", false, 9)
		lib.rail("R1F1R1", false, 8)
		lib.rail("L1F1L1", false, 7)
		lib.rail("R1F1R1", false, 8)
		lib.rail("L1F1L1", false, 5) -- final strip
		lib.rail("F1", true, 3)
		T:go("F1C2R1F1R1F1")
		T:place("minecraft:redstone_torch", -1, "down", false)
		T:go("R2F1L1F1L1U1")
		-- lay floor 9 x 9 rectangle filling below
		for i = 2, 10 do -- repeat 9x
			T:go("m8", false, 0, false, brick)
			if i < 10 then
				if i % 2 == 0 then
					T:go("R1F1R1", false, 0, false, brick)
				else
					T:go("L1F1L1", false, 0, false, brick)
				end
			end
		end
		-- replace first rail with cobble and button
		T:go("R1F1R2D2x1C1B1", false, 0, false)
		T:place("minecraft:stone_button", -1, "forward", false)
		T:go("U2F2L1F1x2")
		T:place("minecraft:hopper_minecart", -1, "down", false)
		T:go("L1F1D1R2C1", false, 0, false, brick) -- cover minecart
		T:go("U1R1F2L1C0F1",false, 0, false)
		-- place slabs
		for j = 1, 4 do
			for i = 1, 9 do
				T:place("slab", -1, "up", false)
				T:forward(1)
			end
			if j < 4 then
				T:go("L1C0F1")
			end
		end
		T:go("L1F1L2") -- get in position
		-- build outer edge
		for j = 1, 4 do
			for i = 1, 9 do
				turtle.back()
				T:place("stone", -1, "forward", false)
			end
			if j < 4 then
				T:turnLeft(1)
				turtle.back()
				T:place("stone", -1, "forward", false)
			end
		end
		T:go("L1F1R2C1L1U1")
		for j = 1, 4 do
			for i = 1, 11 do
				T:go("C0x2F1")
			end
			T:go("C0x2R1F1")
		end
		T:go("R2F2R1F1R1")
		T:go("R2C1R2Q14R1Q14R1Q14R1Q13R1D1", false, 0, false)
		T:go("L1F1R1")
		T:go("R2C1R2n14R1n14R1n14R1n13R1", false, 0, false)	-- now facing in on top of outer walkway
		T:go("R1 C1U1x0 F1C1U1x0 F1C1U1x0 F1C2 F1C2 F1C2 F1C2 U1L1F1") -- back at original entrance
	end
end

function createMobSpawnerBase(pathLength)
	if pathLength > 0 then
		print("Building path to mob spawner base")
		createPath(pathLength)
		T:back(1)
	end
	T:place("stone", -1, "down", true)
	T:go("R1F1L1", false, 0, true)
	createPath(8)
	T:go("L1F1L1F1", false, 0, true)
	createPath(8)
	T:go("R1F1R1F1", false, 0, true)
	createPath(8)
	T:go("L1F1L1F1", false, 0, true)
	createPath(8)
	T:go("L1F2L1F1", false, 0, true)
end

function createMobSpawnerTower(height)	
	height = height or 2
	print("Building mob spawner base")
	-- assume placed at sea level on stone block (andesite / granite / diorite)
	--layers 2, 3 (layer 1 done at base construction)
	T:go("U1F7H2L1F1H2R1F2D1R2P1L1F1R1P1R2U1", false, 0, true)
	for i = 1, 8 do
		T:go("C2R1C1L1F1", false, 0, true)
	end
	T:go("L1F1L2C1R1F1R2C1R2", false, 0, true)
	for i = 1, 8 do
		T:go("C2R1C1L1F1", false, 0, true)
	end
	T:go("U1R2F8R1", false, 0, true)
	T:place("minecraft:water_bucket", -1, "down", false)
	T:go("F1R1", false, 0, true)
	T:place("minecraft:water_bucket", -1, "down", false)
	T:forward(16)
	T:go("R1F1D2", false, 0, true)
	for i = 1, 2 do
		sleep(0.5)
		T:place("minecraft:bucket", -1, "down", false)
	end
	T:go("R1F2", false, 0, true)
	for i = 1, 2 do
		T:go("C1R1C1R2C1R1U1")
	end
	-- height of tower
	height = math.ceil(height / 2)
	for i = 1, height do
		T:go("C1U1C1R1C1R1C1R1C1R1U1")
	end
	-- create platform for player
	T:go("R2F1L1C1R1C1R1C1U1C1L1C1L1C1L1F1L1C!R2C1L1U1F1", false, 0, true)
	-- place stone marker
	T:place("stone", -1, "down")
	-- will need to move back before completing
end

function createMobSpawnerTank()
	--layer 1 of tower + walkway
	-- move back 1 block before continuing with tower top and walkway
	T:go("R2F1R2")
	T:go("C1U2R1F1L1") -- now dropping cobble from above
	T:go("m10L1F1L1")
	T:go("m9R1F1L1F1C2F1L1F1C2L1F1")
	--layer 2
	T:go("U1F1C2R1F1C2F1L1F1m8L1F3L1m8F2L1F1L1")
	--layer 3
	T:go("U1R1F1C2L1F1C2")
	T:go("F1R1F1L1C2F1C2F1L1F1C2")
	T:go("F1C2F1L1F1C2F1C2F2C2F1")
	T:go("L1F1C2L1F2C2B1")
	--layer 4
	T:go("U1F1L1F1R2")
	for i = 1, 4 do
		T:go("F1C2F1C2F1L1")
	end
	T:go("F1R1F1R2")
	--layer 5
	T:go("U1R2F1m7L1F1L1C2F1C2F7C2F1C2")
	T:go("F1R1F1L1C2F1C2F1L1F1C2F1C2F1")
	T:go("L1F1C2F1C2F2C2L1F1L1F1C2R2F1R2")
	-- layer 6
	T:go("U1R2F9C2L1F1C2F1L1F1C2F1L1F1C2R1F8L1F2R2")
	for i = 1, 4 do
		T:go("F1C2F1C2F1L1")
	end
	T:go("F1L1F1")
	T:place("minecraft:water_bucket", -1, "down")
	T:go("R1F1L1")
	T:place("minecraft:water_bucket", -1, "down")
	T:go("R2F2R1F1R1")
	-- layer 7
	T:go("U1R2F8L1F2C2L1F1L1F1C2R1F7C2L1F2R1C2F1R1")
	for i = 1, 4 do
		T:go("F1C2F1C2F1L1")
	end
	T:go("F1R1F1R2")
	T:go("F2")
	-- place stone inside column, ready for temp water source
	T:place("stone", -1, "down", false)
	
	-- layer 8
	-- make temp water source in centre. destroy in createMobSpawnerRoof()
	T:go("F1C2R1F1C2R1F1C2F1R1F2U1R2")
	-- spiral construction
	for j = 3, 9, 2 do
		for i = 1, 4 do
			if i < 4 then
				T:go("m"..j.."L1")
			else
				T:go("m"..j.."F1R1F1L2")
			end
		end
	end
	-- fill water source
	T:go("F5L1F5")
	T:place("minecraft:water_bucket", -1, "down", false)
	T:go("F1R1F1R1")
	T:place("minecraft:water_bucket", -1, "down", false)
	T:go("F5m4F2C2F1R1F1C2F1R1F1C2F1R1F1L1C2F1m4")
	T:go("F8F2m3R1F1R1m3")
	T:go("F5L1F5m3R1F1R1m3")
	T:go("F9F2m3R1F1R1m3")
	-- layer 9
	T:up(1)
	for i = 1, 4 do
		T:go("L1F1L1m3R1F1R1m3L1F1L1m3R1F1R1m3F4")
		T:go("L1F1L1m7R1F1R1m7L1F1L1m7R1F1R1m7F1L1F1R1C2F1C2R1F4")
	end
	-- now add water
	T:go("F6")
	for i = 1, 4 do
		T:down(1)
		T:place("minecraft:bucket", -1, "down", false)
		sleep(0.5)
		T:place("minecraft:bucket", -1, "down")
		T:go("U1F8R1")
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("F1R1")
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("F8L1")
	end
	for i = 1, 2 do
		T:down(1)
		T:place("minecraft:bucket", -1, "down", false)
		sleep(0.5)
		T:place("minecraft:bucket", -1, "down", false)
		T:go("U1F4L1F4L1")
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("F9")
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("L1F5L1F4L2")
	end
	T:go("F9R1F10R1")
	-- layer 10 / 11
	for j = 1, 2 do
		T:go("U1F1m8L1F1C2F1R1F1C2F1R1F1C2F1R1F1R2m8F1R1")
		for i = 1, 3 do
			T:go("F1m17F1R1")
		end
	end
	T:go("F10R1F9D4")
end

function createMobSpawnerRoof()
	-- destroy water source first
	T:go("x2C1R1F1x2L1F1x2L1F1x2L1F1x2L2")
	T:go("U5L1F8L1F8L2") -- top left corner facing e
	T:go("m17R1m17R1m17R1m17") -- outer ring. ends facing n
	T:go("R1F2R1F1L1") -- facing e
	for i = 1, 4 do -- outer ring - 1 (with torches) ends facing e
		T:go("m6t1m3t1m5R1F1t1")
	end
	T:go("R1F1L1") -- facing e
	for i = 1, 4 do -- outer ring - 2 ends facing e
		T:go("m13R1m13R1m13R1m13")
	end
	T:go("R1F1L1") -- ends facing e
	T:go("m11R1m11R1m11R1m11") -- ends facing e
	T:go("R1F1R1F1L1F1")
	for i = 1, 4 do
		T:go("m8R1F1t1")
	end
	T:go("R1F1L1")
	T:go("m7R1m7R1m7R1m7")
	T:go("R1F1R1F1L1")
	T:go("m5R1m5R1m5R1m5")
	T:go("R1F1R1F1L1F1")
	for i = 1, 4 do
		T:go("m2R1F1t1")
	end
	T:go("R1F1L1")
	T:go("m1R1m1R1m1R1m1")
end

function createPlatform(width, length)
	local forward = true
	for w = 1, width do
		for l = 1, length do
			T:go("x2C2", false, 0, true)
			if l < length then
				T:go("F1", false, 0, true)
			end
		end
		if w < width then
			if forward then
				T:go("R1F1R1", false, 0, true)
			else
				T:go("L1F1L1", false, 0, true)
			end
		end
		forward = not forward
	end
end

function createPortal(width, height)
	T:go("D1x1", false, 0, true)
	T:place("minecraft:cobblestone", 0, "forward", true)
	for i = 1, height - 1 do
		T:go("U1x1", false, 0, true)
		T:place("minecraft:obsidian", 0, "forward", true)
	end
	T:go("U1x1", false, 0, true)
	T:place("minecraft:cobblestone", 0, "forward", true)
	for i = 1, width - 1  do
		T:go("R1F1L1x1")
		T:place("minecraft:obsidian", 0, "forward", true)
	end
	T:go("R1F1L1x1", false, 0, true)
	T:place("minecraft:cobblestone", 0, "forward", true)
	for i = 1, height - 1 do
		T:go("D1x1")
		T:place("minecraft:obsidian", 0, "forward", true)
	end
	T:go("D1x1", false, 0, true)
	T:place("minecraft:cobblestone", 0, "forward", true)
	for i = 1, width - 1 do
		T:go("L1F1R1x1")
		T:place("minecraft:obsidian", 0, "forward", true)
	end
	T:go("U1L1F1R1", false, 0, true)
end

local function createPortalPlatform()
	--[[ Used in End World to use minecarts to carry player through portal ]]
	local lib ={}
	
	function lib.findPortal()
		local found = false
		local onSide = false
		for i = 1, 64 do
			if not turtle.up() then -- hit block above
				found = true
				break
			end
		end
		if found then
			-- are we under the centre block, or one of the sides?
			if turtle.detect() then -- under a side
				onSide = true
			else	-- nothing in front, probably under centre, or facing wrong direction so check
				for i = 1, 4 do
					turtle.turnRight()
					if turtle.detect() then
						onSide = true
						break
					end
				end
			end
			if onSide then-- move to centre
				T:go("D1F1")
			end
		end
		local height = 3 -- allows for 2 bedrock + starting space
		while turtle.down() do
			height = height + 1
		end
		return found, height
	end
	
	function lib.addFloor(length)
		for i = 1, length do
			if i < length then
				T:go("C2F1", false, 0, true)
			else
				T:go("C2", false, 0, true)
			end
		end
	end
	
	function lib.buildLadder(height)
		for i = 1, height do
			T:go("F1C1 R1C1 L2C1 L1F1L2", false, 0, true)
			if i > 3 then
				T:go("C2")
			end
			T:place("minecraft:ladder", 0, "forward", true)
			T:up(1)
		end
	end
	
	local found, height = lib.findPortal()
	if found then	-- position under centre of beacon
		-- build ladder up and create platform
		T:go("L1F1L1F2L2")
		T:checkInventoryForItem({"minecraft:ladder"},{height})
		T:checkInventoryForItem({"stone"},{height * 4 + 40})
		lib.buildLadder(height)

		T:go("F1R1F4R2")			-- turn right, forward 4, reverse
		for i = 1, 5 do				-- build 7 x 5 platform
			lib.addFloor(7)			-- forward place block above to 7 blocks
			if i == 1 or i % 2 == 1 then -- 1,3,5,7
				T:go("L1F1L1")
			else
				T:go("R1F1R1")
			end
		end
		T:go("F3L1F4")			-- facing portal entrance, 1 block short
		T:place("minecraft:rail", -1, "forward", false)
		T:go("U1R2")
		T:place("minecraft:rail", -1, "down", false)
		T:forward(1)
		if not T:place("minecraft:powered_rail", -1, "down", false) then
			T:place("minecraft:golden_rail", -1, "down", false)
		end
		T:go("F1C2 U1R2C2 F1")
		T:place("minecraft:minecart", -1, "down", false)
		T:go("F1R2D1")
		T:place("minecraft:stone_button", -1, "forward", false)
	else
		print("Portal not found. Move me under\nthe centre if possible. \n(wait for purple beacon.")
	end
end

local function createRailwayDown(drop)
	-- go(path, useTorch, torchInterval, leaveExisting, preferredBlock)
	if drop == 0 then
		local blockTypeD = T:getBlockType("down")
		while blockTypeD == "" do
			T:go("F1D1", false, 0, true)
			blockTypeD = T:getBlockType("down")
			if blockTypeD == "" then
				T:go("C2", false, 0, true)
			end
		end
	else
		for i = 1, drop - 1 do
			T:go("F1D1C2", false, 0, false)
		end
	end
end

function createRailwayUp(up)
	for i = 1, up do
		T:go("C1U1F1", false, 0, false)
	end
end

function createRetainingWall(length, height)
	local place = false
	if height <= 0 then
		height = 30
	end
	local inWater = false
	for i = 1, 4 do
		if string.find(T:getBlockType("forward"), "water") ~= nil then
			inWater = true
		end
		T:turnRight(1)
	end
	
	local y = 1
	-- go(path, useTorch, torchInterval, leaveExisting)
	-- start at surface, move back 1 block
	-- each iteration completes 3 columns
	local numBlocks = T:getSolidBlockCount()
	print("Solid blocks in inventory: "..numBlocks)
	
	if not inWater then
		T:down(1)
	end
	place = clearVegetation("down") -- returns true if air, water or lava below
	if length == 1 then --single column
		local place = clearVegetation("down")
		while place do -- loop will be entered at least once
			T:down(1)
			y = y + 1
			place = clearVegetation("down")
		end
		for i = 1, y - 1  do
			T:go("U1C2", false, 0, true, false)
		end
	elseif length == 2 then--down then up
		T:back(1) -- move 1 block away from wall edge
		local place = clearVegetation("down")
		while place do -- loop will be entered at least once
			T:go("C1D1", false, 0, true, false)
			y = y + 1
			place = clearVegetation("down")
		end
		T:forward(1) -- in case col in front is deeper
		place = clearVegetation("down")
		while place do -- loop will be entered at least once
			T:down(1)
			y = y + 1
			place = clearVegetation("down")
		end
		T:go("B1C1", false, 0, true) 
		for i = 1, y - 1 do
			T:go("U1C2", false, 0, true)
		end
		T:go("C1", false, 0, true, false)
	else -- length 3 or more eg 3,22; 11
		local remain = length % 3 -- 0; 1; 2 only possible results
		length = length - remain -- 3-0=3; 4-1=3; 5-2=3; 6-0=6
		for i = 3, length, 3 do -- 3, 6, 9, 12 etc
			numBlocks = T:getSolidBlockCount()
			print("Solid blocks in inventory: "..numBlocks)
			if numBlocks < height * 3 then
				--ask player for more
				T:checkInventoryForItem({"stone"}, {height * 3}, false)
			end
			T:go("B1C1", false, 0, true)
			if i > 3 then -- second iteration
				T:go("B1C1")
			end
			-- place blocks forward while descending
			place = clearVegetation("down")
			while place do -- loop will be entered at least once
				T:go("C1D1", false, 0, true)
				y = y + 1
				place = clearVegetation("down")
			end
			T:forward(1) -- in case col in front is deeper
			place = clearVegetation("down")
			while place do -- loop will be entered at least once
				T:down(1)
				y = y + 1
				place = clearVegetation("down")
			end
			while not turtle.detectUp() do	-- go up until column base is met
				T:go("U1C2")
				y = y - 1
				if y < 0 then
					break
				end
			end
			T:go("B1C1B1", false, 0, true) 
			-- return to surface, placing forward and below
			for i = 1, y - 1 do
				T:go("C1U1C2", false, 0, true)
			end
			-- put missing block down
			T:go("C1", false, 0, true)
			y = 1 -- reset counter
		end
		if remain == 1 then -- 1 more column
			y = 1
			T:back(1)
			place = clearVegetation("down")			
			while place do -- loop will be entered at least once
				T:down(1)
				y = y + 1
				place = clearVegetation("down")
			end
			for i = 1, y - 1 do
				T:go("U1C2", false, 0, true)
			end
			-- put missing block down
			T:go("C1", false, 0, true)
		elseif remain == 2 then -- 2 cols
			y = 1
			T:back(1)
			place = clearVegetation("down")
			while place do -- loop will be entered at least once
				T:go("C1D1", false, 0, true)
				y = y + 1
				place = clearVegetation("down")
			end
			T:forward(1)
			place = clearVegetation("down")
			while place do
				T:down(1)
				y = y + 1
				place = clearVegetation("down")
			end
			T:go("B1C1", false, 0, true) 
			for i = 1, y - 1 do
				T:go("U1C2", false, 0, true)
			end
			-- put missing block down
			T:go("C1", false, 0, true)
		end
	end
end

local function createSafeDrop(height)
	-- dig down height blocks, checking for blocks on all sides
	local drop = 0
	T:down(2)
	drop = 2
	for i = 1, height - 1 do
		for j = 1, 4 do
			-- go(path, useTorch, torchInterval, leaveExisting, preferredBlock)
			T:go("C1R1", false, 0, true)
		end
		if T:down(1) then
			 drop = drop + 1
		end
		if T:isWaterOrLava("up") ~= "" then
			T:go("C0x0", false, 0, false) -- delete water/ lava block
		end
	end
	T:go("U1R2x1")
	--place(blockType, damageNo, direction, leaveExisting, signText)
	T:place("minecraft:water_bucket", -1, "down", false)
	T:go("U1x1")
	T:up(drop - 2)
end

function createSandWall(length)
	length = length or 0
	local success = true
	--move above water
	local maxMove = 2
	while turtle.detectDown() and maxMove > 0 do
		T:forward(1)
		maxMove = maxMove - 1
	end
	if length > 0 then
		for i = 1, length - 1 do
			success = dropSand()
			T:forward(1, false)
		end
		success = dropSand()
	else
		while not turtle.detectDown() do -- over water
			while not turtle.detectDown() do -- nested to allow forward movement
				success = dropSand() -- drops sand and checks supplies
			end
			if success then
				T:forward(1, false)
			else -- out of sand
				break
			end
		end
	end
end

function createSinkingPlatform(width, length, height)
	local lib = {}
	function lib.stage1a()
		for l = 1, length do 
			if l == 1 then
				T:go("L1C1R1C2U1C2D1F1C2", false, 0, true)
			elseif l < length then
				T:go("L1C1R1C2F1C2", false, 0, true)
			else
				T:go("L1C1R1C2C1U1C2D1", false, 0, true)
			end
		end
	end
	
	function lib.stage1b()
		for l = 1, length do 
			if l == 1 then
				T:go("R1C1L1C2U1C2D1F1C2", false, 0, true)
			elseif l < length then
				T:go("R1C1L1C2F1C2", false, 0, true)
			else
				T:go("R1C1L1C2C1U1C2D1", false, 0, true)
			end
		end
	end
	
	function lib.stage2(forward)
		if forward then
			T:go("C1R1F1L1C1R2", false, 0, true)
		else
			T:go("C1L1F1R1C1L2", false, 0, true)
		end
	end
		
	local forward = true
	local goingRight = true
	for h = 1, height do
		T:down(1) -- move down into existing platform
		if goingRight then -- first side
			if forward then
				-- complete left side
				T:go("R2C1L2", false, 0, true) -- block 1, 1
				lib.stage1a()
				-- turn ready for next side
				T:go("R1F1L1C1R2C2")
			else
				T:go("L2C1R2", false, 0, true) -- block 1, 1
				lib.stage1b()
				-- turn ready for next side
				T:go("L1F1R1C1L2C2")
			end
		else -- on right side so different approach
			if forward then
				T:go("L2C1R2", false, 0, true) -- block 1, 1
				lib.stage1b()
				-- turn ready for next side
				T:go("C1L1F1R1C1L2C2")
			else
				-- complete left side
				T:go("R2C1L2", false, 0, true) -- block 1, 1
				lib.stage1a()
				-- turn ready for next side
				T:go("C1R1F1L1C1R2C2")
			end
		end
		forward = not forward
		-- continue strips across until at far edge
		for w = 1, width - 2 do
			for l = 1, length do
				if l < length then
					T:go("C2F1", false, 0, true)
				else
					T:go("C2", false, 0, true)
				end
			end
			if goingRight then
				lib.stage2(forward)
			else
				lib.stage2(not forward)
			end
			forward = not forward
		end
		-- far side
		if goingRight then
			if forward then
				lib.stage1b()
			else
				lib.stage1a()
			end
		else
			if forward then
				lib.stage1a()
			else
				lib.stage1b()
			end
		end
		goingRight = not goingRight
		T:turnRight(2)
		forward = not forward
	end
end

function createSolid(width, length, height)
	-- this function currently not used
	for i = 1, width do --width could be 1, 2, 3 etc
		createRetainingWall(length, height)
		if i < width then --width = 2 or more
			if i == 1 or i % 2 == 1 then -- iteration 1, 3, 5
				T:go("L1F1L2C1R1", false, 0, true)
			else
				T:go("R1F1R2C1L1", false, 0, true)
			end
		end
	end
end

local function createStaircase(destination, currentLevel, destLevel)
	-- R# L# F# B# U# D# +0 -0 = Right, Left, Forward, Back, Up, Down, up while detect and return, down while not detect
	-- dig:			  x0,x1,x2 (up/fwd/down)
	-- suck:		  s0,s1,s2
	-- place chest:   H0,H1,H2 
	-- place sapling: S0,S1,S2
	-- place Torch:   T0,T1,T2
	-- place Hopper:  P0,P1,P2
	-- mine floor:	  m# = mine # blocks above and below, checking for valuable items below, and filling space with cobble or dirt
	-- mine ceiling:  M# = mine # blocks, checking for valuable items above, and filling space with cobble or dirt
	-- mine ceiling:  N# same as M but not mining block below unless valuable
	-- place:		  C,H,r,S,T,P,^ = Cobble / cHest / DIrT / Sapling / Torch / hoPper /stair in direction 0/1/2 (up/fwd/down) eg C2 = place cobble down
	
	-- 3| |B| |
	--   - - - 
	-- 2|A| |C|
	--   - - - 
	-- 1|^|D| |
	--   - - - 
	--   1 2 3 
	local function checkFluids()
		local isFluid = false
		-- check if water or lava present
		for i = 1, 4 do
			blockType = T:isWaterOrLava("forward")
			if blockType:find("lava") ~= nil or blockType:find("water") ~= nil then
				isFluid = true
			end
		end
		return isFluid
	end
	
	local function createStaircaseSection(onGround)
		-- start 1,1,1, n
		-- stage A
		local isFluid = checkFluids()
		local blockType = ""
		local data = T:getStock("stairs")
		if data.total == 0 then
			T:craft('stairs', 4)
		end
		if onGround and isFluid then
			-- add right side and block entrance
			T:go("R1C1R1C1R2")
		end
		if isFluid then
			T:go("L1C1 R1F1C2 L1C1 R1x1 R1C1 L1C2B1 C1x1 ^1C2", false, 0, true) --start:1,1,1,n stairs A on level 1, going back to 1,1,1,n
		else
			T:go("F1x1 R1C1 L1C2B1 ^1C2", false, 0, true)
		end
		if not onGround then
			-- stage A1
			T:go("L2C1L2", false, 0, true) -- start 1,1,1,n fix corner on level 1 end: 1,1,1,n
		end
		-- stage B
		T:go("U1L1", false, 0, true) -- end  1,1,1,w layer 2
		isFluid = checkFluids()
		if isFluid then
			T:go("C1", false, 0, true) -- end  1,1,1,w layer 2
		end
		if not onGround then
			if isFluid then
				T:go("L1C1R1", false, 0, true) -- end  1,1,1,w layer 2
			end
		end
		-- stage C1
		if isFluid then
			T:go("R1C1F1C1x1 L1C1 R2C1 L1B1", false, 0, true)
		else
			T:go("R1F1 R1C1 L1B1", false, 0, true)
		end
		-- stage C2
		T:go("U1")
		isFluid = checkFluids()
		if isFluid then
			T:go("L1C1L1 C1L2 C1F1L1 C1R2 C1L1 B1C2D1", false, 0, true) -- end 1,1,2,n
		else
			T:go("F1R1 C1L1 B1D1", false, 0, true) -- end 1,1,2,n
		end
		onGround = false
		-- stage D
		isFluid = checkFluids()
		if isFluid then
			T:go("C1F1C1F1C1x1L1 C1R1 C1R1", false, 0, true) -- 3,1,2,e
		else
			T:go("F2 C1R1", false, 0, true) -- 3,1,2,e
		end
		
		return onGround
	end

	--local height = currentLevel -- eg 64 at top or 5 at bedrock
	local data = T:getStock("stairs")
	--{rt.total, rt.mostSlot, rt.leastSlot, rt.mostCount, rt.leastCount}
	local numStairs = data.total
	local range = math.abs(destLevel - currentLevel)
	local numStairsNeeded = range
	numStairsNeeded = numStairsNeeded - numStairs
	if numStairsNeeded > 40 then
		print('crafting '..numStairsNeeded..' : '..numStairs.. ' in stock')
		T:craft('stairs', 40)	-- max 40 so repeat
		data = T:getStock("stairs")
		if data.total == 0 then
			data = T:getStock("stairs")
		end
		numStairs = data.total
		numStairsNeeded = numStairsNeeded - numStairs
	end
	if numStairsNeeded >  0 then
		T:craft('stairs', numStairsNeeded)
	end
	local height = 0
	if destination == "bedrock" then -- go down towards bedrock
		local atBedrock = false
		for i = 1, range do
			height = height	- 1
			if not T:down() then
				atBedrock = true
				break
			end
		end
		if atBedrock then -- hit bedrock so get to level 5 / -59
			height = T:findBedrockTop(height)
			T:go("R1F1R1", false, 0, true)
		end
	end
	local onGround = true
	height = 0
	while height < range do
		for x = 1, 4 do
			onGround = createStaircaseSection(onGround)
		end
		height = height + 4
	end
end

function createTreefarm(size)
	local blockType
	local blockModifier
	local length
	local width
	
	if size == 1 then
		length = 11
		width = 6
		clearArea(11, 11)
	else
		length = 19
		width = 10
		clearArea(19, 19)
	end
	-- now place dirt blocks and torches
	T:go("F2R1F2L1U1", false, 0, true)
	for x = 1, (width - 2) / 2 do
		for y = 1, (length - 3) / 2 do
			T:place("minecraft:dirt", -1, "down", false)
			if y < (length - 3) / 2 then
				T:forward(1)
				T:place("minecraft:torch", -1, "down", false)
				T:forward(1)
			end
		end
		T:go("R1F2R1", false, 0, true)
		for y = 1, (length - 3) / 2 do
			T:place("minecraft:dirt", -1, "down", false)
			if y < (length - 3) / 2 then
				T:forward(1)
				T:place("minecraft:torch", -1, "down", false)
				T:forward(1)
			end
		end
		if x < (width - 2) / 2 then
			T:go("L1F2L1", false, 0, true)
		else
			T:go("R1F6", false, 0, true)
			if size == 2 then
				T:go("F8", false, 0, true)
			end
			T:go("R1B1", false, 0, true)
		end
	end
end

function createWalkway(length)
	local lengthParts = math.floor(length / 8) -- eg 12/8 = 1
	local lastPart = length - (lengthParts * 8) -- eg 12 - (1 * 8) = 4
	T:up(1)
	for j = 1, lengthParts do
		T:go("M8", false, 0, true)
	end
	if lastPart > 0 then
		T:go("M"..tostring(lastPart)) -- eg m4
	end
	T:go("R2D1", false, 0, true)
	T:place("minecraft:torch", 0, "up", false)
	for j = 1, lengthParts do
		T:go("m8", true)
		T:place("minecraft:torch", 0, "up", false)
	end
	if lastPart > 0 then
		T:go("m"..tostring(lastPart), true) -- eg m4
	end
	T:go("R2", false, 0, true)
end

function createWaterSource(level)
	if level == nil then
		level = 0
	end
	if level > 0 then
		T:up(level)
	elseif level < 0 then
		T:down(math.abs(level))
	end
	-- assume on flat surface, but allow for blocks above
	T:go("x0C2F1 x0C2F1 x0C2F1 x0C2R1 F1 x0C2F1 x0C2F1 x0C2R1 F1 x0C2F1 x0C2F1 x0C2R1 F1 x0C2F1 x0C2", false, 0, false)
	T:go("R1F1D1", false, 0, false) --move to corner and drop down
	T:go("C2F1R1 C2F1R1 C2F1R1 C2F1R1", false, 0, false)
	T:go("U1")
	for i = 1, 2 do
		T:place("minecraft:water_bucket", -1, "down", false)
		T:go("F1R1F1R1", false, 0, false)
	end
	-- refill water buckets
	for i = 1, 2 do
		sleep(0.5)
		T:place("minecraft:bucket", -1, "down", false)
	end
	T:go("R2F1R1F1R1")
	-- end above lower left of pond (starting point)
end

function decapitateBuilding(width, length)
	--clearRectangle with sand drop
	-- could be 1 wide x xx length (trench) up and return
	-- could be 2+ x 2+
	-- even no of runs return after last run
	-- odd no of runs forward, back, forward, reverse and return
	local success
	local directReturn = true
	if width % 2 == 1 then
		directReturn = false
	end
	if width == 1 then -- trench ahead, so fill then return
		for i = 1, length - 1 do
			success = dropSand()
			T:forward(1, false)
		end
		success = dropSand()
		T:go("R2F"..(length - 1).."R2", false, 0, false)
	else --2 or more columns
		if directReturn then -- width = 2,4,6,8 etc
			for i = 1, width, 2 do -- i = 1,3,5,7 etc
				-- move along length, dropping sand
				for j = 1, length - 1 do
					success = dropSand()
					T:forward(1, false)
				end
				success = dropSand()
				T:go("R1F1R1") --turn right and return on next column
				for j = 1, length - 1 do
					success = dropSand()
					T:forward(1, false)
				end
				success = dropSand()
				if i < width - 2 then -- eg width = 8, i compares with 6: 1, 3, 5, 7
					T:go("L1F1L1")
				end
			end
			T:go("R1F"..width - 1 .."R1") --return home
		else
			for i = 1, width, 2 do -- i = 1,3,5,7 etc
				-- move along length, dropping sand
				for j = 1, length - 1 do
					success = dropSand()
					T:forward(1, false)
				end
				success = dropSand()
				T:go("R1F1R1") --turn right and return on next column
				for j = 1, length - 1 do
					success = dropSand()
					T:forward(1, false)
				end
				success = dropSand()
				T:go("L1F1L1")
			end
			-- one more run then return
			for j = 1, length - 1 do
				success = dropSand()
				T:forward(1, false)
			end
			success = dropSand()
			T:go("R2F"..length.."R1F"..width - 1 .."R1")
		end
	end
end

function demolishBuilding(width, length)
	-- start bottom left
	clearBuilding(width, length, 0, false)
end

local function deactivateDragonTower()
	-- go up centre of tower to bedrock
	local height = 0
	--numBlocksMoved, errorMsg = clsTurtle.doMoves(self, numBlocksRequested, direction)
	local numBlocks, message = T:doMoves(1, "up")
	while message == nil do
		numBlocks, message = T:doMoves(1, "up")
		height = height + 1
	end
	-- go round bedrock and destroy crystal
	T:go("F1R2U2x1U1x1")
	-- return to start
	T:down(height + 5)
end

local function undermineDragonTowers()
	--[[
	        -13, -40....12, -40						NNW (4)   	NNE (5)
			
	    -34, -25............33, -25				NWW	(2)				NEE (9)
		
	-42, -1....................42, 0		W (1)						E (8)
	
	     -34, 24............33,24				SWW	(3)				SEE (10)
		 
		      -13,39....12, 39						SSW	(7)		SSE (6)
	
	North towers centres 25 blocks apart, 40 blocks north of axis
	Mid-North towers 67 blocks apart, 25 blocks north of axis
	W-E centres 84 blocks apart, on 0 axis
	Mid-south towers 67 blocks apart, 24 blocks south of axis
	South towers centres 25 blocks apart, 39 blocks south of axis
	]]
	
	local lib = {}
	function lib.findNextTower(maxDistance, withMarker)
		local distance = 0
		local blockTypeF = T:getBlockType("forward")
		local blockTypeD = T:getBlockType("down")
		for i = 1, maxDistance do
			if blockTypeF ~= "minecraft:obsidian" and blockTypeD ~= "minecraft:obsidian" then -- not in a tower
				if withMarker then -- used to mark 0 coordinate
					T:place("cobble", -1, "down", false) -- place cobblestone or cobbled deepslate to mark zero coordinate
				end
			else	-- obsidian found, could still be in an earlier tower
				if i > 10 then
					break
				end
			end
			T:go("F1x0")
			distance = distance + 1
			blockTypeF = T:getBlockType("forward")
			blockTypeD = T:getBlockType("down")
		end
		if distance == maxDistance then -- obsidian not found ? wrong place/ direction
			print("Obsidian not found")
			error()
		end
		-- will now be at side of a tower
		lib.findCentre() -- move into tower to find the other side
		return distance
	end
	
	function lib.findCentre()
		local width = 0
		-- while obsidian in front or below (previously entered tower) measure width and return to centre
		local blockTypeF = T:getBlockType("forward")
		local blockTypeD = T:getBlockType("down")
		while blockTypeF == "minecraft:obsidian" or blockTypeD == "minecraft:obsidian" do
			T:go("F1x0")
			width = width + 1
			blockTypeF = T:getBlockType("forward")
			blockTypeD = T:getBlockType("down")
		end
		-- will always go outside the tower 1 block. width of 5 block tower = 6
		T:go("R2F"..math.ceil(width / 2)) --return to centre of tower
		T:turnLeft(1) -- now find another edge of the tower, dig forward until out of obsidian
		for i = 1, math.ceil(width) do  -- give additional loops if missed centre
			blockTypeF = T:getBlockType("forward")
			blockTypeD = T:getBlockType("down")
			if blockTypeF == "minecraft:obsidian" or blockTypeD == "minecraft:obsidian" then
				T:go("F1x0")
			else
				break
			end
		end
		-- now outside different edge of the tower
		-- reverse and move width/2, dig up + 1 to mark centre, face original direction
		T:go("L2F"..math.ceil(width / 2).."R1U2x1")
		T:place("minecraft:end_stone", -1, "forward", false) -- place endstone to mark facing direction
		T:down(2)
	end
	
	function lib.findPath(maxLength)
		local blockTypeD = T:getBlockType("down")
		local distance = 0
		while blockTypeD:find("cobble") == nil and distance < maxLength do
			T:go("F1x0")							-- return to 0 axis, 
			distance = distance + 1
			blockTypeD = T:getBlockType("down")
		end
		return distance
	end
	
	-- start at 0,y,0, facing West
	T:dig("up")									-- in case not already done
	local maxLength = 0
	local blockTypeD
	local distance = lib.findNextTower(45, true)-- find W tower (1) and mark trail with cobble
	T:turnRight(2)						
	for i = 1, 8 do								-- head back East 8 blocks, turn left (facing north)
		T:go("F1x0")							-- this path may be off-axis, so dig double height
	end
	T:turnLeft(1)
	lib.findNextTower(30)						-- find NWW tower (2)
	T:turnRight(2)
	distance = lib.findPath(30)
	distance = distance + lib.findNextTower(30)	-- find SWW tower (3)
	T:turnRight(2)
	distance = lib.findPath(30)
	T:turnRight(1) 								-- should be on cobble path
	for i = 1, 21 do							-- move East 21 blocks, turn left facing North
		T:go("F1x0")
	end
	T:turnLeft(1)
	
	distance = lib.findNextTower(45)		-- find NNW tower (4)
	T:turnRight(1)							
	distance = lib.findNextTower(30)		-- find NNE tower (5)
	T:turnRight(1)
	distance = lib.findNextTower(85)		-- find SSE tower (6)
	T:turnRight(1)

	distance = lib.findNextTower(30)		-- find SSW tower (7)
	T:turnRight(1)
	distance = lib.findPath(40)				-- head North to 0 axis
	T:go("R1F13") 							-- return to 0,0 facing East
	distance = lib.findNextTower(45, true)	-- find E tower (8)
	
	T:turnRight(2)						
	for i = 1, 9 do
		T:go("F1x0")						-- this path may be off-axis, so dig double height
	end
	T:turnRight(1)
	
	distance = lib.findNextTower(30)		-- find NEE tower (9)
	T:turnRight(2)
	distance = lib.findPath(30) -- return to 0 axis
	distance = lib.findNextTower(30)		-- find SEE tower (10)
	T:turnRight(2)
	distance = lib.findPath(30) 			-- return to 0 axis
	T:go("L1F33")							-- return to 0, 0
end

local function demolishPortal(width, height)
	for i = 1, height do
		T:dig("forward")
		if i < height then
			T:up(1)
		end
	end
	for i = 1, width do
		if i < width then
			T:go("R1F1L1x1")
		end
	end
	for i = 1, height do
		T:dig("forward")
		if i < height then
			T:down(1)
		end
	end
	for i = 1, width do
		if i < width then
			T:go("L1F1R1x1")
		end
	end
end

function digMobTrench(height, length)
	local blockType
	-- go down 1 layer at a time height x, move forward length, fill voids
	if length == 0 then
		length = 8 --common length
	end
	-- check if already facing the wall
	if turtle.detect() then
		T:turnRight(2)
	end
	for i = 1, height do
		T:down(1)
		-- tunnel bottom: E# fill voids both sides, remove floor
		T:go("E"..length - 1 .."R2", false, 0 , true)
	end
	T:up(height)
	if height % 2 == 1 then
		T:forward(length - 1)
	end
end

function digTrench(height, length)
	local blockType
	-- go down height, move forward
	if length == 0 then
		length = 4096 -- will go out of loaded chunks and stop or max 4096 on a server
	end
	for i = 1, length, 2 do
		local count = 0
		for down = 1, height do
			blockType = T:isWaterOrLava("down") 
			-- go down only if no water or lava below
			if string.find(blockType, "water") == nil and string.find(blockType, "lava") == nil then
				T:down(1)
				count = count + 1
			end 
		end
		-- return to surface, continue if block above
		T:go("U"..count)
		-- go up while block in front
		while turtle.detect() do
			blockType = T:getBlockType("forward")
			--print("Ahead: "..blockType)
			if T:isVegetation(blockType) then
				T:dig("forward")
				break
			elseif blockType:find("log") ~= nil then
				T:harvestTree("forward", false)
			else
				T:up(1)
			end
		end
		-- move forward
		T:forward(1)
		-- go down until block detected
		while not turtle.detectDown() do
			blockType = T:isWaterOrLava("down") 
			if string.find(blockType, "water") == nil and string.find(blockType, "lava") == nil then
				T:down(1)
			else
				break
			end
		end
	-- repeat
	end
end

function drainLiquid(width, length, size)
	if size == 1 then --turtle replaces source so use clearSolid()
		--top-bottom =1
		clearSolid(width, length, 1, size)
	else -- mc 1.12.15+ turtle does NOT replace source blocks
		if width == 0 then
			width = 64
		end
		if length == 0 then
			length = 64
		end
		local drop = 0
		local calcLength = 1
		local calcWidth = 0

		-- start above water
		if T:detect("down") then -- in case not over wall
			T:forward(1)
		end
		-- go down until water detected
		while not turtle.detectDown() do
			block, blockType = T:isWaterOrLava("down") 
			if string.find(block, "water") == nil and string.find(block, "lava") == nil then
				T:down(1)
				drop = drop + 1
			else
				break
			end
		end
		
		local place = true
		local block, blockType
		-- place first cobble along the length of water and measure length
		for l = 1, length do
			-- check for water/kelp below
			place = clearVegetation("down") -- returns true if water or removed kelp below
			if place then
				T:go("C2")
			end
			if T:getBlockType("forward") == "minecraft:cobblestone" then
				break
			end
			if l < length then
				if T:getBlockType("forward") == "minecraft:cobblestone" then
					break
				else
					T:forward(1)
					calcLength = calcLength + 1
					if T:getBlockType("down") == "minecraft:cobblestone" then
						turtle.back()
						calcLength = calcLength - 1
						break
					end
				end
			end
		end
		length = calcLength
		T:go("R1F1R1")
		print("calcLength = "..length)
		for w = 1, width- 2, 2 do
			-- place cobble along the length of water return journey
			for l = 1, length do
				-- check for water/kelp below
				place = clearVegetation("down") -- returns true if water or removed kelp below
				if place then
					T:go("C2")
				end
				if l < length then
					T:forward(1)
					T:go("C2")
				end
			end
			-- go to starting point
			T:go("R1F1R1")
			-- remove cobble along the length of water
			for d = 1, length do
				T:dig("down")
				if d < length then
					T:forward(1)
				end
			end
			-- turn right
			T:turnRight(1)
			-- move across to next col 
			for i = 1, 2 do
				if not turtle.detect() then
					T:forward(1)
					calcWidth = calcWidth + 1
				end
			end
			T:turnRight(1)
			--check if now cobble below. If so run has finished
			if T:getBlockType("down") == "minecraft:cobblestone" then
				T:go("R1F1L1")
				calcWidth = calcWidth - 1
				break
			elseif turtle.detect() then
				-- end of line reached
				break
			end
		end
		-- now on retaining wall, return to last cobble strip
		
		-- dig it out
		for d = 1, length do
			T:dig("down")
			if d < length then
				T:forward(1)
			end
		end

		T:go("R1F"..calcWidth - 1 .."R1U"..drop)
		
	end
end

function dropSand()
	local success, slot
	while not turtle.detectDown() do -- over water. will be infinite loop if out of sand
		success, slot = T:place("minecraft:sand", -1, "down", false)
		if not success then
			print("Out of sand. Add more to continue...")
			sleep(2)
		end
	end
	return true --will only get to this point if turtle.detectDown() = true
end

function floodMobFarm()
	-- turtle on floor, pointing towards water source wall
	-- move forward until hit wall
	while turtle.forward() do end
	-- turn left, move forward until hit wall
	T:turnLeft(1)
	while turtle.forward() do end
	-- back 1, place water
	turtle.back()
	T:place("minecraft:water_bucket", -1, "forward", true)
	-- turn round go forward 7, place water
	T:turnLeft(2)
	while turtle.forward() do end
	-- back 1, place water
	turtle.back()
	T:place("minecraft:water_bucket", -1, "forward", true)
	
	-- turn round, go forward 3 (centre of wall), turn left, forward 4 (centre of chamber)
	T:go("L2F3L1F4")
	-- go down, left, forward, turn round
	T:go("D1L1F1R2")
	for i = 3, 9, 2 do
		-- check down, dig forward, go forward, check down (i blocks)
		T:go("m"..i-1, false, 0, true)
		if i == 3 or i == 7 then
			-- left, forward, right, forward, turn round
			T:go("L1F1R1F1R2")
		elseif i < 9 then
			T:go("R1F1L1F1R2")
			-- right, forward, left, forward, turn round
		end
	end
	-- right, forward, right, check down / forward 9 x
	T:go("R1F1R1m8R2F4R1") -- now facing bubble lift, next to wall
	-- go down 2, check floor, up 1, place fence
	T:go("D2C2U1", false, 0, true)
	T:place("fence", -1, "down", false)
	T:go("F1D1C2U1", false, 0, true)
	T:place("fence", -1, "down", false)
	T:go("F1U1R2", false, 0, true)
	T:go("F1R1U1")
	T:place("sign", -1, "down", false)
	T:go("U1C0D1")
	T:place("slab", -1, "up", false)
	T:go("R2F1R2")
	T:place("sign", -1, "forward", false)
	T:go("R1F1R2C1R1F1D1L1") --sitting on soul sand/dirt facing spawner
	if not T:place("minecraft:soul_sand", -1, "down", false) then
		T:place("minecraft:dirt", -1, "down", false)
	end
end

local function harvestRun(runLength)
	local blockType
	local blockModifier

	for i = 1, runLength do
		blockType, blockModifier = T:getBlockType("forward") -- store information about the block in front in a table
		if blockType ~= "" then
			if blockType:find("log") ~= nil then
				T:harvestTree(true, false)
			else
				T:forward(1)			
			end	
		else
			T:forward(1)
		end
	end
end	

local function harvestTreeFarm(size)
	local loopCount
	-- return blockType, blockModifier -- eg  'minecraft:log', 0 on older versions, "minecraft:oak_log" , nil on newer
	local blockType, _ = T:getBlockType("forward") -- store information about the block in front in a table
	if size == 0 then --fell single tree
		if blockType ~= "" then
			if blockType:find("log") ~= nil then
				T:harvestTree(true, false)			
			end	
		end
	else
		if size == 1 then
			loopCount = 4
		else
			loopCount = 8
		end
		if blockType ~= "" then
			if blockType:find("dirt") ~= nil or blockType:find("grass") ~= nil then
				T:up(1)
			end
		end
		for i = 1, loopCount do
			harvestRun(loopCount * 2)
			turtle.turnRight()
			harvestRun(1)
			turtle.turnRight() --now facing opposite direction
			harvestRun(loopCount * 2)
			if i < loopCount then -- turn left if not on last run
				turtle.turnLeft()
				harvestRun(1)
				turtle.turnLeft()
			end
		end
		--return to starting position
		turtle.turnRight()
		harvestRun(loopCount * 2 - 1)
		turtle.turnRight()
	end
end

local function getBoolean(prompt)
	write(prompt..": ")
	local choice  = ""
	while choice:lower() ~= "y" and choice:lower() ~= "n" do
		choice = read()
	end
	if choice:lower() == "y" then
		return true
	else
		return false
	end
end

local function getSize(clear, prompt, lowerLimit, upperLimit, default)
	local retValue = bedrock - 1
	while tonumber(retValue) < lowerLimit or  tonumber(retValue) > upperLimit do
		if clear then
			T:clear()
		end
		if type(prompt) == "table" then
			for i = 1, #prompt do
				if i < #prompt then
					print(prompt[i])
				else
					if prompt[i]:find(": ") == nil then
						prompt[i] = prompt[i]..": "
					end
					term.write(prompt[i])
				end
			end
		else
			if prompt:find(": ") == nil then
				prompt = prompt..": "
			end
			term.write(prompt)
		end
		retValue = read()
		if tonumber(retValue) == nil then -- eg enter or non-numeric input
			if default ~= nil then
				retValue = default
			else
				retValue = 0
			end
		end
		clear = true
	end
	return tonumber(retValue)
end

local function getTaskItemsList()
	-- list of items required for each task
	local text = {}
	--MINING
	text[11] = {"24 torch (optional)", "1 bucket (optional)", "64 stone", "1 chest"} 			-- mine at this level
	text[12] = {"ladder from this level up / down","levels/4 torch (optional)","levels*4 stone"}-- ladder to bedrock
	text[13] = {"stairs from this level up / down", "6*levels stone", "1 chest"} 				-- stairs up/down
	text[14] = {"levels * 4 stone","water_bucket"} 												-- safe drop to water block
	text[15] = {"levels * 4 stone", "1 soul sand", "1 water bucket"} 							-- single column bubble lift
	text[16] = {"1 UNUSED diamond sword (optional)"}											-- salvage mineshaft
	text[17] = {"1 bucket (optional)", "64 stone"} 												-- quick mine
	text[18] = {"1 bucket (optional)", "64 stone"}												-- quick corridor
	text[19] = {"1 bucket (optional)"}															-- mine to bedrock

	-- FORESTRY
	text[21] = {"1 chest (optional)"}-- Fell Tree
	text[22] = {"64 dirt", "16 or 64 torch (optional)"} --Create treefarm
	text[23] = {"4 saplings"} --plant treefarm
	text[24] = {} -- Harvest treefarm
	text[25] = {"3 chest","128 dirt","128 cobble","2 water buckets","1 hopper","21 torch","21 saplings (optional)"}	-- Create Auto-TreeFarm
	text[26] = {} -- Manage Auto-TreeFarm
	text[27] = {"1 chest", "saplings"} -- harvest and replant walled rectangle of natural forest
				
	-- FARMING			
	text[31] = {"64 cobble","128 dirt (optional)", "4 water buckets","4 chests","1 sapling (oak preferred"}-- Create modular crop farm
	text[32] = {"64 cobble","128 dirt (optional)", "4 water buckets","5 chests","1 sapling (oak preferred"}-- extend farm
	text[33] = {} -- Manual harvest and auto setup

	-- OBSIDIAN
	text[41] = {"cobble to cover area of obsidian"}	-- Harvest obsidian
	text[42] = {"10 obsidian", "8 cobble"} -- build Nether portal
	text[43] = {}-- demolish Nether portal
	text[44] = {"84 cobble or cobbled_deepslate"}-- undermine dragon towers
	text[45] = {} -- deactivate dragon tower
	text[46] = {} -- attack chamber
	text[47] = {} -- attack dragon
	text[48] = {"64 stone","1 rail","1 powered rail","1 minecart","1 button","ladders"}-- build end portal minecart
	text[49] = {"256 stone, 145 ladders, 1 obsidian, 1 water bucket"} -- dragon water trap
				
	--CANALS BRIDGES WALKWAYS
	text[51] = {"dirt or cobble (optional)","torch (optional)"} -- single path
	text[52] = {"dirt or cobble (optional)","torch (optional)"} -- 2 block coridoor
	text[53] = {"2 x length dirt or cobble (optional)" } -- Bridge over void/water/lava
	text[54] = {"2 x length dirt or cobble" } -- Covered walkway
	text[55] = {"256 cobble or dirt","2 water buckets","length/8 torches (optional)"} -- left side of new/existing canal
	text[56] = {"Packed ice or blue ice"} 											-- Ice canal
	text[57] = {"width * length stone"} 							-- platform
	text[58] = {"width + 1 * length + 1 stone"} 					-- sinking platform
	text[59] = {"2 TURTLES!","2 x soul sand, 2 water buckets"} 		-- Boat bubble lift
	text[510]= {"wooden trapdoors"}
	
	-- MOB FARM
	text[61] = {"256 cobble","1 slab"} -- 9x9 cube round spawner
	text[62] = {"2 water buckets","2 fence","2 signs","1 slab","1 soul sand (or dirt as placeholder)"} -- flood spawner chamber	
	text[63] = {"128 cobble","2 water buckets","1 soul sand"} 	
	text[64] = {"128 cobble"} 
	text[65] = {"640 cobble","1 slab (blaze 37)","blaze 8 powered rail","blaze 64 rail","blaze 2 redstone torch","blaze 1 hopper minecart","blaze 1 stone button"} -- 9x9 cube round spawner
	text[66] = {"1856 cobble, diorite etc (inc polished)","1 chest","10 empty buckets","2 water buckets","192 fence","8 signs","3 ladder","2 soul sand"} -- build endermen observation tower
				
	-- AREA CARVING
	text[71] = {"64 dirt"} -- Clear field
	text[72] = {} -- Clear rectangle width, length
	text[73] = {} -- Clear wall height, length
	text[74] = {} -- Clear rectangle perimeter only width, length
	text[75] = {} -- Clear structure floor/walls/ceiling
	text[76] = {}
	text[77] = {} -- Dig a trench
	text[78] = {} -- carve mountain
	text[79] = {"Any material suitable for floor or ceiling"} -- floor or ceiling
				
	--LAVA WATER
	text[81] = {"1024 cobblestone or dirt"} -- build wall from water or lava surface downwards
	text[82] = {"1024 sand"} -- drop sand into water or lava surface until solid grond reached
	text[83] = {"768 sand"} -- clear rectangle on top of building and fill with sand
	text[84] = {} -- clear sand wall or harvest sand
	text[85] = {} -- remove sand from cube. start at top
	text[86] = {} -- remove floor, walls (and sand) from building. start at base
	text[87] = {"up to 128 dirt or cobble"} -- Clear all blocks top of water
	text[88] = {} -- clear monument layer
	text[89] = {"ladder to height","cobble, dirt netherrack 3 X height"} -- Ladder to water/lava
	text[810] = {} -- Clear seaweed from enclosed area
				
	-- RAILWAY
	text[91] = {"1 block of choice","1 redstone torch"} -- place redstone torch under current block
	text[92] = {"1 block of choice","1 redstone torch"} -- place redstone torch on upward slope
	text[93] = {"height x block of choice","height/3 x redstone torch"} -- build downward slope
	text[94] = {"height x block of choice","height/3 x redstone torch"} -- build upward slope
	return text
end

local function getTaskHelp(menuLevel, menuItem)
	-- display help about selected task
	-- terminal size = 39 x 13
	                                   --Size
	info = {}
	info.main = {}
	info.sub = {}
	table.insert(info.main,
	[[
MINING:
Can be used in over-world or nether.   
Create a pre-formatted 33 x 33 blocks  
mine at chosen level.                 
Ladders and stairs up/down    
Bubble lift and safe drop to water.     
Strip resources from abandoned mines.  
Faster version of 33x33 mine pattern   
using corridor and rectangle functions.
Mine bottom layer to bedrock (not worth
the fuel and time)
	]])
	table.insert(info.main,
	[[
FORESTRY:
Fell Tree can be used in Nether as well
for cutting any size tree / branches
Create a simple 4x4 or 8x8 tree farm
suitable for turtle harvesting.
Automatic treefarm can be created and
managed with dedicated automatic turtle.
Natural forest can be harvested and
replanted. (Must be walled off)
	]])
	table.insert(info.main,
	[[
FARMING:
Farm modules can be built to fixed size
and placed next to each other in linear
or rectangular pattern.
Whole farm is managed by a dedicated
turtle, which must be equipped with a
diamond hoe as well as pickaxe.
Fuel obtained from an oak tree placed
in the corner of each module.
Double chests store produce and seeds

	]])
	table.insert(info.main,
	[[
OBSIDIAN:
The turtle can extract obsidian from
lava areas safely.
Nether portals can be built or removed
without needing diamond pickaxes.
End World dragon towers can be
undermined ready for deactivating.
End world towers can have the crystals
destroyed.
The dragon can be attacked from below.
Minecart end portal stations built.
	]])
	table.insert(info.main,
	[[
PATHS, BRIDGES, CANALS:
Can be used in Nether and End.
Build pathways over air, water or lava
Optional roofed pathway for Nether use.
Tunnel through rock and place a floor
at the same time.
Build a canal with towpath, must use
two turtles operating simultaneously
Platform for use over air, water, lava
Sinking version is removed and replaced
1 block lower each time
	]])
	table.insert(info.main,
	[[
MOB FARMS:
Tools to create mob farms round
existing spawners. Special version
for Blaze farms uses rail collection
Choice of bubble lift mob dropper
or simple deep trench.
Enderman observation tower can be
built >128 above ground: is expensive.
Theoretically re-spawns new mobs when
used.
Suggest build only the base.
	]])
	table.insert(info.main,
	[[
AREA CLEARING AND REFORMING:
Tools to clear a field inc. trees,
rectangles, single walls, solid and
hollow structures.
Dig a trench, carve away side of a
mountain.
Place or replace floors and ceilings




	]])
	table.insert(info.main,
	[[
WATER AND LAVA TOOLS:
Used to drain ocean monuments and
shipwrecks. Can also be used to make
underwater base. Water is cleared using
sand dropping and recycling from 1.12.+
Alternative method uses solid block
placing and recycling.
Water plants can be removed without
damaging the structure.


	]])
	table.insert(info.main,
	[[
RAILWAY TOOLS:
Used to build diagonal block risers
and fallers for placing 45 deg rail 
tracks.
Placing Redstone torches under powered
rails when above ground level (viaduct)





	]])
	if bedrock == 0 then	--pre 1.18
		info.sub[11] = 
[[Press F3 to check level. Look for 'Y'
Place at level 5, 8, 11 (11 nether)
]]-- Create mine at this level
	else
		info.sub[11] = 
[[Press F3 to check level. Look for 'Y'
Place at level -59, -56, -53 (11 nether)
]]-- Create mine at this level
	end
	info.sub[12] = 
[[Place me on the ground.
The ladder will start at this level
and go up or down on the space in front
of me.
If it reaches bedrock, a lava-proof
chamber will be built
]]-- Ladder up/down
	info.sub[13] = 
[[Place me on the ground. If stairs are
going down, the turtle will drop to
chosen level, then build upwards.
Stairs go to chosen level in a 5x5 block
]] -- Stairs up/down
	info.sub[14] = 
[[Place me on the ground. I will go down
to chosen level enclosing all sides of a
column. A water source will be placed at
the bottom. I will return here.
]] -- safe drop
	info.sub[15] = 
[[Place me on the ground. I will build a
3 x 1 water source and a single column
bubble lift to the chosen height.
]] -- single col bubble lift
	info.sub[16] = 
[[Place me on the end wall of a disused
mine in the centre block, 1 block above
the floor. Provide a diamond sword for
harvesting string from spider webs
]] -- salvage mineshaft
	info.sub[17] = 
[[Place me at eye height
Upper left corner of a rectangle
(1 block above the floor)
]] -- quick coridoor system
	info.sub[18] = info.sub[17] -- quick mine
	if bedrock == 0 then
		info.sub[19] = 
[[Place me level 5 on the floor to mine
into bedrock (slow and inefficient)
]] -- mine all blocks to bedrock
	else
		info.sub[19] = 
[[Place me level -59 on the floor to mine
into bedrock (slow and inefficient)
]] -- mine all blocks to bedrock
	end
	info.sub[21] = 
[[Place me in front of the tree
you want to fell. Fuel not required as
logs will be used if needed
]] -- Fell Tree
	info.sub[22] = 
[[Place me on grass, lower left corner
of an 11x11 OR 19x19 square.
Trees to be grown on alternate blocks
in a square of 4x4 or 8x8 trees, with
a 2 block wide perimeter.
]] -- Create treefarm
	info.sub[23] = 
[[Place me in front of first tree base
(or dirt) on the lower left corner
of a 4x4 trees OR 8x8 trees square.
Provide 2 types of saplings for a
mixed tree farm.
]] -- Plant treefarm
	info.sub[24] = 
[[Place me in front of first tree on
the lower left corner of a 4x4 trees
OR 8x8 trees square.
Fuel not required as logs will be used.
]] -- Harvest treefarm
	info.sub[25] = 
[[For a new Auto-TreeFarm:
Place me on left side of a 19 wide
x 14 long area
]] -- Create Auto-TreeFarm
	info.sub[26] = 
[[Place me in front of sapling
(or tree) with the chest behind me
]] -- Manage Auto-TreeFarm
	info.sub[27] = 
[[Place me in bottom left corner
of walled/ fenced rectangle of forest
]] -- clear and replant natural forest
	info.sub[31] = 
[[Place me on the ground lower left
of an area 14 x 14. A crop farm
12 x 12 with cobble wall will be
built forward and to the right
]] -- Create modular crop farm
	info.sub[32] = 
[[Place me next to the tree on
a chest, either left side of farm
or facing front wall to add a farm
in that direction.
]] -- Extend farm
	info.sub[33] = 
[[Place me over the water on the
left corner. There should be
chests on 2 sides. When the crop
I am facing ripens, harvest will
begin.
]] -- Manual harvest and auto setup
	info.sub[41] = 
[[Place me on any block
on the left side facing the
obsidian field.
]] -- Harvest obsidian
	info.sub[42] = 
[[Place me on the ground on the
left side of the portal base.
]] -- build Nether portal
	info.sub[43] = info.sub[42] -- Demolish Nether portal
	info.sub[44] = 
[[Place me on the ground at 0,y,0
(centre of the dragon arena)
facing West. IMPORTANT!
Double-check position / direction
]] -- Find dragon tower centres
	info.sub[45] = 
[[Place me in the ceiling pit in
centre of the obsidian tower
facing the endstone block.
This maximises success without
being destroyed!
]] -- deactivate dragon tower
	info.sub[46] = 
[[Place me under the bedrock of
the non-active escape portal in
the end World.
I will build a shield round the 
pre-portal and wait at the top
with attack() active.
REMOVE ME BEFORE DRAGON DIES!!
(Portal ignition destroys me)
]] -- build dragon attack chamber
	info.sub[47] = 
[[If not already there, place me
1 block down on the bedrock spire
where the dragon visits.
REMOVE ME BEFORE DRAGON DIES!!
(Portal ignition destroys me)
]] -- attack Dragon
	info.sub[48] = 
[[Place me on the ground under an
end world portal. (Centre marked by
beacon every couple of minutes)
I will build a ladder to a platform,
and minecart entry device.
]] -- build end portal minecart
	info.sub[49] = 
[[Place me on the ground at 0,49,100
on the end world spawn point.
(facing the dragon arena)
I will build a ladder column 145
blocks high, and place a water
source above the dragon perch
]] -- build dragon water trap
	info.sub[51] = 
[[Place me on the ground in front
of water, lava or air.
Follow and supply blocks as needed
]] -- Single path
	info.sub[52] = 
[[Place me on the ground at start
of coridoor.
]] -- 2 block coridoor
	info.sub[53] = 
[[Place me on the ground.
The double path will start in front,
continue for your chosen length
and return
]] -- Bridge over void/water/lava
	info.sub[54] = 
[[Place me on the ground.
The covered walkway will start in front,
(on the ceiling), continue for your
chosen length and return.
]] -- Covered walkway
	info.sub[55] = 
[[I should be on either an existing canal
or ready to start a new one.
If crossing water I should be on top
of a solid block making up the canal wall.
]] -- new/existing canal
	info.sub[56] = 
[[I should be on the left side of either
an existing canal 1 block above water or
a new ice canal.
I will replace the block below me with
packed or blue ice, place ice on alternate
blocks and remove blocks between.
The right side should be air or water.
Minimum wall height is trapdoor or taller
]] -- Place packed ice in existing canal
	info.sub[57] = 
[[Place me any level in air, water or lava.
The platform will start on the left and
build forward and to the right.
]] -- Platform
	info.sub[58] = 
[[Place me on bottom left corner of an
existing platform. Enter same width and
length as existing
]] -- Sinking platform
	info.sub[59] = 
[[Place me in an existing canal or at the
back of 2x2 water source. MUST be source
in front.
Lift extension: place me in air in front
of exit signs, facing back of existing,
water source below.

ESSENTIAL to have a second turtle running
this script simultaneously.
Turtles must be FACING each other enabling
simultaneous start.
]] -- boat bubble lift
	info.sub[510] = 
[[place me next to the wall, above the
canal towpath, at eye level.
I will place trapdoors below, so they
can be raised to create a wall for boats
to travel on the ice blocks.
]] -- ice canal trapdoors
	info.sub[61] = 
[[Place me in contact with spawner
above/below/facing spawner block
to create a 9 x 9 hollow cube.
For Blaze spawners use that option.
]] -- 9x9 cube round spawner
	info.sub[62] = 
[[Place me on the floor facing the wall
where the water sources will start
				
If you do not have soul sand, add dirt
as a temporary place marker.
Make sure you have an escape route!
]] -- Flood spawner chamber
	info.sub[63] = 
[[Place me on the soul sand/dirt block
at the flooded base of the spawner,
facing any direction.
]] -- Build bubble tower kill zone
	info.sub[64] = 
[[Place me at start of trench facing
required direction.
]] -- Dig kill trench for mobs
	info.sub[65] = 
[[Place me in contact with Blaze spawner
above or facing spawner block to create
a safe killzone out of site of Blazes.
]] -- 9x9 cube round spawner with minecart collection
	info.sub[66] = 
[[This is a 3 stage process:
1.New tower lower base: place me on
flat ground in open plain.

2.Upper base: place me in front of
the existing chest at tower base.

3.Main tower, only needed to respawn
mobs. Expensive, not recommended.
Place me in front of chest in ground.
]] -- Build endermen observation tower
	info.sub[71] = 
[[Place me on grass, lower left corner
of the area to be levelled and cleared.
Provide dirt to plug voids in the floor
]] -- Clear field
	info.sub[72] = 
[[Place me inside the left corner of the
rectangle to be cleared at the level to
be worked on.
This corner is included in the site.
]] -- Clear rectangle width, length
	info.sub[73] = 
[[Place me inside top or bottom corner
of the wall to be cleared.
]] -- Clear wall height, length
	info.sub[74] = 
[[Place me inside top or bottom left
corner of the rectangular wall to be
cleared at the level to be worked on.
This corner is included in the site.
]] -- Clear rectangle perimeter only width, length
	info.sub[75] = 
[[Place me INSIDE top or bottom left
corner in the floor or ceiling.
This corner is included in the site.
]] -- Clear structure floor/walls/ceiling
	info.sub[76] = info.sub[75]
	info.sub[77] = 
[[Place me on the ground facing the
trench direction.
]] -- Dig a trench
	info.sub[78] = 
[[Place me on the ground facing the
mountain side.
]] -- Carve mountain
	info.sub[79] = 
[[Place me inside top or bottom left
corner of existing floor or ceiling.

Next instruction checks add or replace.
]] -- (Re)place floor or ceiling
	info.sub[81] = 
[[Place me on the surface facing wall END
(Any block below will be removed)
Wall will be built DOWN and BACKWARDS
]] -- build wall from water or lava surface downwards 	
	info.sub[82] = 
[[Place me on the surface of water or lava
Sand  will be dropped DOWN and Forwards
]] -- drop sand into water or lava surface until solid ground reached
	info.sub[83] = 
[[Place me on top left corner of roof
Sand  will be dropped into the hollow
]] -- clear rectangle on top of building and fill with sand	
	info.sub[84] = 
[[Place me on the surface of sand.
Sand will be mined DOWN then forwards.
Can be used for deserts as well.
]] -- clear sand wall or harvest sand	
	info.sub[85] = 
[[Place me on top left corner of sand
Sand cleared down, then left to right.
Can be used for deserts as well.
]] -- Remove sand from cube. start at top
	info.sub[86] = 
[[Place me on lower left corner of floor
Floor and walls removed and sand cleared
]] -- Remove floor, walls (and sand) from building. start at base	
	info.sub[87] = 
[[Place me on the left corner of the top
of retaining wall facing water or lava.
]] -- Clear all blocks top of water
	info.sub[88] = 
[[Place me on lower left corner of area
on wall or over air. I will descend to
top wall of monument.
]] -- Clear monument layer	
	info.sub[89] = 
[[Place me on the ground.
The ladder will start here and drop to
water or lava below
]] -- Ladder to water/lava	
	info.sub[810] = 
[[Place me on the left corner of the top
of retaining wall facing water
]] -- Clear seaweed from enclosed area
	info.sub[91] = 
[[Place me on suspended railway stone
Redstone torch will go below me
]] -- Place redstone torch under block
	info.sub[92] = 
[[Place me on railway stone going up
Redstone torch will go below me
]] -- place redstone torch on upward slope 	
	info.sub[93] = 
[[Place me on last stone.
Track will go down from this point
]] -- build downward slope
	info.sub[94] = 
[[Place me on last stone.
Track will go up from this point
]] -- build upward slope
	if menuLevel == 1 then -- general help
		print(info.main[menuItem])
		term.write("Enter to continue_")
		return read()
	else -- item specific help
		print(info.sub[menuItem])
		term.setCursorPos(1, 13)
		term.write("Enter to continue. + any key to quit: ")
		return read()
	end
end

local function getTaskOptions()
	local options = {}
	options.main =
	{
		"Mining (includes Nether)",
		"Forestry",
		"Farming",
		"Obsidian, Nether & End Portal",
		"Canal, bridge and walkway",
		"Mob farm tools",
		"Area shaping and clearing",
		"Lava and Water",
		"Railway"
	}
	table.insert(options,
	{
		"Create mine at this level",
		"Ladder up or down",
		"Stairs up or down",
		"Safe drop to water block",
		"Single column bubble lift",
		"Rob disused mineshaft",
		"QuickMine coridoor system",
		"QuickMine rectangle",
		"Mine bedrock level"
	})
	table.insert(options,
	{
		"Fell Tree",
		"Create tree farm",
		"Plant tree farm",
		"Harvest tree farm",
		"Create Auto-treeFarm",
		"Manage Auto-treeFarm",
		"Harvest and replant forest"
	})
	table.insert(options,
	{
		"Create modular crop farm",
		"Extend modular crop farm",
		"Manage modular crop farm"
	})	
	table.insert(options,					
	{
		"Dig obsidian field",
		"Build Nether Portal",
		"Demolish Nether Portal",
		"Undermine Dragon Towers",
		"Deactivate Dragon Tower",
		"Build Dragon attack area",
		"Attack Dragon",
		"Build portal minecart station",
		"Build dragon water trap"
	})
	table.insert(options,
	{
		"Continuous path",
		"2 block high tunnel",
		"2 block wide over air/water/lava",
		"Covered walkway",
		"Water canal",
		"Ice canal (left side)",
		"Platform",
		"Sinking platform for oceans",
		"Boat bubble lift",
		"Ice canal trapdoor border"
	})
	table.insert(options,
	{
		"Create 9x9 cube around spawner",
		"Flood mob farm floor",
		"Create mob bubble lift",
		"Dig mob drop trench",
		"Create Blaze farm around spawner",
		"Build Endermen observation tower"
	})
	table.insert(options,
	{
		"Clear field (inc trees)",
		"Clear a rectangle",
		"Clear single wall up/down",
		"Clear rectangular wall section",
		"Clear hollow structure up/down",
		"Clear solid structure up/down",
		"Dig a trench",
		"Carve mountain side",
		"Place a floor or ceiling"
	})
	table.insert(options,
	{
		"Vertical wall from surface",
		"Drop sand or gravel wall",
		"Decapitate and fill with sand",
		"Clear sand wall",
		"Clear sand filled building",
		"Demolish sand filled structure",
		"Clear top layer of water",
		"Clear monument layer",
		"Ladder down to water/lava",
		"Clear water plants"
	})
	table.insert(options,
	{
		"Place Redstone:torch level track",
		"Place Redstone:torch upward track",
		"Build downward track",
		"Build upward track"
	})
	
	return options
end

local function getTask()	
	local choice = nil
	local mainChoice = nil
	local modifier = ""
	local size = 0
	local width = 0
	local length = 0
	local height = 0
	local prompt = "Choose a task (add h for help eg 3h):"
	local options = getTaskOptions()
	local itemsRequired = getTaskItemsList()
	
	while choice == nil do
		mainChoice, modifier = menu.new(prompt, options.main) -- open main menu options
		if mainChoice == nil and modifier == "q" then
			return nil-- quit task system so it re-starts (in case help menu chosen)
		else
			if modifier == "h" then -- get help on main menu items
				getTaskHelp(1, mainChoice)
			else
				choice, modifier = menu.new(prompt, options[mainChoice]) -- open submenu options
			end
		end
		if choice ~= nil then -- no help options or quit. Go for task
			if choice > 9 then
				choice = choice + (mainChoice * 100) -- 11 = 11 + 1100 = 1111
			else
				choice = choice + (mainChoice * 10) -- 9 = 9 + 9 * 10 = 99
			end
		end
	end
	T:clear()
	-- choice will be an integer, modifier will be empty string or "h"
	if modifier == "h" then -- get help on sub menu items
		listOfItems = itemsRequired[choice]
		if next(listOfItems) ~= nil then
			print("Items required:")
			for k,v in pairs(listOfItems) do
				print("  "..v)
			end
		end
		getTaskHelp(2, choice)
		local input = read()
		T:clear()
		if input ~= "" then
			return nil
		end
	end
	
	-- MINING
	
	if choice == 14 then	-- safe drop to water
		local currentLevel = getSize(true,"Current level (F3->Y coord)?_", bedrock + 5, ceiling)
		local destLevel = getSize(true, "Go down to level? ("..currentLevel - 2 .." to "..bedrock + 5 ..")", bedrock + 5 , currentLevel - 2)
		height = math.abs(destLevel - currentLevel)
	elseif choice == 15 then	-- bubble lift
		local currentLevel = getSize(true,"Current level (F3->Y coord)?_", bedrock + 5, ceiling)
		local destLevel = getSize(true, "Go up to level? ("..currentLevel + 2 .." to "..ceiling ..")", currentLevel + 2, ceiling)
		height = math.abs(destLevel - currentLevel)
	elseif choice == 17 then -- create square 17 x 17 mining coridoor
		width = getSize(false, "Width (2-64 default 17)\n", 2, 64, 17)
		length  = getSize(false, "Length (2-64 default 17)\n", 2, 64, 17)
	elseif choice == 18 then -- clear a rectangle, fill empty spaces above
		width = getSize(false, "Width (2-64 default 15)\n", 2, 64, 15)
		length  = getSize(false, "Length (2-64 default 15)\n", 2, 64, 15)
	elseif choice == 19 then -- Mine bedrock area
		width = getSize(false, "Width (2-64 default 15)\n", 2, 64, 15)
		length = getSize(false, "Length (2-64 default 15)\n", 2, 64, 15)
		height = false
		height = getBoolean("Leave bedrock exposed? (y / n)")
		
	-- FORESTRY
	elseif choice == 22 then --Create treefarm
		options = 	{"4 x 4 trees(16)","8 x 8 trees(64)"}
		size = menu.new(prompt, options)
	elseif choice == 23 then -- plant treefarm
		options = {"4 x 4 trees(16)","8 x 8 trees(64)"}
		size = menu.new(prompt, options)
	elseif choice == 24 then -- Harvest treefarm
		options = {"4 x 4 trees(16)","8 x 8 trees(64)"}
		size = menu.new(prompt, options)
	elseif choice == 27 then -- Harvest and replant natural forest
		-- no instructions
	-- FARMING
		
	-- OBSIDIAN
	elseif choice == 41 then	-- Harvest obsidian
		width = getSize(false, "Width of the area (1-64)\n", 1, 64)
		length  = getSize(false, "Length of the area (1-64)\n", 1, 64)
	elseif choice == 42 then -- build Nether portal
		width = getSize(false, "Width of the portal\n", 1, 64, 4)
		height = getSize(false, "Height of the portal\n", 1, 64, 5)
	elseif choice == 43 then -- demolish Nether portal
		width = getSize(false, "Width of the portal\n", 1, 64, 4)
		height = getSize(false, "Height of the portal\n", 1, 64, 5)
	elseif choice == 44 then -- undermine dragon towers
		-- no instructions
	elseif choice == 45 then -- deactivate dragon tower
		-- no instructions
	elseif choice == 46 then -- build dragon attack area
		-- no instructions
	elseif choice == 47 then -- attack dragon
		-- no instructions
	elseif choice == 48 then -- build end portal minecart station
		-- no instructions
	elseif choice == 49 then -- build dragon water trap
		-- no instructions
		
	-- CANAL BRIDGE
	elseif choice == 51 then	--single path
		length = getSize(false, "Path length? 0 = continuous\n", 0, 1024, 64)
	elseif choice == 52 then	--2 block coridoor
		length = getSize(false, "Corridor length? 0 = continuous\n", 0, 1024, 64)
	elseif choice == 53 then	--return Path over void/water/lava
		size = getSize(false, "Length of the area (1-256)\n", 1, 256, 64)
	elseif choice == 54 then	--Covered walkway
		size = getSize(false, "Length of the walk (1-256)\n", 1, 256, 64)
	elseif choice == 55 then	--left/right side of new/existing canal
		size = getSize(false, "Am I on the left(0) or right(1)?\n", 0, 1)
		length = getSize(false, "Canal length? 0 = continuous\n", 0, 1024, 64)
		height = getSize(false, "Am I on the floor(0) or wall(1)?\n", 0, 1)
	elseif choice == 56 then	--place ice on alternate blocks
		length = getSize(false, "Canal length? 0 = continuous\n", 0, 1024, 64)
	elseif choice == 57 then	--platform
		width = getSize(false, "Platform width?\n", 1, 1024)
		length = getSize(false, "Platform length?\n", 1, 1024 / width)
	elseif choice == 58 then	--sinking platform
		width = getSize(false, "Width (excluding retaining wall)?\n", 1, 1024)
		length = getSize(false, "Length (excluding retaining wall)?\n", 1, 1024 / width)
		height = getSize(false, "Levels to go down?\n", 1, 1024 / width * length)
	elseif choice == 59 then	--boat bubble lift
		size = getSize(false, "New lift(0) or extend existing(1)?\n", 0, 1, 0) -- default new lift
		length = getSize(false, "Am I on the left(0) or right(1)?\n", 0, 1)
		height = getSize(false, "Levels to go up?\n", 4, ceiling - 2)
	elseif choice == 510 then	--ice canal trapdoors
		size = getSize(false, "Am I on the left(0) or right(1)?\n", 0, 1)
		length = getSize(false, "Canal length? \n", 1, 1024, 64)
	-- MOB SPAWNER TOOLS
	elseif choice == 63 then -- create bubble lift at mob spawner
		size = getSize(false, {"Standing inside the cube:",
							  "Killzone on left(0)?",
							  "Killzone on right(1)?",
							  "",
							  "Standing outside the cube:",
							  "Killzone on left(1)?",
							  "Killzone on right(0)?",
							  "",
							  "Type your choice 0 or 1_"}, 0, 1)
	elseif choice == 64 then -- Mob trench
		length = getSize(false, "Length of trench (1-256)\n", 1, 256)
		height = getSize(false, "Depth of trench (1-50)\n", 1, 50)
		
	-- AREA CARVING
	elseif choice == 71 then --Clear field
		width = getSize(false, "Width of the area (1-64)\n", 1, 64)
		length  = getSize(false, "Length of the area (1-64)\n", 1, 64)
	elseif choice == 72 then -- Clear solid rectangle width, length
		width = getSize(false, "Width of the area (1-256)\n", 1, 256)
		length  = getSize(false, "Length of the area (1-256)\n", 1, 256)
	elseif choice == 73 then -- Clear wall height, length, direction
		size = getSize(false, "Bottom->Top (0), Top->bottom(1)",0, 1)
		width = 1
		length = getSize(false, "Length of wall (1-256)\n", 1, 256)
		height = getSize(false, "Height of wall (1-50)\n", 1, 50)
	elseif choice == 74 then -- Clear rectangle perimeter only width, length
		width = getSize(false, "Width of walled area (1-256)\n", 1, 256)
		length  = getSize(false, "Length of walled area (1-256)\n", 1, 256)
		height = 1
	elseif choice == 75 then -- Clear sructure floor/walls/ceiling
		size = getSize(false, "Bottom->Top (0), Top->bottom(1)",0, 1)
		width = getSize(false, "Hollow structure width (1-256)", 1, 256)
		length  = getSize(false, "Hollow structure length (1-256)", 1, 256)
		if size == 0 then
			height  = getSize(false, "Height (1-256)", 1, 256)
		else
			height  = getSize(false, "Depth/Height (1-256)", 1, 256)
		end
	elseif choice == 76 then -- clear solid structure 
		size = getSize(false, "Bottom->Top (0), Top->bottom(1)",0, 1)
		width = getSize(false, "Solid structure width (1-60)", 1, 60)
		length  = getSize(false, "Solid structure length (1-60)", 1, 60)
		if size == 0 then
			height  = getSize(false, "Height (1-256)", 1, 256)
		else
			height  = getSize(false, "Depth/Height (1-256)", 1, 256)
		end
	elseif choice == 77 then	-- Dig a trench
		height = getSize(false, "Depth of the trench (1-64)", 1, 64)
		length = getSize(false, "Trench length? 0 = continuous\n", 0, 1024)
	elseif choice == 78 then	-- Carve side of mountain
		size = getSize(false, "Left <- Right (0), Left -> Right(1)",0, 1)
		width = getSize(false, "Width of area to remove?", 1, 1024)
		length = getSize(false, "Length of area to remove?", 0, 1024)
	elseif choice == 79 then	-- Place a floor or ceiling
		width = getSize(false, "Width of the floor/ceiling (1-64)\n", 1, 64)
		length  = getSize(false, "Length of the floor/ceiling (1-64)\n", 1, 64)
	-- WATER LAVA
	elseif choice == 81 then -- build wall from water or lava surface downwards
		width = 1
		length = getSize(false, "Length of the wall (1-60)\n", 1, 60)
		height = getSize(false, "Estimated depth (1-60) 0=default\n", 0, 60)
	elseif choice == 82 then -- drop sand into water or lava surface until solid grond reached
		width = 1
		length = getSize(false, "Length of dam (0=to solid block)\n", 0, 60)
	elseif choice == 83 then -- clear rectangle on top of building and fill with sand
		width = getSize(false, "Width of roof (<=30)\n", 1, 30)
		length = getSize(false, "Length of of roof (<=30)\n", 1, 30)
	elseif choice == 84 then -- clear sand wall or harvest sand
		width = 1
		length = getSize(false, "Length of sand \n", 1, 250)
	elseif choice == 85 then -- remove sand from cube. start at top
		width = getSize(false, "Width of sand (<=30)\n", 1, 30)
		length = getSize(false, "Length of of sand (<=30)\n", 1, 30)
	elseif choice == 86 then -- remove floor, walls (and sand) from building. start at base
		width = getSize(false, "Width of floor (<=30)\n", 1, 30)
		length = getSize(false, "Length of of floor (<=30)\n", 1, 30)
	elseif choice == 87 then -- Clear all blocks top of water
		size  = lib.waterWarning()
	elseif choice == 88 then -- clear monument layer
		width = getSize(false, "Width of monument area (1 to 64)\n", 1, 64)
		length = getSize(false, "Length of monument area (1 to 64)", 1, 64)
		size = getSize(false, "Clear above and below? 0:no 1:yes", 0, 1)
	elseif choice == 89 then -- Ladder to water/lava	
		height = getSize(false, "Est. height above (?F3)\n", 1, 256)
	elseif choice == 810 then -- Clear seaweed from enclosed area
		width = getSize(false, "water width (1-64)\n", 1, 64)
		length  = getSize(false, "water length (1-64)\n", 1, 64)
		
	-- RAILWAY
	elseif choice == 93 then -- build downward slope
		height  = getSize(false, "How many blocks down (0=auto)?\n", 0, 256)
	elseif choice == 94 then -- build upward slope
		height  = getSize(false, "Go up by how many blocks?\n", 1, 256)
	end
	
	return choice, size, width, length, height -- eg 86, 0, 8, 8, 4
end

local function getTaskInventory(choice, size, width, length, height)
	-- run this loop 2x per second to check if player has put anything in the inventory
	-- fuel 1 coal = 60 = 4 planks. 64 planks = 16 coal = 960 units
	local retValue = ""
	local gotBirchSaplings = 0
	local gotCoal = 0
	local gotCobble = 0
	local gotDirt = 0
	local gotOakSaplings = 0
	local gotPlanks = 0
	local gotTorches = 0
	local thanks = "Thank you.\n\nPress the ESC key\n\nStand Back..\n"
	
	T:clear()
	if choice == 0 then --Missing pickaxe
		T:checkInventoryForItem({"minecraft:diamond_pickaxe"}, {1})
		print("Diamond Pickaxe being tested...")
		T:setEquipment()
	elseif choice == 1 then --Missing crafting table
		T:checkInventoryForItem({"minecraft:crafting_table"}, {1}) -- 0 if not present
		print("Crafting table being tested...")
		T:setEquipment()
	elseif choice == 2 then --Missing chest
		retValue = T:checkInventoryForItem({"minecraft:chest"}, {1}) -- 0 if not present
		sleep(1.5)
		
	-- MINING
	elseif choice == 11 then --Create Mine at this level
		checkFuelNeeded(960)
		T:checkInventoryForItem({"minecraft:torch"}, {24}, false)
		T:checkInventoryForItem({"minecraft:bucket"}, {1}, false)
		T:checkInventoryForItem({"stone"}, {64})
		T:checkInventoryForItem({"minecraft:chest"}, {1})
		sleep(2)
		print("CreateMine starting")
		createMine()
	elseif choice == 12 or choice == 13 then	-- ladder down / up / stairs down / stairs up
		local currentLevel = getSize(true,"Current level (F3->Y coord)?_", bedrock + 5, ceiling)
		local destLevel = bedrock							-- default destination
		local destName = "bedrock"							-- default name
		local description = "Creating ladder going down"	-- default action description
		size = getSize(true,"Going down(0) or up(1)?", 0, 1)
		if size == 0 then-- going down
			destLevel = getSize(true, "Go down to level? ("..currentLevel - 2 .." to "..bedrock + 5 ..")", bedrock + 5 , currentLevel - 2)
			if choice == 13 then
				description = "Creating stairs going down"
			end
		else -- going up
			destLevel = getSize(true, "Go up to level? ("..currentLevel + 2 .." to "..ceiling ..")", currentLevel + 2, ceiling)
			destName = "surface"
			if choice == 12 then
				description = "Creating ladder going up"
			else
				description = "Creating stairs going up"
			end
		end
		local inAir = getBoolean("Are you in air or nether? (y/n)")
		--[[
		range examples
		-50 to -59 = -59 -(-50) = -9   down
		-59 to -50 = -50 -(-59) = 9    up
		 70 to -48 = -48 -   70 = -118 down
		  5 to  64 =  64 -   5  = 59   up
		]]
		local range = math.abs(destLevel - currentLevel)
		T:checkInventoryForItem({"minecraft:bucket"}, {1}, false)
		if choice == 12 then -- ladders
			checkFuelNeeded(range * 2)
			T:checkInventoryForItem({"minecraft:ladder"}, {range})
			T:checkInventoryForItem({"minecraft:torch"}, {math.floor(range / 4)}, false)
			if inAir then
				range = range * 3 -- more blocks needed
			end
			T:checkInventoryForItem({"stone"}, {range})
		else -- stairs
			checkFuelNeeded(range * 10) -- stairs: each layer needs 10 moves
			local numStairsNeeded = range
			local data = T:getStock("stairs")
			local numStairs = data.total
			local cobbleNeeded = math.min(range * 6, 256)
			if inAir then
				cobbleNeeded = range * 6 -- 5 blocks / layer unless in water or lava
			end
			if numStairs < numStairsNeeded then
				cobbleNeeded = cobbleNeeded + (math.floor((2 * (numStairsNeeded - numStairs)) / 3))
			end
			T:checkInventoryForItem({"stairs"}, {numStairsNeeded}, false)
			T:checkInventoryForItem({"minecraft:cobblestone","minecraft:cobbled_deepslate"}, {cobbleNeeded, cobbleNeeded})
			T:checkInventoryForItem({"minecraft:chest"}, {1}) 	-- needed for crafting
		end
		print(thanks)
		print(description)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		if choice == 12 then
			createLadder(destName, currentLevel, destLevel) -- "bedrock", 70, -48
		else
			createStaircase(destName, currentLevel, destLevel)
		end
	elseif choice == 14 then	-- safe drop to water
		checkFuelNeeded(height * 2)
		T:checkInventoryForItem({"minecraft:water_bucket"}, {1})
		T:checkInventoryForItem({"stone"}, {height * 2}, false) -- estimate only partial cloaking needed
		print(thanks)
		print("Creating safe drop ".. height.. " blocks deep")
		print("Wait for my return!")
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createSafeDrop(height)
	elseif choice == 15 then	-- single column bubble lift
		checkFuelNeeded(height * 6)
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"minecraft:bucket"}, {14}, false, "More buckets for speed!")
		T:checkInventoryForItem({"minecraft:soul_sand"}, {1})
		T:checkInventoryForItem({"sign"}, {2})
		T:checkInventoryForItem({"stone"}, {height * 2}, false) -- estimate only partial cloaking needed
		print(thanks)
		print("Creating bubble lift ".. height.. " blocks high")
		print("Wait for my return!")
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createBubbleLift(height)
	elseif choice == 16 then	-- salvage mineshaft
		equippedRight, equippedLeft, inInventory = T:setEquipment() -- check for crafting table, sword, pickaxe, Put sword in spare slot
		if equippedLeft ~= "minecraft:diamond_pickaxe" and equippedRight ~= "minecraft:diamond_pickaxe" then
			T:checkInventoryForItem({"minecraft:diamond_pickaxe"}, {1}, true)
			equippedRight, equippedLeft, inInventory = T:setEquipment() -- check for crafting table, sword, pickaxe, Put sword in spare slot
		end
		if inInventory ~= "minecraft:diamond_sword" then
			returned = T:checkInventoryForItem({"minecraft:diamond_sword"}, {1}, false, "To harvest spider webs\n you need a diamond sword.") --checkInventoryForItem(self, items, quantities, required, message, name)
			if returned ~= nil then
				inInventory = "minecraft:diamond_sword"
			end
		end
		T:checkInventoryForItem({"minecraft:torch"}, {8}, false)
		if inInventory == "minecraft:diamond_sword" then
			print("Clearing Mineshaft and cobwebs")
		else
			print("Clearing Mineshaft")
		end
		clearMineshaft(equippedRight, equippedLeft, inInventory) -- pass whether the sword is present
	elseif choice == 17 then --QuickMine coridoor
		checkFuelNeeded(width * length)
		T:checkInventoryForItem({"stone"}, {64})
		T:checkInventoryForItem({"minecraft:bucket"}, {1}, false)
		print("QuickMine coridoor: size "..width.. " x "..length)
		quickMineCorridor(width, length)
	elseif choice == 18 then --QuickMine
		checkFuelNeeded(width * length)
		T:checkInventoryForItem({"stone"}, {64})
		T:checkInventoryForItem({"minecraft:bucket"}, {1}, false)
		print("QuickMine rectangle: size "..width.. " x "..length)
		quickMine(width, length)
	elseif choice == 19 then --Mine bedrock area
		checkFuelNeeded(width * length)
		T:checkInventoryForItem({"stone"}, {64})
		T:checkInventoryForItem({"minecraft:bucket"}, {1}, false)
		mineBedrockArea(width, length, height) -- height = bool leaveExposed
		
	-- FORESTRY
	elseif choice == 21 then	-- Fell tree
		if T:isLog("forward") then
			T:checkInventoryForItem({"minecraft:chest"}, {1}, false)
			T:forward(1)
			T:harvestWholeTree("up")
			print("Press esc within 2 seconds!")
			os.sleep(2)    -- pause for 2 secs to allow time to press esc
			print("Felling tree")
		else
			print("No log in front..")
			print("Move me in front of a tree!")
			os.sleep(2)    -- pause for 2 secs to allow time to press esc
			retValue = ""
		end
	elseif choice == 22 then --Create treefarm
		if size == 1 then
			checkFuelNeeded(300)
		else
			checkFuelNeeded(900)
		end
		T:checkInventoryForItem({"minecraft:dirt"}, {64})
		if size == 1 then
			T:checkInventoryForItem({"minecraft:torch"}, {16}, false)
		else
			T:checkInventoryForItem({"minecraft:torch"}, {64}, false)
		end
		print(thanks)
		sleep(2)
		print("CreateTreefarm starting: size "..size)
		createTreefarm(size)
	elseif choice == 23 then -- Plant treefarm
		if size == 1 then
			checkFuelNeeded(180)
		else
			checkFuelNeeded(480)
		end
		T:checkInventoryForItem({"sapling"}, {4})
		print(thanks)
		print("plantTreefarm starting: size "..size)
		plantTreefarm(size)
	elseif choice == 24 then	-- Harvest treefarm
		print(thanks)
		os.sleep(2)
		print("Harvesting treefarm starting: size "..size)
		harvestTreeFarm(size)
	elseif choice == 25 then -- create auto tree farm
		checkFuelNeeded(1000)
		T:checkInventoryForItem({"minecraft:chest"}, {3})
		T:checkInventoryForItem({"minecraft:dirt"}, {128})
		T:checkInventoryForItem({"minecraft:cobblestone"}, {128})
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"minecraft:hopper"}, {1})
		T:checkInventoryForItem({"minecraft:torch"}, {21})
		T:checkInventoryForItem({"sapling"}, {21}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Creating automatic tree farm...")
		createAutoTreeFarm()
	elseif choice == 26 then	-- Manage auto-treefarm
		manageFarmSetup("tree")
	elseif choice == 27 then	-- clear natural forest
		T:checkInventoryForItem({"minecraft:chest"}, {1})
		T:checkInventoryForItem({"sapling"}, {64}, false)
		print(thanks)
		print("Clearing and replanting trees")
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		clearAndReplantTrees()
		
	-- FARMING
	elseif choice == 31 then	-- Create modular farm
		checkFuelNeeded(300)
		T:checkInventoryForItem({"minecraft:cobblestone"}, {64})
		T:checkInventoryForItem({"minecraft:dirt"}, {128}, false)
		T:checkInventoryForItem({"minecraft:water_bucket"}, {4})
		T:checkInventoryForItem({"minecraft:chest"}, {4})
		T:checkInventoryForItem({"sapling"}, {1})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Creating modular wheat farm")
		createFarm()
	elseif choice == 32 then	-- Extend modular farm
		checkFuelNeeded(300)
		T:checkInventoryForItem({"minecraft:cobblestone"}, {64})
		T:checkInventoryForItem({"minecraft:dirt"}, {128}, false)
		T:checkInventoryForItem({"minecraft:water_bucket"}, {4})
		T:checkInventoryForItem({"minecraft:chest"}, {5})
		T:checkInventoryForItem({"sapling"}, {1})
		print("Checking position...\n")
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createFarmExtension()
	elseif choice == 33 then	-- manage modular farm
		manageFarmSetup("farm")
	
	-- OBSIDIAN
	elseif choice == 41 then --harvest obsidian
		checkFuelNeeded(width * length * 3)
		T:checkInventoryForItem({"stone"}, {width * length})
		print(thanks)
		sleep(2)
		print("Harvesting obsidian area: size "..width.. " x "..length)
		harvestObsidian(width, length)
	elseif choice == 42 then --build nether portal
		checkFuelNeeded(20)
		T:checkInventoryForItem({"minecraft:obsidian"}, {width * height})
		T:checkInventoryForItem({"stone"}, {width + 4})
		print(thanks)
		sleep(2)
		print("Building Nether portal")
		createPortal(width, height)
	elseif choice == 43 then --demolish nether portal
		checkFuelNeeded(20)
		print("Demolishing Nether portal")
		demolishPortal(width, height)
	elseif choice == 44 then --undermine dragon towers
		local choice = getTaskHelp(2, 44) -- compulsory help display
		if choice == "" then -- Enter only pressed
			checkFuelNeeded(500)
			T:checkInventoryForItem({"minecraft:cobblestone", "minecraft:cobbled_deepslate"}, {84, 84})
			print("Undermining dragon towers")
			undermineDragonTowers()
		end
	elseif choice == 45 then --deactivate dragon tower
		local choice = getTaskHelp(2, 45) -- compulsory help display
		if choice == "" then -- Enter only pressed
			checkFuelNeeded(50)
			print("Deactivating dragon tower")
			deactivateDragonTower()
		end
	elseif choice == 46 then --build dragon attack area
		local choice = getTaskHelp(2, 46) -- compulsory help display
		if choice == "" then -- Enter only pressed
			checkFuelNeeded(200)
			T:checkInventoryForItem({"stone"}, {128})
			print("Building dragon attack area")
			createDragonAttack()
		end
	elseif choice == 47 then -- attack dragon
		if getBoolean("Near the Dragon perch? (y/n)") then
			local choice = getTaskHelp(2, 47) -- compulsory help display
			if choice == "" then -- Enter only pressed
				attack(true)
			end
		else
			attack(false) -- used for shulkers or in dragon water trap
		end
	elseif choice == 48 then --build portal minecart station
		local choice = getTaskHelp(2, 48) -- compulsory help display
		if choice == "" then -- Enter only pressed
			checkFuelNeeded(200)
			T:checkInventoryForItem({"stone"}, {64})
			T:checkInventoryForItem({"minecraft:powered_rail", "minecraft:golden_rail"}, {1, 1}, false)
			T:checkInventoryForItem({"minecraft:rail"}, {2}, false)
			T:checkInventoryForItem({"minecraft:minecart"}, {1}, false)
			T:checkInventoryForItem({"minecraft:stone_button"}, {1}, false)
			T:checkInventoryForItem({"minecraft:ladder"}, {10}, false)
			print("Building portal platform")
			createPortalPlatform()
		end
	elseif choice == 49 then --build dragon water trap
		local choice = getTaskHelp(2, 49) -- compulsory help display
		if choice == "" then -- Enter only pressed
			checkFuelNeeded(256)
			T:checkInventoryForItem({"stone"}, {256})
			T:checkInventoryForItem({"minecraft:obsidian"}, {1})
			T:checkInventoryForItem({"minecraft:ladder"}, {145})
			T:checkInventoryForItem({"minecraft:water_bucket"}, {1})
			print("Building dragon water trap")
			createDragonTrap()
		end
		
	-- CANAL BRIDGE
	elseif choice == 51 then	-- continuous path over void/water/lava
		checkFuelNeeded(512) -- allow for 512 length
		T:checkInventoryForItem({"stone"}, {length}, false)
		--T:checkInventoryForItem({"minecraft:cobblestone",  "minecraft:dirt"}, {64, 64}, false)
		T:checkInventoryForItem({"minecraft:torch"}, {math.floor(length/8)}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building continuous path")
		createPath(length)
	elseif choice == 52 then	-- simple 2 block coridoor
		checkFuelNeeded(length)
		T:checkInventoryForItem({"stone"}, {length * 2}, false)
		T:checkInventoryForItem({"minecraft:torch"}, {math.floor(length * 2 / 8)}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building simple coridoor")
		createCorridor(length)
	elseif choice == 53 then	-- bridge over void/water/lava
		checkFuelNeeded((size + 1) * 2)
		T:checkInventoryForItem({"stone"}, {size * 2})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building bridge ".. size.." blocks")
		createBridge(size)
	elseif choice == 54 then	-- covered walkway
		checkFuelNeeded((size + 1) * 2)
		T:checkInventoryForItem({"stone"}, {size * 2})
		T:checkInventoryForItem({"minecraft:torch"}, {math.ceil(size / 8)}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building bridge ".. size.." blocks")
		createWalkway(size)
	elseif choice == 55 then	-- canal management
		getTaskHelp(2, 55) -- compulsory help display
		read() -- pause until user ready
		local torches = 64
		if length > 0 then
			checkFuelNeeded(length * 4) -- allow for 1024 length
			torches = math.floor(length / 8)
		else
			checkFuelNeeded(2048) -- allow for 1024 length
		end
		T:checkInventoryForItem({"stone"}, {256})
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"minecraft:torch"}, {torches}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building canal")
		createCanal(size, length, height) -- eg 0, 312, 1 = complete a canal 312 blocks long on top of the wall
	elseif choice == 56 then	-- ice canal
		getTaskHelp(2, 56) -- compulsory help display
		read() -- pause until user ready
		if length > 0 then
			checkFuelNeeded(length)
		else
			checkFuelNeeded(1024) -- allow for 1024 length
		end
		T:checkInventoryForItem({"minecraft:packed_ice", "minecraft:blue_ice"}, {math.ceil(length / 2), math.ceil(length / 2)}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building ice canal")
		createIceCanal(length)
	elseif choice == 57 then -- platform
		local volume = width * length
		checkFuelNeeded(volume)
		T:checkInventoryForItem({"stone", "dirt"}, {volume, volume})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building platform")
		createPlatform(width, length)
	elseif choice == 58 then -- sinking platform
		local volume = (width + 1) * (length + 1) 
		checkFuelNeeded(volume * (height + 1))
		T:checkInventoryForItem({"stone"}, {volume})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building sinking platform")
		createSinkingPlatform(width, length, height)
	elseif choice == 59 then -- boat bubble lift
		getTaskHelp(2, 59) -- compulsory help display
		read() -- pause until user ready
		checkFuelNeeded(height * 6)
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"stone"}, {height * 4 + 8})
		T:checkInventoryForItem({"sign"}, {2})
		if size == 0 then -- new lift
			T:checkInventoryForItem({"minecraft:soul_sand"}, {2})
			T:checkInventoryForItem({"slab"}, {2})
		end
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building boat bubble lift")
		createBoatLift(size, length, height) -- size:0=new, size:1=extend, length:0=left, 1=right
	elseif choice == 510 then	-- ice canal borders with trapdoors
		checkFuelNeeded(length)
		T:checkInventoryForItem({"trapdoor"}, {length})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Building ice canal barrier")
		createIceCanalBorder(size, length)
	-- MOB SPAWNER
	elseif choice == 61 then	--  9x9 hollow cube cobble lined
		checkFuelNeeded(600) -- allow for 600 moves
		T:checkInventoryForItem({"stone"}, {256})
		T:checkInventoryForItem({"slab"}, {1})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobFarmCube(false, false) -- not blaze, not contiuation of blaze
	elseif choice == 62 then	--  flood mob spawner
		checkFuelNeeded(60) -- allow for 60 moves
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"fence"}, {2})
		T:checkInventoryForItem({"sign"}, {2})
		T:checkInventoryForItem({"slab"}, {1})
		T:checkInventoryForItem({"minecraft:soul_sand", "minecraft:dirt"}, {1, 1}, true)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		floodMobFarm()
	elseif choice == 63 then -- build bubble lift on top of soul sand
		checkFuelNeeded(200) -- allow for 200 moves
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"stone"}, {128})
		if T:getBlockType("down") ~= "minecraft:soul_sand" then
			T:checkInventoryForItem({"minecraft:soul_sand"}, {1})
		end
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobBubbleLift(size)
	elseif choice == 64 then -- dig mob trench
		checkFuelNeeded(length * height) -- allow for 600 moves
		T:checkInventoryForItem({"stone"}, {128})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		digMobTrench(height, length)
	elseif choice == 65 then	-- Blaze spawner
		checkFuelNeeded(2500) -- allow for 2500 moves
		local continue = getBoolean("Is this a new spawner? (y/n)\n('n' if building blaze killzone)")
		continue = not continue -- answered y to new spawner: not continuing!
		if not continue then
			T:checkInventoryForItem({"stone"}, {640})
			T:checkInventoryForItem({"slab"}, {1})
			print("You will be asked for more assets later\n")
			os.sleep(2)    -- pause for 2 secs to allow time to press esc
		end
		createMobFarmCube(true, continue)
	elseif choice == 66 then -- build endermen tower
		-- build in 3 sections, base, tower, top
		getTaskHelp(2, 66) -- compulsory help display
		read() -- pause until user ready
		local numFuel = 700
		local options = 
		{
			"New tower lower base",
			"Add tower upper base + killzone",
			"128 block tower to existing base"
		}
		local option, modifier = menu.new("Select build stage:", options)
		if option == 1 then --lower base
			T:checkInventoryForItem({"minecraft:chest"}, {1})
			T:place("chest", -1, "down", false)
			T:emptyInventory("up")
			checkFuelNeeded(320) -- allow for 320 moves
			T:checkInventoryForItem({"stone"}, {144}) -- <3 stacks
			T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
			T:checkInventoryForItem({"fence"}, {4})
			T:checkInventoryForItem({"sign"}, {4})
			T:checkInventoryForItem({"door"}, {2})
			T:checkInventoryForItem({"minecraft:soul_sand"}, {1})
		elseif option == 2 then -- upper base
			checkFuelNeeded(710) -- allow for 703 moves
			T:checkInventoryForItem({"stone"}, {384}) -- 6 stacks
			T:checkInventoryForItem({"minecraft:bucket"}, {4})
			T:checkInventoryForItem({"fence"}, {15})
			T:checkInventoryForItem({"sign"}, {4})
			T:checkInventoryForItem({"ladder"}, {3})
			T:checkInventoryForItem({"minecraft:soul_sand"}, {1})
		else -- main tower
			checkFuelNeeded(3000) -- allow for 3000 moves
			if T:getBlockType("down") ~= "minecraft:chest" then
				T:checkInventoryForItem({"minecraft:chest"}, {1})
				T:place("chest", -1, "down", false)
			end
			T:checkInventoryForItem({"stone"}, {768}) -- 12 stacks
			T:checkInventoryForItem({"minecraft:bucket"}, {10})
			T:checkInventoryForItem({"fence"}, {64})	-- 1 stacks		
		end
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createEnderTower(option) -- 1, 2, or 3
	--[[
	elseif choice == 66 then	--  block path over water
		checkFuelNeeded(size + 24) -- allow for 88 moves
		T:checkInventoryForItem({"minecraft:cobblestone"}, {88})
		T:checkInventoryForItem({"stone"}, {1})
		T:checkInventoryForItem({"minecraft:torch"}, {8}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobSpawnerBase(size)
	elseif choice == 67 then	-- mob spawner tower
		checkFuelNeeded(1500) -- allow for 1000 blocks +  moves
		T:checkInventoryForItem({"minecraft:chest"}, {2})
		T:checkInventoryForItem({"minecraft:hopper"}, {2})
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"minecraft:cobblestone"}, {576})
		T:checkInventoryForItem({"stone"}, {1})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobSpawnerTower(size)
	elseif choice == 68 then	-- mob spawner tank 
		checkFuelNeeded(1000) -- allow for 500 blocks +  moves
		T:checkInventoryForItem({"minecraft:water_bucket"}, {2})
		T:checkInventoryForItem({"minecraft:cobblestone"}, {576})
		T:checkInventoryForItem({"stone"}, {1})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobSpawnerTank()
	elseif choice == 69 then	-- mob spawner roof
		checkFuelNeeded(500) -- allow for 400 blocks +  moves
		T:checkInventoryForItem({"minecraft:cobblestone"}, {384})
		T:checkInventoryForItem({"minecraft:torch"}, {20}, false)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		createMobSpawnerRoof()
	]]
			
	-- AREA CARVING
	elseif choice == 71 then--Clear area
		checkFuelNeeded(width * length * 3)
		T:checkInventoryForItem({"minecraft:dirt"}, {64})
		print(thanks)
		sleep(2)
		print("Clearing area: size "..width.. " x "..length)
		clearArea(width, length, true)
	elseif choice == 72 then --Clear rectangle
		checkFuelNeeded(width * length)
		print("Clearing rectangle: size "..width.. " x "..length)
		clearRectangle(width, length)
	elseif choice == 73 then --Clear wall
		checkFuelNeeded(length * height)
		print("Recycling wall "..height.." blocks high")
		clearWall(1, length, height, size)
	elseif choice == 74 then --Clear single height perimeter wall
		checkFuelNeeded((width + length) * 2)
		print("Recycling wall section "..width.." x "..length)
		clearPerimeter(width, length, false, false)
	elseif choice == 75 then --Clear hollow structure
		--size = direction (0/1=up/down); width/length/height as expected
		--size =  Bottom->Top (0), Top->bottom(1)
		local withCeiling = true
		local withFloor = true
		local withCeiling = getBoolean("Remove ceiling? (y/n)")
		local withFloor = getBoolean("Remove floor? (y/n)")
		checkFuelNeeded((width * length) + ((width + length) * height))
		print("Recycling hollow structure "..width.." x "..length.." height: "..height)
		clearBuilding(width, length, height, size, withCeiling, withFloor) -- eg 5, 5, 6, 0, true, true
	elseif choice == 76 then --Clear solid structure
		--size =  Bottom->Top (0), Top->bottom(1)
		checkFuelNeeded((width * length) + ((width + length) * height))
		print("Recycling solid structure "..width.." x "..length.." height: "..height)
		clearSolid(width, length, height, size) -- size is 'up' or 'down'
	elseif choice == 77 then	-- Dig trench
		checkFuelNeeded(height * length * 2)
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		if length == 0 then
			print("Digging continuous trench "..height.." blocks deep")
		else
			print("Digging trench "..length.." blocks long, "..height.." blocks deep")
		end
		digTrench(height, length)
	elseif choice == 78 then --Carve mountain
		--size =  Left <- Right (0), Left _> Right(1)
		checkFuelNeeded(length * width * 10)
		print("Carving mountain side "..width.." x "..length)
		clearMountainSide(width, length, size) -- size is 'left<-Right' or 'left->Right'
	elseif choice == 79 then -- Place floor or Ceiling
		checkFuelNeeded(width * length)
		T:clear()
		print("IMPORTANT!\n\nRemove any items not used to create\nthe floor or ceiling.")
		print("\n\nAdd at least "..width * length.." any blocks\nstarting in slot 1\n\n")
		term.write("Next -> Enter")
		read()
		local items = {	"Replacing current floor",
						"New floor over existing",
						"Replacing current ceiling",
						"New ceiling under existing"}
		size = menu.new("Laying what?", items)
		createFloorCeiling(width, length, size ) -- size integer 1 to 4
		
	-- LAVA WATER
	elseif choice == 81 then --build containing wall in water or lava
		checkFuelNeeded(length * length)
		T:checkInventoryForItem({"stone"}, {1024}, false)
		print("Building retaining wall in lava/water. length"..length)
		createRetainingWall(length, height)
	elseif choice == 82 then --drop sand
		checkFuelNeeded(100)
		T:checkInventoryForItem({"minecraft:sand"}, {1024}, false)
		print("Building sand wall. length: "..length)
		createSandWall(length)
	elseif choice == 83 then --decapitate and drop sand
		checkFuelNeeded(length * width)
		T:checkInventoryForItem({"minecraft:sand"}, {768}, false)
		print("Decapiating structure. length: "..length.." width: "..width)
		decapitateBuilding(width, length)
	elseif choice == 84 then --harvest sand
		checkFuelNeeded(100)
		print("Digging sand. length: "..length)
		clearSandWall(length)
	elseif choice == 85 then --remove sand cube
		checkFuelNeeded(length * width * 4)
		print("Removing sand cube. length: "..length.." width: "..width)
		clearSandCube(width, length)
	elseif choice == 86 then --
		--[[
		checkFuelNeeded(length * width + 64)
		print("Clearing monument layer: "..length.." width: "..width)
		clearMonumentLayer(width, length, size)]]
	elseif  choice == 87 then -- Drain water/lava
		if length == 0 or width == 0 then
			checkFuelNeeded(width * length)
			T:checkInventoryForItem({"stone"}, {128}, false)
			print("Draining enclosed area with auto settings")
		else
			checkFuelNeeded(width * length)
			T:checkInventoryForItem({"stone"}, {length * 2}, false)
			print("Draining enclosed area "..width.." x "..length)
		end
		drainLiquid(width, length, size)
	elseif choice == 88 then --clear monument layer
		checkFuelNeeded(length * width + 64)
		print("Clearing monument layer: "..length.." width: "..width)
		clearMonumentLayer(width, length, size)
	elseif choice == 89 then --ladder to water/lava
		checkFuelNeeded(height * 2)
		T:checkInventoryForItem({"minecraft:ladder"}, {height})
		local cobble = height * 3 + 10
		T:checkInventoryForItem({"stone"}, {cobble})
		print(thanks)
		os.sleep(2)    -- pause for 2 secs to allow time to press esc
		print("Creating ladder to bedrock")
		createLadderToWater()
	elseif choice == 810 then --remove plants
		checkFuelNeeded(length * width * 4)
		T:checkInventoryForItem({"minecraft:sand"}, {64})
		print("Removing seaweed. length: "..length.." width: "..width)
		clearSeaweed(width, length)
		
	-- RAILWAY
	elseif choice == 91 then --place redstone torch level or downward slope
		checkFuelNeeded(10)
		local userChoice = T:checkInventoryForItem({"stone"}, {1})
		T:checkInventoryForItem({"minecraft:redstone_torch"}, {1})
		print("Placing redstone torch on ".. userChoice)
		placeRedstoneTorch("level", userChoice)
	elseif choice == 92 then --place redstone torch on upward slope
		checkFuelNeeded(10)
		local userChoice = T:checkInventoryForItem({"stone"}, {1})
		T:checkInventoryForItem({"minecraft:redstone_torch"}, {1})
		print("Placing redstone torch and ".. userChoice)
		placeRedstoneTorch("up", userChoice)
	elseif choice == 93 then --build downward slope
		checkFuelNeeded(height * 2)
		T:checkInventoryForItem({"stone"}, {height})
		--T:checkInventoryForItem({"minecraft:redstone_torch"}, {math.ceil(height / 3)}, false)
		print("Building downward slope")
		createRailwayDown(height)
	elseif choice == 94 then --build upward slope
		checkFuelNeeded(height * 2)
		T:checkInventoryForItem({"stone"}, {height + math.ceil(height / 3)})
		--T:checkInventoryForItem({"minecraft:redstone_torch"}, {math.ceil(height / 3)}, false)
		print("Building upward slope")
		createRailwayUp(height)
	end
	return retValue
end

function harvestObsidian(width, length)
	local heightParts = math.floor(length / 8) -- eg 12/8 = 1
	local lastPart = length - (heightParts * 8) -- eg 12 - (1 * 8) = 4
	if width % 2 ~= 0 then
		width = width + 1
	end
	for y = 1, width do
		print("Mining column "..tostring(y).." of "..tostring(width))
		for j = 1, heightParts do
			T:go("m8")
		end
		if lastPart > 0 then
			T:go("m"..tostring(lastPart)) -- eg m4
		end
		-- width = tonumber(width)
		if y < width then
			if y % 2 == 0 then
				T:go("L1F1L1")
			else
				T:go("R1F1R1")
			end
		end
	end
end

function manageFarm(useFile)
	local lib = {}
	
	function lib.checkPosition(currentRow)
		local atHome = false
		local blockType = T:getBlockType("down")
		print("Checking position\n"..blockType.. " below")
		if string.find(blockType, "water") ~= nil then --over water
			if T:getBlockType("forward") == "minecraft:chest" then
				atHome = true
				T:turnRight(1)
				if T:getBlockType("forward") == "minecraft:chest" then
					T:turnRight(2)
				else
					T:turnRight(1)
				end
			else -- not a chest
				T:turnRight(1)
				if T:getBlockType("forward") == "minecraft:chest" then
					atHome = true
					T:turnLeft(1)
				end
			end
		end
		if currentRow == nil and not atHome then -- no position file
			print("Unable to determine my position.\n")
			print("Place me in the lower left corner")
			print("over water, facing the crops")
			print("with chests to my right and behind")
			print("\nEnter to continue")
			read()
		end
		return atHome
	end
	
	function lib.crossFarm(stopAtFirst)
		local blockType = ""
		if stopAtFirst == nil then
			stopAtFirst = false
		end
		-- will go forward until chest or cobble detected below
		-- if detected within 1 move, this is ignored
		local numMoves = 0
		local endOfPath = false
		while not endOfPath do
			local success, data = turtle.inspectDown()
			if data.name == nil then --nothing below
				turtle.forward()
			elseif data.name == "minecraft:chest" or data.name =="minecraft:cobblestone" then
				blockType = data.name
				if stopAtFirst then -- do not move further if first instance of cobble or chest
					endOfPath = true
				else
					if numMoves <= 1 then -- has chest been detected after 0 or 1 move?
						turtle.forward()
					else -- chest found after >1 move
						endOfPath = true
					end
				end
			else
				turtle.forward()
			end
			numMoves = numMoves + 1
		end
		return blockType -- either "" or chest/cobble
	end
	
	function lib.emptyCropItem(direction, item, keepAmount)
		local lib2 = {}
		
		function lib2.getInventory(tbl)
			local indexes = {"carrot", "potato", "wheat_seeds",  "beetroot_seeds", "wheat", "beetroot", "sapling"} --order important wheat_seeds before wheat
			local inventory = {}
			for i = 1, #indexes do
				inventory[indexes[i]] = {}
				inventory[indexes[i]].leastSlot = 0
				inventory[indexes[i]].minQ = 0
				inventory[indexes[i]].mostSlot = 0
				inventory[indexes[i]].maxQ = 0
			end
			-- eg inventory["sapling"].leastSlot = 0
			for i = 1, 16 do
				--print("index :"..i)
				if turtle.getItemCount(i) >  0 then -- only check if items in slot
					local data = turtle.getItemDetail(i)
					local crop = ""
					for j = 1, #indexes do
						if data.name:find(indexes[j]) ~= nil then
							crop = indexes[j]
							break
						end
					end
					-- eg carrot = tbl["minecraft:carrot"], sapling = tbl["minecraft:oak_sapling"]
					if crop ~= "" then
						if inventory[crop].leastSlot == 0 then
							inventory[crop].leastSlot = i
							inventory[crop].minQ = data.count
						else -- crop already found
							if data.count < inventory[crop].minQ then
								if data.count > inventory[crop].maxQ then
									inventory[crop].mostSlot = i
									inventory[crop].maxQ = data.count
								end
								inventory[crop].leastSlot = i
								inventory[crop].minQ = data.count
							else -- current quantity > minQ. check if > maxQ
								if data.count >= inventory[crop].maxQ then
									inventory[crop].mostSlot = i
									inventory[crop].maxQ = data.count
								end
							end
						end
						--print("Slot "..i.."-"..crop..": minQ:"..inventory[crop].minQ)
						--read()
					end
				end
			end
			-- only one slot  available == leastSlot, mostSlot = 0
			-- 2 or more slots available == smallest in leastSlot, largest in mostSlot
			return inventory -- inventory["sapling"].leastSlot = 1, inventory["carrots"].leastSlot = 3
		end
		
		function lib2.drop(slot, direction, count)
			if direction == nil then
				direction = "forward"
			end
			local drop = turtle.drop
			if direction == "down" then
				drop = turtle.dropDown
			elseif direction == "up" then
				drop = turtle.dropUp
			end
			
			local success = true
			if slot == nil then
				success = false
			else
				--print("slot = "..slot)
				if slot > 0 then
					turtle.select(slot)
					if count == 0 then
						if drop() then
							success = true
						end
					else
						if drop(count) then
							success = true
						end
					end
				end
			end
			return success
		end
		
		function lib2.main(direction, item, keepAmount)
			if keepAmount == nil then
				keepAmount = 0
			end
			--local tbl = lib2.createTable() -- eg tbl["minecraft:sapling"] = "sapling"
			local inventory = lib2.getInventory() -- get table of crop items
			--print("Inventory obtained. Enter")
			--read()
			local search = inventory[item] -- returns inventory["sapling"] if it exists
			if search ~= nil then
				--print("Search: "..tostring(search.minQ).." slot: "..tostring(search.leastSlot))
				local total = search.minQ + search.maxQ
				--print("Total: "..total.." keep: "..keepAmount)
				while total > keepAmount do
					if keepAmount == -1 then -- drop 1 stack only
						if search.maxQ > 0 then
							lib2.drop(search.mostSlot, direction, 0)
						elseif search.minQ > 1 then
							lib2.drop(search.leastSlot, direction, 1) -- drop 1 item to reserve a place
						end
						break
					elseif keepAmount == 0 then
						lib2.drop(search.leastSlot, direction, 0)
						lib2.drop(search.mostSlot, direction, 0)
					else -- calculate ideal slot to drop based on keepAmount
						if keepAmount == 64 and total > 64 then -- some needs dropping
							--print("About to drop from "..search.leastSlot..", "..direction)
							lib2.drop(search.leastSlot, direction, 0)
						else
							local count = search.minQ - keepAmount
							lib2.drop(search.mostSlot, direction, 0)
							lib2.drop(search.leastSlot, direction, count)
						end	
					end
					inventory = lib2.getInventory()
					search = inventory[item]
					total = search.minQ + search.maxQ
				end
			end
		end
		
		lib2.main(direction, item, keepAmount)
		turtle.select(1)
	end
	
	function lib.farmToRight(isFarmToRight, isFarmToFront, farmPos, waiting)
		while isFarmToRight do -- move to next farm
			--get some seeds and veg to take to next farm
			lib.getCrops()
			T:up(1)
			lib.crossFarm(false)
			T:go("F1D1")
			farmPos[1] = farmPos[1] + 1 -- change x position
			currentRow = 1
			lib.manageTree(true, farmPos)
			isFarmToRight, isFarmToFront, farmPos, waiting = lib.watchFarm(waiting, farmPos, currentRow, going)
		end
		return isFarmToRight, isFarmToFront, farmPos, waiting
	end
	
	function lib.getCrops()
		T:turnRight(1)
		if T:getBlockType("forward") == "minecraft:chest" then
			lib.getSeeds("forward")
		end
		T:turnRight(1)
		if T:getBlockType("forward") == "minecraft:chest" then
			lib.getVeg("forward")
		end
		T:turnRight(2)
	end
	
	function lib.getSaplings(direction)
		if direction == nil then
			direction = "forward"
		end
		-- get saplings if available
		local saplingSlot = 0
		local logSlot = 0
		local quantity = 0
		local slotContains = ""
		local toolSlot = lib.getToolSlot()
		for i = 1, 16 do  -- fill inventory with seeds and saplings
			turtle.suck()
		end
		for i = 1, 16 do
			-- slotContains, slotCount, slotDamage
			slotContains, quantity = T:getSlotContains(i)
			if string.find(slotContains, "log") ~= nil then
				logSlot = i
			elseif string.find(slotContains, "sapling") ~= nil then
				saplingSlot = i
			end
		end
		lib.refuel(0) --use any sticks or planks for fuel
		lib.emptyCropItem(direction, "sapling", 1) -- ensure saplings occupy first slot in chest
		lib.emptyCropItem(direction, "wheat_seeds", 0)
		lib.emptyCropItem(direction, "beetroot_seeds", 0)
		turtle.select(1)
		return saplingSlot
	end
	
	function lib.getSeeds(direction)
		-- Inside inventory: minecraft:beetroot, minecraft:beetroot_seeds
		-- minecraft:carrot, minecraft:potato
		-- minecraft:wheat_seeds
		-- planted: minecraft:carrots, minecraft:beetroots, minecraft:potatoes, minecraft:wheat
		-- get 1 stack of wheat seeds, 1 stack of beetroot seeds
		if direction == nil then
			direction = "forward"
		end
		local suck = turtle.suck
		if direction == "down" then
			suck = turtle.suckDown
		end
		turtle.select(1)
		for i = 1, 16 do  -- fill inventory with seeds/ saplings/planks/sticks
			suck()
		end
		lib.refuel(0) -- remove any sticks or planks
		-- keep 1 stack of seeds, beetroot seeds
		lib.emptyCropItem(direction, "sapling", 0) -- ensure sapling fills first chest slot
		lib.emptyCropItem(direction, "beetroot_seeds", 64)
		lib.emptyCropItem(direction, "wheat_seeds", 64)
	end
	
	function lib.getToolSlot()
		local item = "minecraft:diamond_hoe"
		local toolSlot = T:getItemSlot(item)
		if toolSlot == 0 then
			item = "minecraft:diamond_pickaxe"
			toolSlot = T:getItemSlot(item)
			if toolSlot == 0 then
				item = "minecraft:crafting_table"
				toolSlot = T:getItemSlot(item)
			end
		end
		return toolSlot, item
	end
	
	function lib.getVeg(direction)
		if direction == nil then
			direction = "forward"
		end
		suck = turtle.suck
		if direction == "down" then
			suck = turtle.suckDown
		end
		-- get 1 stack of carrots, 1 stack of potatoes
		turtle.select(1)
		for i = 1, 16 do  -- fill inventory with carrots and potatoes
			suck()
		end
		lib.emptyCropItem(direction, "carrot", -1) -- drop one stack only if >1 slot
		lib.emptyCropItem(direction, "potato", -1) -- drop one stack only if >1 slot
		lib.emptyCropItem(direction, "carrot", 64) 
		lib.emptyCropItem(direction, "potato", 64) 
		lib.emptyCropItem(direction, "wheat", 0)
		lib.emptyCropItem(direction, "beetroot", 0)
	end
	
	function lib.goHome(currentRow)
		-- after a re-boot go to start
		local success = false
		local onWater = false
		local onChest = false
		local onCobble = false
		local onAir = false
		if currentRow == -1 then -- harvesting tree
			-- is tree above or in front
			-- check if log in front
			if string.find(T:getBlockType("forward"), "log") ~= nil then
				lib.harvestTree("forward")
			elseif string.find(T:getBlockType("up"), "log") ~= nil then
				lib.harvestTree("up")
			else
				while turtle.down() do end
			end
			-- should be on tree's dirt block
			local blockType = T:getBlockType("down")
			if blockType == "minecraft:chest" then
				onChest = true
			elseif blockType == "minecraft:dirt" or blockType == "minecraft:grass_block" then --on tree block
				local cobble = -1
				for i = 1, 4 do 
					T:forward(1)
					blockType = T:getBlockType("down")
					if blockType == "minecraft:chest" then
						onChest = true
						break
					elseif blockType == "minecraft:cobblestone" then
						cobble = i
					end
					turtle.back()
					T:turnRight(1)
				end
				if not onChest then
					if cobble >= 0 then
						T:turnRight(cobble)
						T:forward(1)
						onCobble = true
					else
						onAir = true
					end
				end
			else -- not over tree block
				onAir = true
			end
		else -- could be anywhere, currentRow 1-10 -2,-3,-4
			local blockType = T:getBlockType("down")
			-- is water below?
			if blockType:find("water") ~= nil then
				onWater = true
			elseif blockType:find("chest") ~= nil then
				onChest = true
			elseif blockType:find("cobble") ~= nil then
				onCobble = true
			else
				onAir = true -- over crops, bare soil or water source
			end
		end

		if onAir then -- if onAir find cobble or chest
			local blockType = lib.crossFarm(true) -- go along until chest or cobble found
			if blockType == "minecraft:cobblestone" then
				onCobble = true
			elseif blockType == "minecraft:chest" then
				onChest = true
			end
		end
		
		if onCobble then -- if onCobble find chest
			-- check which direction cobble continues
			for i = 1, 4 do
				if turtle.forward() then -- no obstruction
					local blockType = T:getBlockType("down")
					if blockType == "minecraft:cobblestone" then --continue this route
						break
					elseif blockType == "minecraft:chest" then --stay here and exit loop
						onChest = true
						break
					end
				else -- blocked ? tree/sapling
					local blockType = T:getBlockType("forward")
					if blockType:find("log") ~= nil or blockType:find("sapling") ~= nil then
						-- next to tree/sapling, but not on chest, must be behind extended farm
						T:go("R2F1") -- turn round and continue forwards
						break
					end
				end
				turtle.back()
				T:turnLeft(1)
			end
			if not onChest then
				-- move forward until cobble runs out-- will be over retaining wall, or on chest
				while T:getBlockType("down") == "minecraft:cobblestone" do
					local success = turtle.forward()
					if not success then -- movement obstructed, must be tree/sapling
						local blockType = T:getBlockType("forward")
						if blockType:find("log") ~= nil or blockType:find("sapling") ~= nil then
							-- next to tree/sapling, but not on chest, must be behind extended farm
							T:go("R2F1") -- turn round and continue forwards
						end
					end
				end
				-- no longer on cobble, could be a chest
				if  T:getBlockType("down") == "minecraft:chest" then
					onChest = true
				end
			end
			-- cobble ended, over edge of wall, on tree base with no sapling, or on chest
		end
		
		if onChest then -- if onChest find water
			-- check if next block is a chest
			for i = 1, 4 do
				if turtle.forward() then -- no obstruction
					local blockType = T:getBlockType("down")
					if blockType == "minecraft:dirt" or blockType == "minecraft:grass_block" then -- on tree base
						turtle.back()
						break
					elseif blockType == "minecraft:chest" then --stay here and exit loop
						onChest = true
						break
					end
				else -- blocked ? tree/sapling
					local blockType = T:getBlockType("forward")
					if blockType:find("log") ~= nil or blockType:find("sapling") ~= nil then
						-- next to tree/sapling
						break
					end
				end
				turtle.back()
				T:turnLeft(1)
			end
			-- now on chest next to tree
			T:go("R1F1D1")
			blockType = T:getBlockType("down")
			if blockType:find("water") ~= nil then
				onWater = true
			else -- no water so return to other side of chest
				T:go("R2U1F2D1")
				blockType = T:getBlockType("down")
				if blockType:find("water") ~= nil then
					onWater = true
				end
			end
		end
		
		if onWater then -- if onWater check position to discover if at start
			--orientate
			onWater = lib.checkPosition(currentRow)
			-- move over chest and look for cobble
			success = false
			while not success do
				T:go("R1U1F2R1F2L1") -- behind tree/sapling if cobble below continue on this route
				blockType = T:getBlockType("down")
				if blockType == "minecraft:cobblestone" then --continue
					while T:getBlockType("down") == "minecraft:cobblestone" do
						turtle.forward()
					end
					T:go("F1L1F1D1")
				else -- go back
					T:go("L1F2L1F2D1R1")
					success = true
				end
			end
		end
		
		return success
	end
	
	function lib.gotoTree(farmPos)
		turtle.select(1)
		T:turnRight(1)
		lib.getSaplings("forward")
		T:go("U1F1R1")
		lib.writePosition(farmPos, 0) 
		lib.harvestTree("forward") -- fell tree or plant sapling, return to start
	end
	
	function lib.harvest(waiting, farmPos, currentRow, going)
		if currentRow == 0 then
			T:go("U1") --ready to farm field
		end
		lib.writePosition(farmPos,"1") --field 1,1 row 1
		local isFarmToRight = false
		local isFarmToFront = false
		going = true
		local width = 11
		local length = 10
		for l = 1, length do
			for w = 1, width do
				isReady, blockType, status = lib.isCropReady("down")
				turtle.select(1)
				if blockType == "" then -- ? untilled soil or air above water
					turtle.digDown()
					turtle.digDown()
					lib.plantCrop("", "down")
				elseif isReady then
					turtle.digDown()
					lib.plantCrop(blockType, "down")
				end
				if  w == width and T:getBlockType("down") == "minecraft:chest" then
					isFarmToRight = true
				end
				if w < width then
					T:forward(1)
				end
			end
			-- end of the row: over cobble or chest of next farm
			if l < length then
				if going then
					T:go("L1F1L1")
				else
					T:go("R1F1R1")
				end	
			end
			currentRow = currentRow +  1
			going = not going
		end
		T:go("R1F1") -- goes over chest/cobble on top wall
		local blockType = T:getBlockType("down")
		if blockType == "minecraft:chest" then
			isFarmToFront = true
		end
		T:go("R2F1R1F1L1")
		-- now on cobble wall
		lib.writePosition(farmPos, "-2") -- heading towards start on left cobble
		while T:getBlockType("down") == "minecraft:cobblestone" do
			T:forward(1)
		end
		-- now above first chest
		T:go("F1L1F1D1") -- facing crops at start position
		lib.storeCrops() -- moves from start to deposit seeds and crops
		return isFarmToRight, isFarmToFront, farmPos, waiting
	end	
	
	function lib.harvestTree(direction)
		--[[
			start in front of / during tree harvest
			Check if sapling present
			Harvest tree if present, replant sapling
			Dispose of apples. Use sticks as fuel
			Return to base
		]]
		local gotLogs = false
		if direction == nil then
			direction = "forward"
		end
		local inFront = T:getBlockType("forward")
		local saplingSlot = T:getSaplingSlot()
		if inFront == "" and saplingSlot > 0 then --no tree or sapling
			turtle.select(saplingSlot)
			turtle.place()
		elseif string.find(inFront, "log") ~= nil or direction == "up" then -- tree above or in front
			-- clsTurtle.harvestTree(self, extend, craftChest, direction)
			T:harvestTree(false, false, direction) --do not investigate side branches in case chunk unloaded
			T:go("R2F1R2")
			saplingSlot = T:getSaplingSlot()
			if saplingSlot > 0 then
				turtle.select(saplingSlot)
				turtle.place()
			end
		end
		T:sortInventory()
		-- drop any apples
		local item = T:getItemSlot("minecraft:apple")
		if item > 0 then
			T:drop(item, "up")
		end
		-- return to base
		T:go("R1F1D1R2")
	end
	
	function lib.manageTree(toTree, farmPos)
		if toTree then
			lib.gotoTree(farmPos) --check for sapling or harvest tree, retuns to start
		end
		lib.refuelWithLogs() -- use any logs for fuel and store saplings
		lib.getSeeds("forward") -- get 1 stack of beetroot / wheat seeds
		T:turnRight(1)
		lib.getVeg("forward")
		T:turnRight(2)
		waiting = true
		--print("SortInventory. Enter")
		--read()
		T:sortInventory()
		lib.writePosition(farmPos, -1)
	end
	
	function lib.initialise()
		local doExit = false
		T:setEquipment()-- will ensure pickaxe on left and crafting table on right are equipped
		T:checkInventoryForItem({"minecraft:diamond_hoe"}, {1}, true) -- if already in place will continue
		-- equip hoe, swapping out crafting chest
		if not T:equip("right", "minecraft:diamond_hoe", 0) then
			print("Unable to equip hoe. Enter to quit")
			read()
			doExit = true
		end
		return doExit
	end
	
	function lib.isCropReady(direction)
		local isReady = false
		local status = ""
		local blockType = ""
		if direction == nil then
			direction = "forward"
		end
		local success = false
		local data = {}
		if direction == "down" then
			success, data = turtle.inspectDown()
		else
			success, data = turtle.inspect()
		end
		if success then
			blockType = data.name
			if data.name == "minecraft:carrots" then
				status = data.state.age.." / 7"
				if data.state.age == 7 then
					isReady = true
				end
			elseif data.name == "minecraft:potatoes" then
				status = data.state.age.." / 7"
				if data.state.age == 7 then
					isReady = true
				end
			elseif data.name == "minecraft:wheat" then
				status = data.state.age.." / 7"
				if data.state.age == 7 then
					isReady = true
				end
			elseif data.name == "minecraft:beetroots" then
				status = data.state.age.." / 3"
				if data.state.age == 3 then
					isReady = true
				end
			end
		end
		return isReady, blockType, status
	end
		
	function lib.plantCrop(crop, direction)
		if direction == nil then
			direction = "down"
		end
		local place = turtle.place
		if direction == "down" then
			place = turtle.placeDown
		end
		
		local crops = {}
		crops.carrot = 0
		crops.potato = 0
		crops.wheat = 0
		crops.beetroot = 0
		
		local success = false
		local data = {}
		local cropList = {}
		for i = 1, 16 do
			local count = turtle.getItemCount(i)
			if count > 0 then
				data = turtle.getItemDetail(i)
				if data.name == "minecraft:carrot" then
					crops.carrot = i
				elseif data.name == "minecraft:potato" then
					crops.potato = i
				elseif data.name == "minecraft:wheat_seeds" then
					crops.wheat = i
				elseif data.name == "minecraft:beetroot_seeds" then
					crops.beetroot = i
				end
			end
		end
		local anyCropSlot = 0

		if crops.carrot > 0 then
			table.insert(cropList, crops.carrot)
		end
		if crops.potato > 0 then
			table.insert(cropList, crops.potato)
		end
		if crops.wheat > 0 then
			table.insert(cropList, crops.wheat)
		end
		if crops.beetroot > 0 then
			table.insert(cropList, crops.beetroot)
		end
		if crop == nil or crop == "" then -- choose any crop randomly
			if #cropList > 0 then
				math.randomseed(os.time())
				anyCropSlot = cropList[math.random(1, #cropList)]
				turtle.select(anyCropSlot)
				place()
			end
		else
			if crop:find("carrot") ~= nil and crops.carrot > 0 then
				turtle.select(crops.carrot)
				place()
			elseif crop:find("potato") ~= nil and crops.potato > 0 then
				turtle.select(crops.potato)
				place()
			elseif crop:find("wheat") ~= nil and crops.wheat > 0 then
				turtle.select(crops.wheat)
				place()
			elseif crop:find("beetroot") ~= nil and crops.beetroot > 0 then
				turtle.select(crops.beetroot)
				place()
			else
				if anyCropSlot > 0 and anyCropSlot ~= toolSlot then
					turtle.select(anyCropSlot)
					place()
				end
			end
		end
		turtle.select(1)
	end
	
	function lib.readPosition()
		local farmPos = {nil, nil}
		local row = nil
		if fs.exists("farmPosition.txt") then
			local h = fs.open("farmPosition.txt", "r")
			farmPos[1] = h.readLine()
			farmPos[2] = h.readLine()
			row = h.readLine()
			h.close()
		end
		return farmPos, row
	end
	
	function lib.refuel(slot)
		if slot == nil or slot == 0 then
			for i = 1, 16 do --search for sticks or planks
				if turtle.getItemCount(i) > 0 then
					-- slotContains, slotCount, slotDamage
					slotContains, quantity = T:getSlotContains(i)
					if string.find(slotContains, "planks") ~= nil then
						--refuel with planks unless full
						turtle.select(i)
						while turtle.getFuelLevel() < turtle.getFuelLimit()  do
							if not turtle.refuel(1) then
								break
							end
						end
					end
				end
			end
		else
			turtle.select(slot)
			turtle.refuel()
		end
		turtle.select(1)
	end
	
	function lib.refuelWithLogs()
		-- assume positioned in front of seed/sapling chest
		local toolSlot, item = lib.getToolSlot()
		local logSlot = 0
		for i = 1, 16 do -- drop everything except crafting table and logs into chest
			if i ~= toolSlot then
				--return slotContains, slotCount, slotDamage
				if turtle.getItemCount(i) > 0 then
					local slotContains, slotCount, slotDamage = T:getSlotContains(i)
					if string.find(slotContains, "log") == nil then --not a log
						T:drop(i, "forward")
					else
						logSlot = i
					end
				end
			end
		end
		if logSlot > 0 and toolSlot > 0 then -- logs onboard so need to craft
			--slotContains, slotCount, slotDamage
			if item == "minecraft:crafting_table" then
				turtle.select(toolSlot)
				if not T:equip("right", item, 0) then
					print("Unable to equip "..item..". Enter to quit")
					read()
					toolEquipped = false
				end
			end
			local slot = T:getFirstEmptySlot() --find an empty slot in the inventory
			turtle.select(slot)
			turtle.suck() --take first item from chest into empty slot eg saplings
			turtle.select(toolSlot)
			turtle.drop() -- put hoe into chest
			turtle.select(slot) --contains item from chest
			turtle.dropDown() -- drop into water below
			turtle.craft() --craft logs into planks
			turtle.select(toolSlot)
			turtle.suck() --remove hoe/pickaxe from chest
			turtle.select(slot)
			turtle.suckDown() --retrieve other item from below
			turtle.drop() -- put item back into chest
			toolSlot, item = lib.getToolSlot()
			turtle.select(toolSlot)
			if not T:equip("right", item, 0) then
				print("Unable to equip "..item..". Enter to quit")
				read()
				toolEquipped = false
			end
			lib.refuel(0) --refuel to max limit
		end
		turtle.select(1)
		return toolEquipped
	end
	
	function lib.returnToFront(farmPos)
		if farmPos[2] > 1 then
			currentRow = -5
			T:go("U1R1")
			lib.writePosition(farmPos, currentRow)
			while farmPos[2] > 1 do -- return to start
				lib.crossFarm(false)
				turtle.back()
				farmPos[2] = farmPos[2] - 1 -- change x position
				lib.writePosition(farmPos, currentRow)
			end
			T:go("D1L1")
		end
		return farmPos
	end
	
	function lib.returnToLeft(farmPos)
		if farmPos[1] > 1 then
			currentRow = -4
			T:go("U1R2")
			lib.writePosition(farmPos, currentRow)
			while farmPos[1] > 1 do -- return to start
				lib.crossFarm(false)
				turtle.back()
				farmPos[1] = farmPos[1] - 1 -- change x position
				lib.writePosition(farmPos, currentRow)
			end
			T:go("D1R2")
		end
		return farmPos
	end
	
	function lib.storeCrops()
		T:turnRight(1)
		if T:getBlockType("forward") == "minecraft:chest" then
			lib.storeSeeds("forward", true)
		end
		T:turnRight(1)
		if T:getBlockType("forward") == "minecraft:chest" then
			lib.storeVeg("forward", true)
		end
		T:turnRight(2)
	end
	
	function lib.storeSeeds(direction, all)
		if direction == nil then
			direction = "forward"
		end
		if all == nil then
			all = true
		end
		if string.find(T:getBlockType(direction), "chest") ~= nil then -- chest exists
			lib.emptyCropItem(direction, "beetroot_seeds", 64)
			lib.emptyCropItem(direction, "wheat_seeds", 64)
			if all then
				lib.emptyCropItem(direction, "beetroot_seeds", 0)
				lib.emptyCropItem(direction, "wheat_seeds", 0)
			end
		end
		turtle.select(1)
	end
	
	function lib.storeVeg(direction, all)
		if direction == nil then
			direction = "forward"
		end
		if all == nil then
			all = true
		end
		for i = 1, 16 do
			if turtle.getItemCount(i) > 0 then
				local item = T:getItemName(i)
				if item == "minecraft:poisonous_potato" or item == "minecraft:apple" then
					T:drop(i, "up")
				end
			end
		end
		if string.find(T:getBlockType(direction), "chest") ~= nil then
			lib.emptyCropItem(direction, "carrot", 64)
			lib.emptyCropItem(direction, "potato", 64)
			if all then
				lib.emptyCropItem(direction, "carrot", 0)
				lib.emptyCropItem(direction, "potato", 0)
			end
			lib.emptyCropItem(direction, "wheat", 0)
			lib.emptyCropItem(direction, "beetroot", 0)
			
		end
		turtle.select(1)
	end
	
	function lib.watchFarm(waiting, farmPos, currentRow, going)
		local isFarmToRight = false
		local isFarmToFront = false
		local isReady, blockType, status
		-- check state of crop below. Harvest if ripe
		while waiting do
			isReady, blockType, status = lib.isCropReady("forward")
			if blockType == "" or isReady then
				waiting = false
			else
				print("Waiting for "..blockType.." status: "..status)
				sleep(60)
			end
		end
		currentRow = 0 -- force turtle up ready to harvest
		isFarmToRight, isFarmToFront, farmPos, waiting = lib.harvest(waiting, farmPos, currentRow, going)
		return isFarmToRight, isFarmToFront, farmPos, waiting
		--return waiting, farmPos, currentRow, going
	end
	
	function lib.writePosition(farmPos, currentRow)
		local h = fs.open("farmPosition.txt", "w")
		h.writeLine(tostring(farmPos[1]))
		h.writeLine(tostring(farmPos[2]))
		h.writeLine(tostring(currentRow))
		h.close()
	end
	
	function lib.main(useFile)
		--[[
			called from args on start, or from user choice
			farm already built, needs planting and/or harvesting
			needs both pickaxe and hoe
			may start in any position if chunk unloaded while running
		]]
		if useFile == nil then
			useFile = true
		end
		local farmPos = {nil, nil}
		local currentRow = nil
		local waiting = false
		local going = true
		local doExit = false
		if useFile then -- read file
			farmPos, currentRow = lib.readPosition() -- nil or a farm position and row number
			--print("File read:"..farmPos[1]..", "..farmPos[2]..", "..currentRow)
		end
		if farmPos[1] == nil then
			farmPos[1] = 1
			farmPos[2] = 1
		end
		if lib.checkPosition(currentRow) then -- ? already at home
			currentRow = nil
			farmPos[1] = 1
			farmPos[2] = 1
			--print("CurrentRow reset to nil. Enter")
			--read()
		else -- not at home
			if currentRow == nil then -- no position file and not at home
				doExit = true -- break out of this task
			end
		end
		if currentRow == nil then -- no text file, so assume at beginning
			lib.storeCrops()
			doExit = lib.initialise() -- false if missing hoe
			if not doExit then
				currentRow = 0
				lib.manageTree(true, farmPos)
				waiting = true
			end
		else
			if lib.goHome(currentRow) then
				waiting = true
			else
				doExit = true
			end
		end
		while not doExit do -- start loop of watching crops, farming all modules
			isFarmToRight, isFarmToFront, farmPos, waiting = lib.watchFarm(waiting, farmPos, currentRow, going) --waits if required, harvests farm
			isFarmToRight, isFarmToFront, farmPos, waiting = lib.farmToRight(isFarmToRight, isFarmToFront, farmPos, waiting) -- no action if no farmToRight
			-- return home and continue with front
			farmPos = lib.returnToLeft(farmPos)
			while isFarmToFront do
				lib.getCrops()
				T:go("U1L1")
				lib.crossFarm(false)
				T:go("F1D1R1")
				currentRow = 0
				farmPos[2] = farmPos[2] + 1 -- change y position
				lib.manageTree(true, farmPos)
				isFarmToRight, isFarmToFront, farmPos, waiting = lib.watchFarm(waiting, farmPos, currentRow, going)
				isFarmToRight, isFarmToFront, farmPos, waiting = lib.farmToRight(isFarmToRight, isFarmToFront, farmPos, waiting)
			end
			farmPos = lib.returnToLeft(farmPos)
			farmPos = lib.returnToFront(farmPos)
			waiting = true -- reset when returned to the start
		end
	end
	
	lib.main(useFile)
end

function manageFarmSetup(farmType)
	-- check if startup.lua exists
	T:clear()
	if fs.exists("start.txt") then
		print("This turtle has been configured to")
		print("start automatically and run the farm")
		print("management program.\n")
		print("Do you want to disable this? (y/n)")
		response = string.lower(read())
		if response == "y" then
			fs.delete("start.txt")
		else
			if farmType == "farm" then
				manageFarm(false)
			else
				manageTreeFarm()
			end
		end
	else
		print("This turtle can be configured")
		print("to be a dedicated farm manager.")
		print("It will start automatically and")
		print("monitor the farm complex, harvesting")
		print("and replanting automatically.\n")
		if farmType == "farm" then
			print("You must provide a diamond hoe")
			print("and place me over the water source")
		else
			print("Place me between the chest and")
			print("first sapling / tree")
		end
		print("\nAre you ready? (y/n)")
		local response = string.lower(read())
		if response == "y" then
			if not fs.exists("startup.lua") then
				local h = fs.open("startup.lua", "w")
				h.writeLine('function main()')
				h.writeLine('	if fs.exists("start.txt") then')
				h.writeLine('		local handle = fs.open("start.txt", "r")')
				h.writeLine('		local cmd = handle.readLine()')
				h.writeLine('		handle.close()')
				h.writeLine('		shell.run("tk.lua "..cmd)')
				h.writeLine('	end')
				h.writeLine('end')
				h.writeLine('main()')
				h.close()
			end
			local h = fs.open("start.txt", "w")
			if farmType == "farm" then
				h.writeLine('farm')
			else
				h.writeLine('tree')
			end
			h.close()
		end
		T:clear()
		print("Startup files written")
		if farmType == "farm" then
			print("Press Enter to check equipment")
			read()
			local equippedRight, equippedLeft = T:setEquipment()
			if equippedRight ~= "minecraft:crafting_table" then
				T:checkInventoryForItem({"minecraft:crafting_table"}, {1})
				local equippedRight, equippedLeft = T:setEquipment()
			end
			T:checkInventoryForItem({"minecraft:diamond_hoe"}, {1})
			print("Place oak, birch, spruce saplings in")
			print("the chest on the right of the tree")
			print("Also wheat and beetroot seeds.\n")
			print("Place carrots and potatoes in")
			print("the chest on the left of the tree\n")
		else
			print("Drop saplings into the water")
			print("if none are planted")
		end
		print("Do you want to start now? (y/n)")
		local response = string.lower(read())
		if response == "y" then
			if farmType == "farm" then
				manageFarm(false)
			else
				manageTreeFarm()
			end
		end
	end
end

function manageTreeFarm()
	local lib = {}
	
	function lib.emptyLogs()
		for i = 1, 16 do
			if turtle.getItemCount(i) > 0 then
				turtle.select(i)
				local name = T:getItemName(i)
				if name:find("sapling") ~= nil then
					turtle.dropDown()
				elseif name:find("apple") ~= nil then
					turtle.drop()
				end
			end
		end
		T:sortInventory()
		-- convert 6 logs-->planks-->360 fuel
		local logsKept = false
		local logSlot = 0
		for i = 1, 16 do
			if turtle.getItemCount(i) > 0 then
				if T:getSlotContains(i):find("log") ~= nil and turtle.getItemCount(i) > 10 then -- 10+ logs in this slot
					turtle.select(i)
					if not logsKept then
						turtle.drop(turtle.getItemCount(i) - 10)
						logsKept = true
						logSlot = i
					else
						turtle.drop()
					end
				else
					turtle.drop()
				end
			end
		end
	end
	
	function lib.initialise()
		local ready = false
		local blockType = ""
		local chest = false
		for i = 1, 4 do
			blockType = T:getBlockType("forward")
			if blockType == "minecraft:chest" then
				chest = true
				break
			end
			T:turnRight(1)
		end
		if chest then
			T:turnRight(2)
			ready = true
		else
			-- allow 10 secs to stop else find start position
			local countDown = 10
			for i = countDown, 1, -1 do
				T:clear()
				print("Hit Ctrl+T to terminate program")
				print("Starting in "..i.." seconds...")
			end
			if lib.goHome() then
				ready = true
			end
		end
		return ready
	end
	
	function lib.getSaplings()
		T:go("L1F2L1F1R1D5F1R2") -- over sapling chest, facing water
		turtle.select(1)
		while turtle.suckDown() do end
		local saplingsKept = false
		for i = 1, 16 do
			if turtle.getItemCount(i) > 0 then
				-- slotContains, slotCount, slotDamage
				item = T:getSlotContains(i)
				if item:find("sapling") == nil then -- not saplings
					turtle.select(i)
					turtle.dropDown()
				elseif item:find("sapling") ~= nil then --saplings
					if saplingsKept then
						turtle.select(i)
						turtle.dropDown()
					else
						saplingsKept = true
					end
				end
			end
		end
		-- max one stack of saplings, sticks used for fuel, others returned
		T:sortInventory()
		T:go("F1U5L1F1R1F2L1")
	end
	
	function lib.goHome()
		local atHome = false
		local onCobble = false
		local hitCobble = false
		local onChest = false
		local onWater = false
		-- find starting position
		-- not at home as lib.initialise failed
		-- ? tree above
		local blockType = T:getBlockType("up")
		if blockType:find("log") ~= nil then -- tree above
			T:harvestTree(false, false, "up")
		end
		-- ? chest/hopper below
		blockType = T:getBlockType("down")
		if blockType == "minecraft:chest" then --over sapling chest
			while turtle.detect() do
				turtle.turnRight()
				onChest = true
			end
		elseif blockType == "minecraft:hopper" then --over hopper 
			for i = 1, 4 do
				if turtle.forward() then
					if T:getBlockType("down") == "minecraft:chest" then
						T:turnRight(2)
						onChest = true
						break
					else
						turtle.back()
					end
				end
				turtle.turnRight()
			end
		end
		if onChest then
			T:go("F1U5L1F1R1F2L1")
			if lib.initialise() then
				atHome = true
			end
		else
			-- ? in the treetops
			blockType = T:getBlockType("down")
			if blockType == "" or blockType:find("leaves") ~= nil then
				while blockType == "" or blockType:find("leaves") ~= nil do
					T:down(1)
					blockType = T:getBlockType("down")
				end
			end
			blockType = T:getBlockType("down")
			if blockType:find("sapling") ~= nil then
				T:go("F1D1")
				blockType = T:getBlockType("down")
			end
			if blockType:find("torch") ~= nil or
			   blockType == "minecraft:dirt" or 
			   blockType == "minecraft:grass_block" then
			
				for i = 1, 4 do
					if turtle.forward() then
						if T:getBlockType("down") == "" then
							T:down(2)
							onWater = true
							break
						end
						turtle.back()
					end
					T:turnRight(1)
				end
			elseif blockType == "minecraft:cobblestone" then
				onCobble = true
			elseif blockType:find("water") ~= nil then
				onWater = true
			end
			if onWater then --move to cobble
				while T:getBlockType("down"):find("water") ~= nil do
					if not turtle.forward() then
						--hit the one cobble block above sapling chest
						hitCobble = true
						break
					end
				end
				if not hitCobble then
					-- now not over water: should be cobble or over ditch to hopper
					if T:getBlockType("down") == "minecraft:cobblestone" then
						onCobble = true
					else
						T:forward(1)
						if T:getBlockType("down") == "minecraft:cobblestone" then
							onCobble = true
						end
					end
				end
			end
			if onCobble then -- find home
				-- find cobble direction
				onCobble = false
				for i = 1, 4 do
					T:go("L1F1")
					if T:getBlockType("down") == "minecraft:cobblestone" then
						onCobble = true
						break
					end
					turtle.back()
				end
				-- continue on cobble until hitCobble
				while not hitCobble do
					while T:getBlockType("down") == "minecraft:cobblestone" do
						if not turtle.forward() then
							if T:getBlockType("forward") == "minecraft:cobblestone" then
								hitCobble = true
								break
							end
						end
					end
					if not hitCobble then
						-- not cobble below, on corner
						turtle.back()
						T:turnLeft(1)
					end
				end
			end
			if hitCobble then -- find home
				hitCobble = false
				-- probably hit from widest cobble path. look for space under cobble block
				if T:getBlockType("down") == "" then --could be over hopper
					T:down(1)
					if T:getBlockType("forward") == "" then
						hitCobble = true
					end
				else -- not over hopper
					T:go("L1F1R1F1R1D1")
					if T:getBlockType("forward") == "" then
						hitCobble = true
					else
						T:go("U2F2D2")
						if T:getBlockType("forward") == "" then
							hitCobble = true
						end
					end
				end
				if hitCobble then-- now over hopper column
					T:go("R2U3L1F1R1F2R1")
					blockType = T:getBlockType("forward")
					if blockType == "minecraft:chest" then
						atHome = true
						lib.emptyLogs()
					end
					T:turnRight(2)
				end
			end
		end
		
		return atHome
	end
	
	function lib.harvest()
		-- started from manageTreeFarm as first sapling grown
		local success = true
		local blockType
		local firstDirt = true
		for  j = 1, 7 do
			for i = 1, 3 do
				blockType = T:getBlockType("forward")
				if blockType == "" then --nothing ahead, so plant sapling
					if firstDirt then
						firstDirt = false
						T:forward(2)
					else
						T:go("U1F1")
						local saplingSlot, name, count = T:getSaplingSlot("sapling")
						if count > 1 then
							T:place("sapling", -1, "down", false)
						end
						T:go("F1D1")
					end
				else -- block ahead, sapling or tree
					if string.find(blockType, "log") ~= nil then
						-- clsTurtle.harvestTree(extend, craftChest, direction)
						T:harvestTree(true, false, "forward")
						if firstDirt then
							firstDirt = false
							T:forward(1)
						else
							turtle.up()
							local saplingSlot, name, count = T:getSaplingSlot("sapling")
							if count > 1 then
								T:place("sapling", -1, "down", false)
							end
							T:go("F1D1")
						end
					elseif  string.find(blockType, "sapling") ~= nil then
						T:go("U1F2D1")
					end	
				end
			end
			if j % 2 == 1 then --odd 1,3,5,7
				T:go("R1F2R1")
			else
				T:go("L1F2L1")
			end
		end
		T:go("F6R1F14L1") --facing chest
		blockType = T:getBlockType("forward")
		if blockType == "minecraft:chest" then -- back home
			T:turnRight(2)
			blockType = T:getBlockType("forward")
			if blockType == "" then --nothing ahead, so plant sapling
				T:place("sapling", -1, "forward", false)
			end
			T:turnRight(2)
			lib.emptyLogs()
			T:turnRight(2)
		else
			success = false
		end
		return success
	end
	
	function lib.main()
		if lib.initialise() then
			while true do
				local blockType = ""
				local waiting = true
				local needsPlanting = false
				local hasSaplings = false
				-- check state of sapling in front. Harvest if changed to log
				while waiting do
					blockType = T:getBlockType("forward")
					if blockType == "" then --no sapling or log
						needsPlanting = true
						break
					elseif blockType:find("log") ~= nil then
						waiting = false
					else --sapling
						print("Waiting for "..blockType)
						sleep(60)
					end
				end
				lib.getSaplings()
				if T:getItemSlot("sapling", -1) > 0 then
					hasSaplings = true
				end
				if (hasSaplings and needsPlanting) or not waiting then
					if not lib.harvest() then-- harvest trees and plant saplings
						break
					end
				end
			end
		end
	end
	
	lib.main()
end

function mineBedrockArea(width, length, leaveExposed)
	--[[ 
	Assume on level 5 or -59
	for 1, width do
		for 1, length do
			go down until bedrock, digging/replacing all directions
			return to 5 / -59
			move forward 1 blocks
		end
		turn right/ forward 2 turn right
	end
	]]	
	function clearColumn()
		local level = 0
		--T:go("L1x1R2x1L1")
		local success = T:down(1)
		while success do
			level = level + 1
			if leaveExposed then
				T:go("R1x1R1x1R1x1R1x1", false, 0, true)
			else
				T:go("R1C1R1C1R1C1R1C1", false, 0, true)
			end
			success = T:down(1)
		end
		if leaveExposed then
			T:go("U"..level)
		else
			T:go("U"..level.."C2")
		end
	end
	local goRight = true
	for i = 1, width do
		for j = 1, length do
			clearColumn()
			T:forward(1)
		end
		if goRight then
			T:go("R1F1R1")
		else
			T:go("L1F1L1")
		end
		goRight = not goRight
	end
end

function placeRedstoneTorch(direction, userChoice)
	if direction == "level" then
		T:go("R1F1D2L2F1R1")
		--clsTurtle.place(self, blockType, damageNo, direction, leaveExisting)
		T:place(userChoice, -1, "forward", false)
		T:back(1)
		T:place("minecraft:redstone_torch", -1, "forward", true)
		T:go("R1F1L1F1U2L1F1R1")
	elseif direction == "up" then
		T:go("R1F1D3R2F1L1")
		T:place("minecraft:redstone_torch", -1, "up", false)
		T:go("R1B1U3F1R1")
		T:place(userChoice, -1, "forward", false)
	end
end

function plantTreefarm(size)
	-- .name = "minecraft:sapling"
	-- .state.type = "dark_oak"
	-- .metadata = 5
	local saplings = {"oak","spruce","birch","jungle","acacia","dark oak"}
	local leastSlot = 0
	local total = 0
	local leastModifier = 0
	local most = 0
	local mostID = -1
	local mostName = ""
	local secondMost = 0
	local secondMostID = -1
	local secondMostName = ""
	
	-- slotData.leastSlot, slotData.leastModifier, total, slotData -- integer, integer, integer, table
	leastSlot, leastModifier, total, data = T:getItemSlot("sapling", i) -- eg 3xoak, 9x spruce Data.leastSlot, Data.leastModifier, total, Data
	--[[
	--  return table
	slotData.mostSlot = 0
	slotData.mostName = ""
	slotData.mostCount = 0
	slotData.mostModifier = 0
	slotData.leastSlot = 0
	slotData.leastName = ""
	slotData.leastCount = 0
	slotData.leastModifier = 0
	]]
	-- example <= 1.12.2: 1, 0,   13, {1, 'minecraft:sapling', 13, 0, 1, 'minecraft:sapling', 13, 0}
	-- example >  1.12.2: 1, nil, 13, {1, 'minecraft:oak_sapling', 13, nil, 1, 'minecraft:oak_sapling', 13, nil}
	if leastSlot > 0 then -- item found
		mostID = data.mostModifier -- number or nil if mc > 1.12.2
		mostName = data.mostName
		secondMostID = data.leastModifier -- number or nil if mc > 1.12.2
		secondMostName = data.leastName
		if mostID == nil then
			for i = 1, #saplings do
				if string.find(mostName, saplings[i]) ~= nil then
					print("first sapling choice: "..saplings[i])
				end
				if string.find(secondMostName, saplings[i]) ~= nil then
					print("second sapling choice: "..saplings[i])
				end
			end
		else -- older mc "minecraft:sapling" with damage = 0, 1, 2 etc
			print("first sapling choice: "..saplings[mostID + 1])
			print("second sapling choice: "..saplings[secondMostID + 1])
		end
	end

	
	local outerCount = 4
	local innerCount = 1
	local repeatCount = 1
	if size > 1 then
		outerCount = 8
		innerCount = 5
		repeatCount = 3
	end
	-- user may have put turtle on the ground
	if T:getBlockType("forward") == "minecraft:dirt" or T:getBlockType("forward") == "minecraft:grass" then
		T:up(1)
	end
	-- start at base of first tree LL corner
	T:go("U1F1")
	for x = 1, 4 do -- place most in a ring on outside of planting area
		for i = 1, outerCount do
			T:place(mostName, mostID, "down", false)
			if i < outerCount then
				T:forward(1)
				T:place(mostName, mostID, "down", false)
				T:forward(1)
			end
		end
		T:turnRight(1)
	end
	-- over first sapling, facing forward
	T:go("F2R1F2L1")
	-- place secondMost sapling in centre
	-- T:place(blockType, damageNo, direction, leaveExisting)
	for x = 1, repeatCount do
		-- plant first column
		for i = 1, innerCount do
			if not T:place(secondMostName, secondMostID, "down", false) then
				T:place(mostName, mostID, "down", false)
			end
			if i < innerCount + 1 then
				T:forward(2)
				if not T:place(secondMostName, secondMostID, "down", false) then
					T:place(mostName, mostID, "down", false)
				end
			end
		end
		-- turn round and move to next column
		T:go("R1F2R1")
		-- plant return column
		for i = 1, innerCount do
			if not T:place(secondMostName, secondMostID, "down", false) then
				T:place(mostName, mostID, "down", false)
			end
			if i < innerCount + 1 then
				T:forward(2)
				if not T:place(secondMostName, secondMostID, "down", false) then
					T:place(mostName, mostID, "down", false)
				end
			end
		end
		if x < repeatCount then
			T:go("L1F2L1")
		end
	end
	if size == 1 then
		T:go("F1R1F3L1F2R1F1R1D2")
	else
		T:go("F1R1F9F2L1F2R1F1R1D2")
	end
end

function quickMine(width, length)
	-- could be 1 wide x xx length (trench) up and return
	-- could be 2+ x 2+
	-- even no of runs return after last run
	-- odd no of runs forward, back, forward, reverse and return
	local directReturn = true
	turtle.select(1)
	if width % 2 == 1 then
		directReturn = false
	end
	numBlocks = 0
	if directReturn then -- width = 2,4,6,8 etc
		for i = 1, width, 2 do -- i = 1,3,5,7 etc
			T:go("V"..length - 1)
			numBlocks = numBlocks + length
			T:go("R1F1R1")
			T:go("V"..length - 1)
			numBlocks = numBlocks + length
			if i < width - 2 then -- eg width = 8, i compares with 6: 1, 3, 5, 7
				T:go("L1F1L1", false, 0, false)
			end
			if numBlocks >= 512 then
				numBlocks = 0
				T:dumpRefuse("forward", 1)
			end
		end
		T:go("R1")
		T:go("V"..width - 1)
		T:go("R1")
	else -- width = 3, 5, 7, 9 eg width 7
		for i = 1, width - 1, 2 do -- i = 1,3,5
			T:go("V"..length - 1)
			numBlocks = numBlocks + length
			T:go("R1F1R1", false, 0, false)
			T:go("V"..length - 1)
			numBlocks = numBlocks + length
			T:go("L1F1L1")
			if numBlocks >= 512 then
				numBlocks = 0
				T:dumpRefuse("forward", 1)
			end
		end
		-- one more run then return
		T:go("V"..length - 1)
		T:go("R2")
		T:go("V"..length - 1)
		T:go("R1")
		T:go("V"..width - 1)
		T:go("R1")
	end
end

function quickMineCorridor(width, length)
	T:go("W"..length - 1 .."R1")
	T:go("W"..width - 1 .."R1")
	T:go("W"..length - 1 .."R1")
	T:go("W"..width - 1 .."R1")
end

function repairWall(startAt, height, width, replaceWith)
	-- go up to startAt
	
	-- if width = 1
	
		-- for h = startAt, height, 1 do
		
			-- replace block with replaceWith ("" = any)
			
			-- move up
			
		--end
		
		-- move back to beginning
		
	-- else
	
		-- remain = height % 2
		
		-- for w = 1, width - remain do
		
			-- for h = startAt, height, 1 do
			
				-- replace block with replaceWith ("" = any)
				
				-- move up
				
			--end
			
			-- move to the right 1 block
			
			-- for i = height, startAt, -1 do
			
				-- replace block with replaceWith ("" = any)
				
				-- move down
				
			--end
			
		-- end
		
	-- end
	
end

function searchForSpawner(level)
	-- go down 8 with ladder above
	-- go(path, useTorch, torchInterval, leaveExisting)
	-- depth = math.floor(level / 8)
	
	T:down(1)
	for i = 1, 6 do
		T:go("C1R1C1R1C1R1C1R1D1e0", false, 0, true, true)
	end
	-- go down 1 further
	T:down(1)
	
	local distance = 0
	local returnLength = 0
	local actualLength = 32
	for j = 1, 4 do
		for i = 1, 3 do
			actualLength = 32
			actualLength = T:createTunnel(actualLength, true) --may cut short if in ocean
			T:turnRight(1)
			T:createTunnel(9, false)
			T:turnRight(1)
			T:createTunnel(actualLength, false)
			returnLength = returnLength + 8
			-- ready to cut next section
			if i < 3 then
				T:turnLeft(1)
				actualLength = T:createTunnel(9, true) --may cut short if in ocean
				if actualLength == 9 then
					returnLength = returnLength + 8
					T:turnLeft(1)
				else
					-- cut short, block tunnel and return
					T:go("C2R1C1L1C1L1C1R1U1L1C1R1C1R1C1R1C0", false, 0, true, false)
					T:go("F"..actualLength.."D1", false, 0, true, true)
					break
				end
			else
				T:turnRight(1)
			end
			T:dumpRefuse(3) -- keep 4 stacks cobble
		end
		T:createTunnel(returnLength + 1, false, false)
		-- move 9 places forward and repeat
		T:createTunnel(9, false)
	end
end

function main()
	local doContinue = true
	checkLabel() -- make sure turtle label is set
	--check if lib folder exists
	if not checkLibs("lib", "clsTurtle") then
		-- use pastebin get to download clsTurtle to libs folder
		print("Missing clsTurtle.lua in libs directory")
		print("Attempting to obtain from Pastebin...")
		if shell.run("pastebin","get","xtNjxjhP","lib/clsTurtle.lua") then
			print("clsTurtle.lua installed from Pastebin")
		else
			print("failed to install clsTurtle.lua from Pastebin")
			doContinue = false
		end
	end
	if not checkLibs("lib", "menu") then
		-- use pastebin get to download menu.lua to libs folder
		print("Missing menu.lua in libs directory")
		print("Attempting to obtain from Pastebin...")
		if shell.run("pastebin","get","BhjbYsw4","lib/menu.lua") then
			print("menu.lua installed from Pastebin")
		else
			print("failed to install menu.lua from Pastebin")
			doContinue = false
		end
	end
	if doContinue then
		menu = require("lib.menu")
		T = require("lib.clsTurtle").new(false) -- true enables logfile to log.txt note dot NOT colon
		T:clear()
		if args[1] ~= nil then
			if args[1] == "farm" then
				if not T:isEmpty() then -- will not run when turtle placed down
					manageFarm(true) -- use file to read status
				end
			elseif args[1] == "tree" then
				manageTreeFarm() -- use file to read status
			elseif args[1] == "test" then
				-- show test menu
				local choice = menu.new("Select test activity", {"Quit","Dig Down"})
				if choice == 2 then -- dig down
					T:down(3)
				end
			end
		else
			print("Minecraft major version: "..mcMajorVersion)
			print("Bedrock level: "..bedrock)
			if T:getUseLog() then
				print("Logging enabled")
			else
				print("Logging disabled")
			end	
			sleep(1)
			local choice, size, width, length, height
			choice = nil
			while choice == nil do
				choice, size, width, length, height = getTask()
				if choice ~= nil then
					getTaskInventory(choice, size, width, length, height)
					break	-- run once only
				end
			end
		end
		T:clear()
		print("Thank you for using 'survival toolkit'")
	else
		print("Add missing files and restart")
	end
end

main()